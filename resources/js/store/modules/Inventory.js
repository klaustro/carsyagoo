export default {
	state:{
		inventories:[],
	    perPage: null,
	    currentPage: 1,
	    lastPage: null,
	    totalRows: null,
	},
	actions:{
		getProducts( context ,params){
			axios.get('admin/playfair/getProducts?page='+params.page + '&search=' + params.target)
			.then(response => {
				context.commit('getProducts' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createInventory(context,payload){
			axios.post('/admin/playfair/createInventory',payload)
			.then(response => {
				let newItem = {
					id: response.data.data.product.id,
                    quantity: response.data.data.quantity,
                    product_id: response.data.data.product_id,
                    name: response.data.data.product.name,
				}
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
				context.commit('createInventory', newItem)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		}
	},
	mutations:{
		getProducts(state,{ list }){
			state.inventories = list.data
		  	state.lastPage = list.last_page
	        state.totalRows = list.total
	        state.perPage = list.per_page
		},
		createInventory(state,newItem){
        	let index = state.inventories.findIndex(inventory => inventory.id == newItem.product_id);
       		state.inventories.splice(index,1,newItem);
		},
	},

}