<?php

use App\BillingTerm;
use Faker\Generator as Faker;

$factory->define(App\BillingService::class, function (Faker $faker) {
    return [
        'billing_term_id' => function(){
          return firstOrFactory(BillingTerm::class);
        },
        'name' => $faker->numerify('Plan ##'),
        'description' => $faker->paragraph(4),
        'price' => rand(10, 50),
        'icon' => 'BugattiChiron-1200x8011-1200x700.jpg',
        'code' => $faker->randomNumber(),
    ];
});
