<?php

use App\Comment;
use App\Post;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::all()->map(function($post){
            factory(Comment::class, 3)->create([
                'post_id' => $post->id,
            ]);
        });

        Comment::all()->map(function($comment){
            factory(Comment::class)->create([
                'post_id' => $comment->post_id,
                'reply' => $comment->id,
            ]);
        });
    }
}
