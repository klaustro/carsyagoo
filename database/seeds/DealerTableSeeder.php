<?php

use Illuminate\Database\Seeder;


class DealerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Dealer::class)->create([
            'user_id' => 1,
        ]);

        factory(App\Dealer::class)->create([
            'user_id' => 2,
        ]);

        factory(App\Dealer::class, 9)->create();
    }
}
