<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostController extends Controller
{
    public function index()
    {
        return Post::with(['category', 'tags', 'comments', 'votes'])
            ->category(request()->blog_category_id)
            ->tag(request()->tag_id)
            ->search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == true ? 'DESC' : 'ASC')
            ->paginate(request()->rows);
    }

    public function show($slug)
    {
        $post = Post::with(['tags', 'user', 'comments.user', 'comments.replies.user'])->where('slug', $slug)->first();

        return $post;
    }

    public function store()
    {
        $post = new Post();
        $post->user_id = auth()->user()->id;
        $post->blog_category_id = request()->blog_category_id;
        $post->title = request()->title;
        $post->intro = request()->intro;
        $post->content = request()->content;
        $post->slug = Str::slug(request()->title);

        $imgUrl = str_replace(" ","", request()->imageUrl);
        $folder = "/storage/posts/{$post->id}/";

        $post->image = $folder . $imgUrl;
        $post->image_alt = request()->image_alt;
        $post->metadesc = request()->metadesc;
        $post->metakeywords = request()->metakeywords;
        $post->publish_date = request()->publish_date;
        $post->save();

        if (request()->tags) {
            $post->tags()->attach(array_column(request()->tags,'id'));
        }

        if (request()->image != '') {
            saveFile(request()->image, $folder, $imgUrl);
        }

        return [
            'message' => trans('app.post.store_message'),
            'id' => $post->id,
            'image' => $post->image,
        ];
    }

    public function update($id)
    {
        $imgUrl = str_replace(" ","", request()->imageUrl);
        $folder = "/storage/posts/$id/";
        $post = Post::find($id);
        $post->user_id = auth()->user()->id;
        $post->blog_category_id = request()->blog_category_id;
        $post->title = request()->title;
        $post->intro = request()->intro;
        $post->image = request()->imageUrl ? $folder . $imgUrl : request()->image;
        $post->content = request()->content;
        $post->slug = Str::slug(request()->title);
        $post->save();

        $post->tags()->detach();
        if (request()->tags) {
            $post->tags()->attach(array_column(request()->tags,'id'));
        }

        if (request()->image != '') {
            saveFile(request()->image, $folder, $imgUrl);
        }

        return ['message' => trans('app.post.update_message')];
    }

    public function delete($id)
    {
        $post = Post::destroy($id);

        return ['message' => trans('app.post.delete_message')];
    }
}
