<?php

namespace App\Http\Controllers;

use App\AdManager;
use App\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function store()
    {
        $review = Review::where([
            'ads_manager_id' => request()->ads_manager_id,
            'user_id' => auth()->user()->id,
        ])->first();
        if ($review) {
            $review ->fill(request()->all());
            $review->save();
            return [
                'message' => trans('app.review.update_message'),
                'new_average' => $review->ads_manager->review_average
            ];
        } else {
            $review = new Review();
            $review->ads_manager_id = request()->ads_manager_id;
            $review->user_id = auth()->user()->id;
            $review->value = request()->value;
            $review->comment = request()->comment;
            $review->save();
            return [
                'message' => trans('app.review.store_message'),
                'new_average' => $review->ads_manager->review_average
            ];
        }


    }

    public function update($id)
    {
        $review = Review::find($id);
        $review ->fill(request()->all());
        $review->save();
        return ['message' => trans('app.review.update_message')];

    }

}
