<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_services', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('billing_term_id');
            $table->foreign('billing_term_id')->references('id')->on('billing_terms')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->string('name');
            $table->string('icon')->nullable();
            $table->longText('description');
            $table->string('code');
            $table->double('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billingservices');
    }
}
