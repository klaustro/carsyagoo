@component('mail::message')

{!!
    str_replace(':first_name', $data['first_name'],
    str_replace(':last_name', $data['last_name'],
    str_replace(':phone', $data['phone'],
    str_replace(':message', $data['message'],
        getConfig('contact_mail'))))
    )
!!}

Thanks,<br>

{{config('app.name')}}

@endcomponent