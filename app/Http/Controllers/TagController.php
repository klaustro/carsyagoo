<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index()
    {
        return Tag::search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);
    }

    public function store()
    {
        $tag = Tag::create(request()->all());
        $tag->save();

        return [
            'message' => trans('app.tag.store_message'),
            'id' => $tag->id,
        ];
    }

    public function update($id)
    {
        $tag = Tag::find($id);
        $tag->fill(request()->all());
        $tag->save();

        return ['message' => trans('app.tag.update_message')];
    }

    public function delete($id)
    {
        $tag = Tag::destroy($id);

        return ['message' => trans('app.tag.delete_message')];
    }

    public function updateAll($id)
    {
        foreach (request()->payload['tags'] as $item) {
            $tag = Tag::find($item['id']);
            $tag->fill($item);
            $tag->save();
        }
        return ['message' => trans('app.tag.update_message')];
    }

    public function list()
    {
        return Tag::all();
    }
}
