<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transmission;

class TransmissionController extends Controller
{

    public function index(){

        return Transmission::search(request()->search)
                    ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
                    ->paginate(5);
    }

    public function TransmissionList()
    {
        $transmission = Transmission::get();

/*        $transmission->map(function($transmission){
            $transmission->anouncement_count = $transmission->anouncements->count();
        });*/

        return $transmission;

    }

	public function store()
    {

        try {

            $v = \Validator::make(request()->draft, [
                'name' => 'required',
                'code' => 'required',
                'category_id' => 'required',
                'status'    => 'required',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }


            if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);

                rename(public_path().'/'.request()->images[0]['url'],public_path().'/images/transmissions/'.request()->images[0]['url']);
                $folder = '/images/transmissions/';
                $transamission = Transmission::create([
                    'name' => request()->draft['name'],
                    'code' => request()->draft['code'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                    'iconimage' => $folder . request()->images[0]['url'],
                ]);

                return [
                    'message' => trans('app.transmission.store_message'),
                    'id' => $transamission->id, 'data' => $transamission
                ];

            }else{

                $transamission = Transmission::create([
                    'name' => request()->draft['name'],
                    'code' => request()->draft['code'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                ]);

                return [
                    'message' => trans('app.transmission.store_message_error'),
                    'id' => $transamission->id, 'data' => $transamission
                ];
            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.store_error')], 500);
        }
    }

	public function update($id)
    {
        try {

            $v = \Validator::make(request()->draft, [
                'name' => 'required',
                'code' => 'required',
                'category_id' => 'required',
                'status'    => 'required',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }

            if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);
                rename(public_path().'/'.request()->images[0]['url'],public_path().'/images/transmissions/'.request()->images[0]['url']);
                 $folder = '/images/transmissions/';
                $equipment = Transmission::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'code' => request()->draft['code'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                    'iconimage' => $folder . request()->images[0]['url'],
                ]);

                return ['message' => trans('app.transmission.update_message')];

            }else{
                $equipment = Transmission::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                ]);

                return ['message' => trans('app.transmission.update_message_error')];
            }
        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.error')], 500);
        }

    }

    public function delete($id)
    {
        Transmission::destroy($id);

        return ['message' => trans('app.transmission.delete_message')];
    }

}
