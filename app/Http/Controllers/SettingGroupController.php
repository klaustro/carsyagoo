<?php

namespace App\Http\Controllers;

use App\SettingGroup;
use Illuminate\Http\Request;

class SettingGroupController extends Controller
{
    public function index()
    {
        $setting_groups = SettingGroup::search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);
        return $setting_groups;
    }

    public function store()
    {
        $setting_group = SettingGroup::create(request()->all());
        $setting_group->save();

        return [
            'message' => trans('app.setting_group.store_message'),
            'id' => $setting_group->id,
        ];
    }

    public function update($id)
    {
        $setting_group = SettingGroup::find($id);
        $setting_group->fill(request()->all());
        $setting_group->save();

        return ['message' => trans('app.setting_group.update_message')];
    }

    public function delete($id)
    {
        $setting_group = SettingGroup::destroy($id);

        return ['message' => trans('app.setting_group.delete_message')];
    }
}
