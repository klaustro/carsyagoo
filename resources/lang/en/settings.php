<?php

return [
    'blog' => [
        'image' => getConfig('blog_image'),
    ],
    'inventory' => [
        'image' => getConfig('inventory_image'),
    ],
    'subscribe' => [
        'image' => getConfig('subscribe_image'),
    ],
    'default_image' => getConfig('default_image'),
    'related' => [
        'columns' => getConfig('related_columns'),
    ],
    'days_expires' => getConfig('days_expires'),
];