<?php

use Faker\Generator as Faker;

$factory->define(App\Transmission::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
        'code' => $faker->randomNumber(),
        'iconimage' => $faker -> imageUrl(640, 480),
        'category_id' => function(){
            return firstOrFactory(\App\Category::class);
        },
        'status' => $faker->boolean,
    ];
});
