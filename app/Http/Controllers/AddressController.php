<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function store()
    {
        $address = new Address();
        $address->user_id = auth()->user()->id;
        $address->state = request()->administrative_area_level_1;
        $address->country = request()->country;
        $address->latitude = request()->latitude;
        $address->locality = request()->locality;
        $address->longitude = request()->longitude;
        $address->postal_code = request()->postal_code;
        $address->route = request()->route;
        $address->street_number = request()->street_number;
        $address->secondary = request()->secondary;
        $address->secondary = request()->secondary;
        $address->phone = request()->phone;
        $address->save();

        return [
            'message' => trans('app.address.store_message'),
            'id' => $address->id,
        ];
    }

    public function update($id)
    {
        $address = Address::find($id);
        $address->state = request()->administrative_area_level_1;
        $address->country = request()->country;
        $address->latitude = request()->latitude;
        $address->locality = request()->locality;
        $address->longitude = request()->longitude;
        $address->postal_code = request()->postal_code;
        $address->route = request()->route;
        $address->street_number = request()->street_number;
        $address->secondary = request()->secondary;
        $address->phone = request()->phone;
        $address->save();

        return ['message' => trans('app.address.update_message')];
    }

    public function show()
    {
        return Address::where('user_id', auth()->user()->id)->first();
    }
}
