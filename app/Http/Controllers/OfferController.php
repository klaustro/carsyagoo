<?php

namespace App\Http\Controllers;

use App\Notifications\AdsOffered;
use App\Notifications\GuestAdsOfferNotification;
use App\Offer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class OfferController extends Controller
{
    public function index()
    {
        return Offer::with('ads_manager')->search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);
    }

    public function store()
    {
        $offer = Offer::create(request()->offer);
        $offer->save();

        $admins = User::where('user_group_id', User::ADMIN)->get();

        Notification::send($offer->ads_manager->dealer->user, new AdsOffered($offer));
        Notification::send($admins, new AdsOffered($offer));
        Notification::send($offer, new GuestAdsOfferNotification($offer));

        return [
            'message' => trans('app.offer.store_message'),
            'id' => $offer->id,
        ];
    }
}
