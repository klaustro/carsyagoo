<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Invoice;

class InvoiceController extends Controller
{
    public function index()
    {
        return Invoice::search(request()->search)
                ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
                ->paginate(5);

    }

    public function invoiceList()
    {
        return Invoice::all();
    }

	public function store()
    {
        try {

            $v = \Validator::make(request()->draft, [
                'note' => 'required',
                'total' => 'required',
                'tax' => 'required',
                'subtotal' => 'required',
                'date' => 'required',
                'dealer_id' => 'required',
                'billing_service_id' => 'required',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }

            $invoice = Invoice::create(request()->draft);


            if ( $invoice->save()) {
                return [
                    'message' => trans('app.invoice.store_message'),
                    'id' => $invoice->id, 'data'=>$invoice, 'status' => 1,
                ];

            }else{
                return [
                    'message' => trans('app.invoice.store_message_error'), 'status' => 1,

                ];

            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.store_error')], 500);
        }
    }

	public function update($id)
    {
        try {

            $v = \Validator::make(request()->draft, [
                'note' => 'required',
                'total' => 'required',
                'tax' => 'required',
                'subtotal' => 'required',
                'date' => 'required',
                'dealer_id' => 'required',
                'billing_service_id' => 'required',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }
            $invoice = Invoice::where('id',$id)
            ->update(request()->draft);

            if ( $invoice) {
                return [
                    'message' => trans('app.invoice.update_message'), 'status' => 1,
                ];
            }else{
                return [
                    'message' => trans('app.invoice.update_message_error'), 'status' => 1
                ];
            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.error')], 500);
        }

    }

    public function delete($id)
    {
        $invoice = Invoice::destroy($id);

        return ['message' => trans('app.invoice.delete_message')];
    }

    public function dealerList($dealer_id)
    {
        return Invoice::filterDealer($dealer_id)->orderBy('id', 'desc')->paginate(5);
    }

    public function print($id)
    {
        $invoice = Invoice::find($id);
        if ($invoice->dealer_id == auth()->user()->dealer->id) {
            return view('dealer.invoice', compact('invoice'));
        }

        abort(403);
    }

    public function pdf($id)
    {
        $invoice = Invoice::find($id);
        if ($invoice->dealer_id == auth()->user()->dealer->id) {
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadView('dealer.invoice', compact('invoice'));
            return $pdf->stream();
        }
        abort(403);
    }
}
