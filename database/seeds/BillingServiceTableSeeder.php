<?php

use App\Dealer;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class BillingServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $billing_services = factory(App\BillingService::class, 3)->create();

        $dealer = Dealer::find(1);

        $due_date = Carbon::now()->addMonth();

        $dealer->services()->attach($billing_services[1]->id, ['expires' => $due_date]);
    }
}
