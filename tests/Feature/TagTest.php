<?php

namespace Tests\Feature;

use App\Tag;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TagTest extends TestCase
{
    use RefreshDatabase;

    protected $user, $category;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->actingAs($this->user);
    }

    /** @test */
    function if_tag_is_paginated()
    {
        $tags = factory(Tag::class, 6)->create();

        $this->get('admin/tag?page=1&search=&orderBy=id&desc=true')
             ->assertSuccessful()
             ->assertSee($tags[4]->name)
             ->assertDontSee($tags[0]->name);

    }

    /** @test */
    function if_tag_is_finded()
    {
        $searched = factory(Tag::class)->create([
            'name' => 'Searched tag',
        ]);

        $tag = factory(Tag::class)->create([
            'name' => 'other'
        ]);

        $this->get('admin/tag?search=Searched&orderBy=id&desc=true')
             ->assertSuccessful()
             ->assertSee($searched->name)
             ->assertDontSee($tag->name);
    }


    /** @test */
    function if_tag_can_be_stored()
    {

        $response = $this->post('admin/tag', [
            'name' => 'Tag name',
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.tag.store_message')]);

        $this->assertDatabaseHas('tags',[
            'name' => 'Tag name',
        ]);

    }

    /** @test */
    function if_tag_can_be_updated()
    {
        $tag = factory(Tag::class)->create();

        $this->put('admin/tag/' . $tag->id, [
            'name' => 'Tag name',
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.tag.update_message')]);

        $this->assertDatabaseHas('tags',[
            'name' => 'Tag name',
        ]);

    }

    /** @test */
    function if_tag_can_be_deleted()
    {
        $tag = factory(Tag::class)->create();

        $this->delete('admin/tag/' . $tag->id)
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.tag.delete_message')]);

        $this->assertDatabaseMissing('tags',[
            'name' => $tag->name,
        ]);

    }
}
