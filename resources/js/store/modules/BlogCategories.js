let state = {
    blogCategories: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading: false,
    data: [],
}

let getters = {
        findBlogCategory(state){
            return function(id){
                let blogCategory = state.blogCategories.find(blogCategory => blogCategory.id == id)
                return blogCategory;
            }
        },
}

let actions = {
    getBlogCategories(context, params){
        context.state.loading = true
        axios.get('/admin/blogCategory?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getBlogCategories', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },


    storeBlogCategory(context, payload){
        context.state.loading = true
        axios.post('/admin/blogCategory', payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    let newBlogCategory = {
                        id: response.data.id,
                        name: response.data.data.name,
                        description: response.data.data.description,
                        imageurl: response.data.data.imageurl,
                        metadesc: response.data.data.metadesc,
                        metakeywords: response.data.data.metakeywords,
                        description: response.data.data.description,
                    }
                    context.commit('storeBlogCategory', newBlogCategory)
                    Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                }

                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },
    updateBlogCategory(context, payload){
        context.state.loading = true
        axios.put('/admin/blogCategory/' + payload.id, payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                    context.commit('updateBlogCategory', payload)
                }
                context.state.loading = false

            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeBlogCategory(context, id){
        context.state.loading = true
        axios.delete('/admin/blogCategory/' + id)
            .then(response => {

                context.commit('removeBlogCategory', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                context.state.loading = false
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })

    },

    listBlogCategories(context){
        axios.get('/admin/blogCategoryList')
            .then(response => {
                context.commit('listBlogCategories', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    getPostCategories(context){
        context.state.loading = true
        axios.get('/api/blogCategory')
            .then(response => {
                context.commit('getPostCategories', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getBlogCategories(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.blogCategories = data.data;
    },

    storeBlogCategory(state, newBlogCategory){
        state.blogCategories.unshift(newBlogCategory);
    },

    updateBlogCategory(state, {id, draft}){
        let index = state.blogCategories.findIndex(blogCategory => blogCategory.id == id);
        state.blogCategories.splice(index, 1, draft);
    },

    removeBlogCategory(state, id)    {
        let index = state.blogCategories.findIndex(blogCategory => blogCategory.id == id);
        state.blogCategories.splice(index, 1);
    },

    listBlogCategories(state, data){
        state.list = data.data;
    },
    getPostCategories(state, {data}){
        state.data = data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}