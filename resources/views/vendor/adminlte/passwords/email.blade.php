@extends('layouts.app')


@section('content')
<div class="main" role="main">
    <div id="content" class="content full">
        <div class="container">
            <div class="listing-header margin-40">
                <h2>Reset Password</h2>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <form action="{{ url(config('adminlte.password_email_url', 'password/email')) }}" method="post">
                        {!! csrf_field() !!}

                        <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                            <input type="email" name="email" class="form-control" value=""
                                   placeholder="{{ trans('adminlte::adminlte.email') }}">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <button type="submit"
                                class="btn btn-primary btn-block btn-flat"
                        >{{ trans('adminlte::adminlte.send_password_reset_link') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


