<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fuel extends Model
{
    protected $fillable = ['letter','csscode','name', 'image','category_id'];


    public function category()
    {
        return $this->BelongsTo('App\Category');
    }

    public function anouncements()
    {
        return $this->hasMany(AdManager::class, 'fuel_id');
    }

    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            $query->where('name', 'like', "%$target%")
            ->where('letter', 'like', "%$target%")
            ->where('csscode', 'like', "%$target%")
            ->orWhereHas('category', function ($query) use ($target) {
                    $query->Where('name', 'like',"%$target%");
                });
        }
        return $query;
    }
}
