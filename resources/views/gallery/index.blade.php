@extends('layouts.app')
@section('content')

	@if (empty($filter))
		@php
			$filter=0 ;
		@endphp
	@endif
	
    <gallery :filter="{{ json_encode($filter)  }}" :user="{{ json_encode(auth()->user()) }}"></gallery>
@endsection