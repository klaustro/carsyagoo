<?php

use App\AdManager;
use App\User;
use Faker\Generator as Faker;

$factory->define(App\Review::class, function (Faker $faker) {
    return [
        'ads_manager_id' => function(){
            return firstOrFactory(AdManager::class);
        },
        'user_id' => function(){
            return firstOrFactory(User::class);
        },
        'value' => rand(1, 5),
        'comment' => $faker->sentence,
    ];
});
