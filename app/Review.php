<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = ['ads_manager_id', 'user_id', 'value', 'comment'];

    public function ads_manager()
    {
        return $this->belongsTo(AdManager::class, 'ads_manager_id');
    }
}
