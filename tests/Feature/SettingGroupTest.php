<?php

namespace Tests\Feature;

use App\SettingGroup;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SettingGroupTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->actingAs($this->user);
    }

    /** @test */
    function if_setting_group_is_paginated()
    {
        $setting_groups = factory(SettingGroup::class, 25)->create();

        $this->get('admin/settingGroup?page=1&search=&orderBy=id&desc=true')
             ->assertSuccessful()
             ->assertJsonFragment(['name' => $setting_groups[24]->name])
             ->assertJsonMissing(['name' => $setting_groups[0]->name]);

    }

    /** @test */
    function if_setting_group_is_finded()
    {
        $searched = factory(SettingGroup::class)->create([
            'setting_tab_id' => 1,
            'name' => 'Searched setting_group',
            'icon' => 'icon-test'
        ]);

        $setting_group = factory(SettingGroup::class)->create([
            'name' => 'Other group'
        ]);

        $this->get('admin/settingGroup?search=Searched&orderBy=id&desc=true')
             ->assertSuccessful()
             ->assertSee($searched->name)
             ->assertDontSee($setting_group->name);

    }
    /** @test */
    function if_setting_group_can_be_stored()
    {
        $response = $this->post('admin/settingGroup', [
            'setting_tab_id' => 1,
            'name' => 'Searched setting_group',
            'icon' => 'icon-test'
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.setting_group.store_message')]);

        $this->assertDatabaseHas('setting_groups',[
            'setting_tab_id' => 1,
            'name' => 'Searched setting_group',
            'icon' => 'icon-test'
        ]);

    }
    /** @test */
    function if_setting_group_can_be_updated()
    {
        $setting_group = factory(SettingGroup::class)->create();

        $this->put('admin/settingGroup/' . $setting_group->id, [
            'setting_tab_id' => 1,
            'name' => 'Searched setting_group',
            'icon' => 'icon-test'
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.setting_group.update_message')]);

        $this->assertDatabaseHas('setting_groups',[
            'setting_tab_id' => 1,
            'name' => 'Searched setting_group',
            'icon' => 'icon-test'
        ]);

    }
    /** @test */
    function if_setting_group_can_be_deleted()
    {
        $setting_group = factory(SettingGroup::class)->create();

        $this->delete('admin/settingGroup/' . $setting_group->id)
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.setting_group.delete_message')]);

        $this->assertDatabaseMissing('setting_groups',[
            'setting_tab_id' => 1,
            'name' => 'Searched setting_group',
            'icon' => 'icon-test'
        ]);

    }
    /** @test */
    function if_setting_group_is_ordered_by_any_column()
    {
        $first = factory(SettingGroup::class)->create([
            'setting_tab_id' => 1,
            'name' => 'AAAAA',
            'icon' => 'AAAAA'
        ]);


        $setting_groups = factory(SettingGroup::class, 20)->create([
            'setting_tab_id' => 1,
            'name' => 'BBBBB',
            'icon' => 'BBBBB'
        ]);

        $last = factory(SettingGroup::class)->create([
            'setting_tab_id' => 1,
            'name' => 'ZZZZZZZZZZZZZZZ',
            'icon' => 'ZZZZZZZZZZZZZZZ',
        ]);

        $this->get('admin/settingGroup?orderBy=name&desc=false')
             ->assertSuccessful()
             ->assertSee($first->name)
             ->assertDontSee($last->name);

        $this->get('admin/settingGroup?orderBy=name&desc=true')
             ->assertSuccessful()
             ->assertSee($last->name)
             ->assertDontSee($first->name);

        $this->get('admin/settingGroup?orderBy=icon&desc=false')
             ->assertSuccessful()
             ->assertSee($first->name)
             ->assertDontSee($last->name);

        $this->get('admin/settingGroup?orderBy=icon&desc=true')
             ->assertSuccessful()
             ->assertSee($last->name)
             ->assertDontSee($first->name);
    }
}
