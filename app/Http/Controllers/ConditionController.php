<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Condition;
use Carbon\Carbon;
use Image;

class ConditionController extends Controller
{
    public function index()
	{

        return Condition::search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);

	}

    public function conditionList()
    {
        return Condition::all();
    }


	public function store()
    {
        if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

            $Base64Img = request()->images[0]['Base64Img'];
            list(, $Base64Img) = explode(';', $Base64Img);
            list(, $Base64Img) = explode(',', $Base64Img);
            $Base64Img = base64_decode($Base64Img);
            $file = file_put_contents(request()->images[0]['url'], $Base64Img);

            $folder = '/images/conditions/';
            rename(public_path().'/'.request()->images[0]['url'],public_path() . $folder . request()->images[0]['url']);

            $condition = Condition::create([
                'name' => request()->draft['name'],
                'category_id' => request()->draft['category_id'],
                'status' => request()->draft['status'],
                'image' => $folder . request()->images[0]['url'],
            ]);

            return [
                'message' => trans('app.condition.store_message'),
                'id' => $condition->id
            ];

        }else{

            $condition = Condition::create([
                'name' => request()->draft['name'],
                'category_id' => request()->draft['category_id'],
                'status' => request()->draft['status'],
            ]);

            return [
                'message' => trans('app.condition.store_message_error'),
                'id' => $condition->id
            ];
        }
    }

	public function update($id)
    {
        if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

            $Base64Img = request()->images[0]['Base64Img'];
            list(, $Base64Img) = explode(';', $Base64Img);
            list(, $Base64Img) = explode(',', $Base64Img);
            $Base64Img = base64_decode($Base64Img);
            $file = file_put_contents(request()->images[0]['url'], $Base64Img);
            $folder = '/images/conditions/';
            rename(public_path().'/'.request()->images[0]['url'],public_path() . $folder . request()->images[0]['url']);

            $condition = Condition::where('id',$id)->update([
                'name' => request()->draft['name'],
                'category_id' => request()->draft['category_id'],
                'status' => request()->draft['status'],
                'image' => $folder . request()->images[0]['url'],
            ]);

            return ['message' => trans('app.condition.update_message')];

        }else{
            $condition = Condition::where('id',$id)->update([
                'name' => request()->draft['name'],
                'category_id' => request()->draft['category_id'],
                'status' => request()->draft['status'],
            ]);

            return ['message' => trans('app.condition.update_message')];
        }

    }

    public function delete($id)
    {
        $condition = Condition::destroy($id);

        return ['message' => trans('app.condition.delete_message')];
    }
}
