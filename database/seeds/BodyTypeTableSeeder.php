<?php

use Illuminate\Database\Seeder;


class BodyTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\BodyType::class)->create([
            'name' => 'SUV',
            'image' => '/images/body_types/SUV_small.png'
        ]);

        factory(App\BodyType::class)->create([
            'name' => 'Sedan',
            'image' => '/images/body_types/sedan_small.png'
        ]);

        factory(App\BodyType::class)->create([
            'name' => 'Luxury',
            'image' => '/images/body_types/luxury_small.png'
        ]);


        factory(App\BodyType::class)->create([
            'name' => 'Sport Car',
            'image' => '/images/body_types/sport_small.png'
        ]);

        factory(App\BodyType::class)->create([
            'name' => 'Convertible',
            'image' => '/images/body_types/convertible_small.png'
        ]);

        factory(App\BodyType::class)->create([
            'name' => 'Crossover',
            'image' => '/images/body_types/crossover_small.png'
        ]);

        factory(App\BodyType::class)->create([
            'name' => 'Hatchbag',
            'image' => '/images/body_types/hatchbag_small.png'
        ]);

        factory(App\BodyType::class)->create([
            'name' => 'hybrid',
            'image' => '/images/body_types/hybrid_small.png'
        ]);


        factory(App\BodyType::class)->create([
            'name' => 'Convertible',
            'image' => '/images/body_types/convertible_small.png'
        ]);

        factory(App\BodyType::class)->create([
            'name' => 'Mini',
            'image' => '/images/body_types/mini_small.png'
        ]);


        factory(App\BodyType::class)->create([
            'name' => 'Truck',
            'image' => '/images/body_types/truck_small.png'
        ]);

        factory(App\BodyType::class)->create([
            'name' => 'VAN',
            'image' => '/images/body_types/VAN_small.png'
        ]);

        factory(App\BodyType::class)->create([
            'name' => 'Wagon',
            'image' => '/images/body_types/wagon_small.png'
        ]);
    }
}
