<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('dealer_id');
            $table->foreign('dealer_id')->references('id')->on('dealers')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->unsignedInteger('billing_service_id');
            $table->foreign('billing_service_id')->references('id')->on('billing_services')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->decimal('subtotal', 15, 8);
            $table->decimal('tax', 15, 8);
            $table->decimal('total', 15, 8);
            $table->dateTime('date')->nullable();
            $table->string('token')->nullable();
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
