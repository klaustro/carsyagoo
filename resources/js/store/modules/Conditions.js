let state = {
    conditions: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading: false,
}

let getters = {
        findCondition(state){
            return function(id){
                let condition = state.conditions.find(condition => condition.id == id)
                return condition;
            }
        },
}

let actions = {
    getConditions(context, params){
        context.state.loading = true
        axios.get('/admin/condition?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getConditions', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    storeCondition(context, payload){
        context.state.loading = true
        axios.post('/admin/condition', payload)
            .then(response => {
                let newCondition = {
                    id: response.data.id,
                    name: payload.draft.name,
                    image: '/images/conditions/' + payload.draft.image,
                    category_id: payload.draft.category_id,
                    status: payload.draft.status,
                }
                Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                context.commit('storeCondition', newCondition)
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateCondition(context, payload){
        context.state.loading = true
        axios.put('/admin/condition/' + payload.id, payload)
            .then(response => {
                context.commit('updateCondition', payload)
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeCondition(context, id){
        context.state.loading = true
        axios.delete('/admin/condition/' + id)
            .then(response => {
                context.commit('removeCondition', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })

    },

    listConditions(context){
        axios.get('/admin/condition-list')
            .then(response => {
                context.commit('listConditions', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getConditions(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.conditions = data.data;
    },

    storeCondition(state, newCondition){
        state.conditions.unshift(newCondition);
    },

    updateCondition(state, payload){
        payload.draft.image = payload.draft.image ? '/images/conditions/' + payload.draft.image : ''
        let index = state.conditions.findIndex(condition => condition.id == payload.id);
        state.conditions.splice(index, 1, payload.draft);
    },

    removeCondition(state, id)    {
        let index = state.conditions.findIndex(condition => condition.id == id);
        state.conditions.splice(index, 1);
    },

    listConditions(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}