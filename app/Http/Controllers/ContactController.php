<?php

namespace App\Http\Controllers;

use App\Contact ;
use App\Mail\Contact as MailContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index()
    {
        return Contact::search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(request()->rows);
    }
    public function store()
    {
        try {
            Mail::to(request()->email)
                ->send(new MailContact(request()->all()));

            $contact = Contact::create(request()->all());
            $contact->save();

            return ['message' => 'Message sended successful, we will respond back soon'];

        } catch (Exception $e) {
            return response()->json(['status', 'error'], 500);
        }

    }
}
