<?php

use App\BlogCategory;
use App\Post;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BlogCategory::all()->map(function($category){
            factory(Post::class, 3)->create([
                'blog_category_id' => $category->id,
            ]);
        });
    }
}
