let state = {
    data: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    loading: false,
}

let actions = {
    getReviews(context, params){
        context.state.loading = true
        axios.get('/api/review?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getReviews', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    storeReview(context, payload){
        context.state.loading = true
        return new Promise((resolve, reject) => {
            axios.post('/api/review', payload)
                .then(response => {
                    context.commit('storeReview', response)
                    resolve(response)
                })
                .catch(error => {
                    Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                    context.state.loading = false
                    reject(error)
                })
        })
    },
}

let mutations = {
    getReviews(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.data = data.data;
    },

    storeReview(state, response){
        Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
        state.loading = false
    },
}

export default {
    state,
    actions,
    mutations
}