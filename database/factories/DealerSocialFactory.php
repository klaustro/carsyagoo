<?php

use Faker\Generator as Faker;

$factory->define(App\DealerSocial::class, function (Faker $faker) {
    return [
        'socialname' => $faker->name,
        'link' => $faker->url,
        'dealer_id' => function(){
            return firstOrFactory(\App\Dealer::class);
        },
        'status' => $faker->boolean,

    ];
});
