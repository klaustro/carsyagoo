let state = {
    dealerSocials: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading:false,
}

let getters = {
    finDealerSocial(state){
        return function(id){
            let dealerSocial = state.dealerSocials.find(dealerSocial => dealerSocial.id == id)
            return dealerSocial;
        }
    },
}

let actions = {
    getDealerSocials(context, params){
        context.state.loading = true
        axios.get('/admin/dealerSocial?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc + '&dealer_id=' + params.dealer_id)
            .then(response => {
                context.commit('getDealerSocials', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    storeDealerSocial(context, payload){
        context.state.loading = true
        axios.post('/admin/dealerSocial', payload)
            .then(response => {
                let newDealerSocial = {
                    id: response.data.id,
                    socialname: payload.draft.socialname,
                    icon: payload.images ? '/images/dealer_socials/' + payload.images[0].upload.filename : null,
                    link: payload.draft.link,
                    dealer_id: payload.draft.dealer_id,
                    status: payload.draft.status
                }
                Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                context.commit('storeDealerSocial', newDealerSocial)
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateDealerSocial(context, payload){
        context.state.loading = true
        axios.put('/admin/dealerSocial/' + payload.id, payload)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateDealerSocial', payload)
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeDealerSocial(context, id){
        context.state.loading = true
        axios.delete('/admin/dealerSocial/' + id)
            .then(response => {
                context.commit('removeDealerSocial', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })

    },

    listDealerSocials(context){
        axios.get('/admin/dealerSocialList')
            .then(response => {
                context.commit('listDealerSocials', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getDealerSocials(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.dealerSocials = data.data;
    },

    storeDealerSocial(state, newDealerSocial){
        state.dealerSocials.unshift(newDealerSocial);
    },

    updateDealerSocial(state, payload){
                console.log(payload)
        payload.draft.icon = payload.images[0].upload ? '/images/dealer_socials/' + payload.images[0].upload.filename : payload.draft.icon
        let index = state.dealerSocials.findIndex(dealerSocial => dealerSocial.id == payload.id);
        state.dealerSocials.splice(index, 1, payload.draft);
    },

    removeDealerSocial(state, id)    {
        let index = state.dealerSocials.findIndex(dealerSocial => dealerSocial.id == id);
        state.dealerSocials.splice(index, 1);
    },

    listDealerSocials(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}