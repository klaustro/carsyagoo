<?php

use App\AdManager;
use App\User;
use Faker\Generator as Faker;

$factory->define(App\WishList::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return firstOrFactory(User::class);
        },
        'ads_manager_id' => function() {
            return firstOrFactory(AdManager::class);
        },
    ];
});
