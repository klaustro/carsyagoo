<?php

namespace App\Http\Controllers;

use App\BodyType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Image;


class BodyTypeController extends Controller
{
    public function index()
	{
        return BodyType::search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);
	}

    public function bodyTypeList()
    {
        $bodytype = BodyType::orderBy('name')->get();

/*        $bodytype->map(function($bodytype){
            $bodytype->anouncement_count = $bodytype->anouncements->count();
        });
*/
        return $bodytype;
    }

	public function store()
    {
     try {

            if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);

                $folder = '/images/body_types/';
                rename(public_path().'/'.request()->images[0]['url'],public_path(). $folder .request()->images[0]['url']);

                $bodyType = BodyType::create([
                    'name' => request()->draft['name'],
                    'metakey' => request()->draft['metakey'],
                    'metadesc' => request()->draft['metadesc'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                    'image' => $folder . request()->images[0]['url'],
                ]);
            }else{
                $bodyType = BodyType::create([
                    'name' => request()->draft['name'],
                    'metakey' => request()->draft['metakey'],
                    'metadesc' => request()->draft['metadesc'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                ]);
            }
            return [
                'message' => trans('app.bodyType.store_message'),
                'id' => $bodyType->id
            ];

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.store_error')], 500);
        }
    }

	public function update($id)
    {
        try {

            if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);

                $folder = '/images/body_types/';
                rename(public_path().'/'.request()->images[0]['url'],public_path(). $folder .request()->images[0]['url']);

                $bodyType = BodyType::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                    'image' => $folder . request()->images[0]['url'],
                    'metakey' => request()->draft['metakey'],
                    'metadesc' => request()->draft['metadesc'],
                ]);

                return ['message' => trans('app.bodyType.update_message')];

            }else{
                $bodyType = BodyType::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                    'metakey' => request()->draft['metakey'],
                    'metadesc' => request()->draft['metadesc'],
                ]);

                return ['message' => trans('app.bodyType.update_message_error')];
            }


        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.update_error')], 500);
        }
    }

    public function delete($id)
    {
        $bodyType = BodyType::destroy($id);

        return ['message' => trans('app.bodyType.delete_message')];
    }
}
