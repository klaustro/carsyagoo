<?php

use App\Dealer;
use Faker\Generator as Faker;

$factory->define(App\BillingPayment::class, function (Faker $faker) {
    return [
        'type' => $faker->creditCardType,
        'cardnumber' => $faker->creditCardNumber,
        'expire_month' => $faker->month,
        'expire_year' => $faker->year,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'cvc' => $faker->buildingNumber,
        'zipcode' => $faker->postcode,
        'account_number' => $faker->bankAccountNumber,
        'routing_number' => $faker->swiftBicNumber,
        'primary' => $faker->boolean,
        'status' => $faker->boolean,
        'dealer_id' => function(){
          return firstOrFactory(Dealer::class);
        },
    ];
});
