let state = {
    dealers: [],
    solo: null,
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading:false
}

let getters = {
    findDealer(state){
        return function(id){
            let dealer = state.dealers.find(dealer => dealer.id == id)
            return dealer;
        }
    },
}

let actions = {
    getDealers(context, params){
        context.state.loading = true
        axios.get('/admin/dealer?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getDealers', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    getDealer(context, id){
        context.state.loading = true
        return new Promise((resolve, reject) => {
        axios.get('/admin/dealer/' + id)
            .then(response => {
                context.commit('getDealer', {data: response.data})
                context.state.loading = false
                resolve()
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
                reject()
            })
        })
    },

    storeDealer(context, payload){
        context.state.loading = true
        new Promise((resolve, reject) => {
            axios.post('/admin/dealer', payload)
                .then(response => {
                    let newDealer = {
                            id: response.data.dealer.id,
                            user: {
                                first_name: response.data.user.first_name,
                                last_name: response.data.user.last_name,
                                login: response.data.user.login,
                                email: response.data.user.email,
                                state_id: response.data.user.state_id,
                                city_id: response.data.user.city_id,
                                zipcode: response.data.user.zipcode,
                                address_1: response.data.user.address_1,
                                address_2: response.data.user.address_2,
                            },
                            dealer_shortname: response.data.dealer.dealer_shortname,
                            dealer_manager: response.data.dealer.dealer_manager,
                            images: response.data.dealer.images,
                            status: response.data.dealer.status,
                            dealer_EIN: response.data.dealer.dealer_EIN,
                            latitude: response.data.dealer.latitude,
                            longitude: response.data.dealer.longitude,
                            website: response.data.dealer.website,
                            original_phone: response.data.dealer.original_phone,
                            redirect_phone: response.data.dealer.redirect_phone,
                            fax: response.data.dealer.fax,
                            mission: response.data.dealer.mission,
                            vision: response.data.dealer.vision,
                            overview: response.data.dealer.overview,
                            biographies: response.data.dealer.biographies,
                            mail_receive: response.data.dealer.mail_receive,
                            sunbizzurl: response.data.dealer.sunbizzurl,
                            about: response.data.dealer.about,
                    }
                    Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                    context.commit('storeDealer', newDealer)
                    context.state.loading = false
                    resolve(response)
                })
                .catch(error => {
                    Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                    context.state.loading = false
                    reject(error)
                })
        })
    },

    updateDealer(context, payload){
        context.state.loading = true
        axios.put('/admin/dealer/' + payload.id, payload)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateDealer', payload)
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeDealer(context, id){
        context.state.loading = true
        axios.delete('/admin/dealer/' + id)
            .then(response => {
                context.commit('removeDealer', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })

    },

    listDealers(context){

        axios.get('/admin/dealerList')
            .then(response => {
                context.commit('listDealers', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getDealers(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.dealers = data.data;
    },

    getDealer(state, {data}){
        state.solo = data
    },

    storeDealer(state, newDealer){
        state.dealers.unshift(newDealer);
    },

    updateDealer(state, {id, draft}){
        let index = state.dealers.findIndex(dealer => dealer.id == id);
        state.dealers.splice(index, 1, draft);
    },

    removeDealer(state, id)    {
        let index = state.dealers.findIndex(dealer => dealer.id == id);
        state.dealers.splice(index, 1);
    },

    listDealers(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}