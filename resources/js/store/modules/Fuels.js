let state = {
    fuels: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading: false,
}

let getters = {
        findFuel(state){
            return function(id){
                let fuel = state.fuels.find(fuel => fuel.id == id)
                return fuel;
            }
        },
}

let actions = {
    getFuels(context, params){
        context.state.loading = true
        axios.get('/admin/fuel?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getFuels', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    storeFuel(context, payload){
        context.state.loading = true
        axios.post('/admin/color', payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    let newFuel = {
                        id: response.data.id,
                        name: payload.draft.name,
                        image: payload.draft.image,
                        csscode: payload.draft.csscode,
                        category_id: payload.draft.category_id,
                        letter: payload.draft.letter,
                    }
                    context.commit('storeFuel', newFuel)
                    Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                }
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateFuel(context, payload){
        context.state.loading = true
        axios.put('/admin/fuel/' + payload.id, payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                    context.commit('updateFuel', payload)
                }
                    context.state.loading = false

            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeFuel(context, id){
        context.state.loading = true
        axios.delete('/admin/fuel/' + id)
            .then(response => {
                context.commit('removeFuel', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                context.state.loading = false
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })

    },

    listFuels(context){
        axios.get('/admin/fuel-list')
            .then(response => {
                context.commit('listFuels', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getFuels(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.fuels = data.data;
    },

    storeFuel(state, newFuel){
        state.fuels.unshift(newFuel);
    },

    updateFuel(state, {id, draft}){
        let index = state.fuels.findIndex(fuel => fuel.id == id);
        state.fuels.splice(index, 1, draft);
    },

    removeFuel(state, id)    {
        let index = state.fuels.findIndex(fuel => fuel.id == id);
        state.fuels.splice(index, 1);
    },

    listFuels(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}