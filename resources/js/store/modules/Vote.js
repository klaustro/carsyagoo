let state = {
    count: null,
    own: null,
    loading: false,
}

let actions = {
    getVotes(context, params){
        context.state.loading = true
        axios.get('/api/vote?post_id=' + params.post_id
            )
            .then(response => {
                context.commit('getVotes', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
    storeVote(context, payload){
        context.state.loading = true
        axios.post('/api/vote', payload)
            .then(response => {
                context.commit('storeVote', response.data.vote)
                if (response.data.vote == 1) {
                    Vue.toasted.show(response.data.message, {icon: 'heart', type: 'success'})

                } else {
                    Vue.toasted.show(response.data.message, {icon: 'heart-o', type: 'error'})
                }
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },
}

let mutations = {
    getVotes(state, {data}){
        state.count = data.votes;
        state.own = data.own_vote;
    },
    storeVote(state, vote){
        state.count += vote;
        state.own += vote;

    },
}

export default {
    state,
    actions,
    mutations
}