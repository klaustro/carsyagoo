<?php

namespace Tests\Feature;

use App\BlogCategory;
use App\Post;
use App\Tag;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase;

    protected $user, $category, $tags;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->category = factory(BlogCategory::class)->create();

        $this->tags = factory(Tag::class, 3)->create();

        $this->actingAs($this->user);
    }

    /** @test */
    function if_post_is_paginated()
    {
        $posts = factory(Post::class, 4)->create();

        $this->get('api/post?page=1&search=&orderBy=id&desc=true&rows=3')
             ->assertSuccessful()
             ->assertSee($posts[3]->title)
             ->assertDontSee($posts[0]->title);

    }

    /** @test */
    function if_post_is_finded()
    {
        $searched = factory(Post::class)->create([
            'title' => 'Searched post',
        ]);

        $post = factory(Post::class)->create();

        $this->get('api/post?search=Searched&orderBy=id&desc=true')
             ->assertSuccessful()
             ->assertSee($searched->title)
             ->assertDontSee($post->title);
    }


    /** @test */
    function if_post_can_be_stored()
    {

        $response = $this->post('admin/post', [
            'user_id' => $this->user->id,
            'blog_category_id' => $this->category->id,
            'title' => 'Post title',
            'intro' => 'Post intro',
            'image' => '/post/image1.jpg',
            'content' => '<p>HTML Post content</p>',
            'tags' => $this->tags->toArray(),
            'image_alt' => 'image alt',
            'metadesc' => 'metadesc',
            'metakeywords' => 'metakeywords',
            'publish_date' => '2018-01-01',
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.post.store_message')]);

        $this->assertDatabaseHas('posts',[
            'user_id' => $this->user->id,
            'blog_category_id' => $this->category->id,
            'title' => 'Post title',
            'intro' => 'Post intro',
            'content' => '<p>HTML Post content</p>',
            'slug' => 'post-title',
            'image_alt' => 'image alt',
            'metadesc' => 'metadesc',
            'metakeywords' => 'metakeywords',
            'publish_date' => '2018-01-01',
        ]);

    }

    /** @test */
    function if_post_can_be_updated()
    {
        $post = factory(Post::class)->create();

        $this->put('admin/post/' . $post->id, [
            'user_id' => $this->user->id,
            'blog_category_id' => $this->category->id,
            'title' => 'Post title',
            'intro' => 'Post intro',
            'image' => '/post/image1.jpg',
            'content' => '<p>HTML Post content</p>',
            'tags' => $this->tags->toArray(),
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.post.update_message')]);

        $this->assertDatabaseHas('posts',[
            'user_id' => $this->user->id,
            'blog_category_id' => $this->category->id,
            'title' => 'Post title',
            'intro' => 'Post intro',
            'content' => '<p>HTML Post content</p>',
        ]);

    }

    /** @test */
    function if_post_can_be_deleted()
    {
        $post = factory(Post::class)->create();

        $this->delete('admin/post/' . $post->id)
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.post.delete_message')]);

        $this->assertDatabaseMissing('posts',[
            'user_id' => $post->user_id,
            'blog_category_id' => $post->blog_category_id,
            'title' => $post->title,
            'intro' => $post->intro,
            'image' => $post->image,
            'content' => $post->content,
            'slug' => $post->slug,
        ]);

    }
}
