<?php

use Illuminate\Database\Seeder;
use App\State;
class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(State::class)->create(['name' => 'California', 'acronym' => 'CA']);
        factory(State::class)->create(['name' => 'Florida', 'acronym' => 'FL']);
        /*
        factory(State::class)->create(['name' => 'Alabama', 'acronym' => 'AL']);
        factory(State::class)->create(['name' => 'Alaska', 'acronym' => 'AK']);
        factory(State::class)->create(['name' => 'Arizona', 'acronym' => 'AZ']);
        factory(State::class)->create(['name' => 'Arkansas', 'acronym' => 'AR']);
        factory(State::class)->create(['name' => 'Armed Forces America', 'acronym' => 'AA']);
        factory(State::class)->create(['name' => 'Armed Forces Europe', 'acronym' => 'AE']);
        factory(State::class)->create(['name' => 'Armed Forces Pacific', 'acronym' => 'AP']);
        factory(State::class)->create(['name' => 'California', 'acronym' => 'CA']);
        factory(State::class)->create(['name' => 'Colorado', 'acronym' => 'CO']);
        factory(State::class)->create(['name' => 'Connecticut', 'acronym' => 'CT']);
        factory(State::class)->create(['name' => 'Delaware', 'acronym' => 'DE']);
        factory(State::class)->create(['name' => 'District of Columbia', 'acronym' => 'DC']);
        factory(State::class)->create(['name' => 'Georgia', 'acronym' => 'GA']);
        factory(State::class)->create(['name' => 'Hawaii', 'acronym' => 'HI']);
        factory(State::class)->create(['name' => 'Idaho', 'acronym' => 'ID']);
        factory(State::class)->create(['name' => 'Illinois', 'acronym' => 'IL']);
        factory(State::class)->create(['name' => 'Indiana', 'acronym' => 'IN']);
        factory(State::class)->create(['name' => 'Iowa', 'acronym' => 'IA']);
        factory(State::class)->create(['name' => 'Kansas', 'acronym' => 'KS']);
        factory(State::class)->create(['name' => 'Kentucky', 'acronym' => 'KY']);
        factory(State::class)->create(['name' => 'Louisiana', 'acronym' => 'LA']);
        factory(State::class)->create(['name' => 'Maine', 'acronym' => 'ME']);
        factory(State::class)->create(['name' => 'Maryland', 'acronym' => 'MD']);
        factory(State::class)->create(['name' => 'Massachusetts', 'acronym' => 'MA']);
        factory(State::class)->create(['name' => 'Michigan', 'acronym' => 'MI']);
        factory(State::class)->create(['name' => 'Minnesota', 'acronym' => 'MN']);
        factory(State::class)->create(['name' => 'Mississippi', 'acronym' => 'MS']);
        factory(State::class)->create(['name' => 'Missouri', 'acronym' => 'MO']);
        factory(State::class)->create(['name' => 'Montana', 'acronym' => 'MT']);
        factory(State::class)->create(['name' => 'Nebraska', 'acronym' => 'NE']);
        factory(State::class)->create(['name' => 'Nevada', 'acronym' => 'NV']);
        factory(State::class)->create(['name' => 'New Hampshire', 'acronym' => 'NH']);
        factory(State::class)->create(['name' => 'New Jersey', 'acronym' => 'NJ']);
        factory(State::class)->create(['name' => 'New Mexico', 'acronym' => 'NM']);
        factory(State::class)->create(['name' => 'New York', 'acronym' => 'NY']);
        factory(State::class)->create(['name' => 'North Carolina', 'acronym' => 'NC']);
        factory(State::class)->create(['name' => 'North Dakota', 'acronym' => 'ND']);
        factory(State::class)->create(['name' => 'Ohio', 'acronym' => 'OH']);
        factory(State::class)->create(['name' => 'Oklahoma', 'acronym' => 'OK']);
        factory(State::class)->create(['name' => 'Oregon', 'acronym' => 'OR']);
        factory(State::class)->create(['name' => 'Pennsylvania', 'acronym' => 'PA']);
        factory(State::class)->create(['name' => 'Rhode Island', 'acronym' => 'RI']);
        factory(State::class)->create(['name' => 'South Carolina', 'acronym' => 'SC']);
        factory(State::class)->create(['name' => 'South Dakota', 'acronym' => 'SD']);
        factory(State::class)->create(['name' => 'Tennessee', 'acronym' => 'TN']);
        factory(State::class)->create(['name' => 'Texas', 'acronym' => 'TX']);
        factory(State::class)->create(['name' => 'Utah', 'acronym' => 'UT']);
        factory(State::class)->create(['name' => 'Vermont', 'acronym' => 'VT']);
        factory(State::class)->create(['name' => 'Virginia', 'acronym' => 'VA']);
        factory(State::class)->create(['name' => 'Washington', 'acronym' => 'WA']);
        factory(State::class)->create(['name' => 'West Virginia', 'acronym' => 'WV']);
        factory(State::class)->create(['name' => 'Wisconsin', 'acronym' => 'WI']);
        factory(State::class)->create(['name' => 'Wyoming', 'acronym' => 'WY']);
        */
    }
}
