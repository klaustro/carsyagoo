<?php

use Faker\Generator as Faker;

$factory->define(App\Address::class, function (Faker $faker) {
    return [
        'country' => $faker->country,
        'state' => $faker->randomElement(['FL', 'CA']),
        'locality' => $faker->city,
        'street_number' => $faker->buildingNumber,
        'route' => $faker->streetAddress,
        'secondary' => $faker->secondaryAddress,
        'postal_code' => $faker->postcode,
        'phone' => $faker->phoneNumber,
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
    ];
});
