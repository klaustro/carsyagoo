<?php

namespace Tests\Feature;

use App\AdManager;
use App\Dealer;
use App\Notifications\AdsOffered;
use App\Offer;
use App\User;
use App\UserGroup;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class OfferTest extends TestCase
{
    use RefreshDatabase;

    protected $user, $dealer;

    public function setUp()
    {
        parent::setUp();

        $user_group = factory(UserGroup::class)->create([
            'title' => 'admin',
        ]);

        $this->user = factory(User::class)->create([
            'user_group_id' => $user_group->id,
        ]);

        $this->dealer = factory(Dealer::class)->create([
            'user_id' => $this->user->id
        ]);

        $this->actingAs($this->user);
    }

    /** @test */
    function if_offer_is_paginated()
    {
        $offers = factory(Offer::class, 6)->create();

        $this->get('api/offer?page=1&search=&orderBy=id&desc=true')
             ->assertSuccessful()
             ->assertSee($offers[4]->name)
             ->assertDontSee($offers[0]->name);

    }

    /** @test */
    function if_offer_is_finded()
    {
        $searched = factory(Offer::class)->create([
            'amount' => '99999',
        ]);

        $offer = factory(Offer::class)->create([
            'amount' => '11111'
        ]);

        $this->get('api/offer?search=99999&orderBy=id&desc=true')
             ->assertSuccessful()
             ->assertSee($searched->amount)
             ->assertDontSee($offer->amount);
    }


    /** @test */
    function if_offer_can_be_stored()
    {
        $ads_manager = factory(AdManager::class)->create([
            'dealer_id' => $this->dealer->id,
        ]);

        $response = $this->post('api/offer', ['offer' =>
                [
                    'ads_manager_id' => $ads_manager->id,
                    'name' => 'Juan Palencia',
                    'email' => 'juanchopalen@gmail.com',
                    'phone' => '+584149900552',
                    'amount' => 900,
                ]
            ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.offer.store_message')]);

        $this->assertDatabaseHas('offers',[
            'ads_manager_id' => $ads_manager->id,
            'name' => 'Juan Palencia',
            'email' => 'juanchopalen@gmail.com',
            'phone' => '+584149900552',
            'amount' => 900,
        ]);

    }
}
