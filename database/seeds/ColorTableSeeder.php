<?php

use App\Color;
use Illuminate\Database\Seeder;

class ColorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Color::class)->create(['name' => 'Beige']);
        factory(Color::class)->create(['name' => 'Black']);
        factory(Color::class)->create(['name' => 'Blue']);
        factory(Color::class)->create(['name' => 'Brown']);
        factory(Color::class)->create(['name' => 'Blue']);
        factory(Color::class)->create(['name' => 'Golden']);
        factory(Color::class)->create(['name' => 'White']);
        factory(Color::class)->create(['name' => 'Red']);
        factory(Color::class)->create(['name' => 'Silver']);
        factory(Color::class)->create(['name' => 'Gray']);
        factory(Color::class)->create(['name' => 'Other']);
        factory(Color::class)->create(['name' => 'Green']);
        factory(Color::class)->create(['name' => 'Yellow']);
        factory(Color::class)->create(['name' => 'Orange']);
        factory(Color::class)->create(['name' => 'Maroon']);
    }
}
