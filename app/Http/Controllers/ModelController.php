<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CarModel;

class ModelController extends Controller
{


    public function index()
	{
        return CarModel::search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);
	}

    public function modelList()
    {
        $model = CarModel::where('make_id', request()->make_id)
        ->orderBy('name', 'asc')
        ->get();

/*        $model->map(function($model){
            $model->anouncement_count = $model->anouncements->count();
        });*/

        return $model;
    }

	public function store()
    {
        try {
            if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);

                rename(public_path().'/'.request()->images[0]['url'],public_path().'/images/models/'.request()->images[0]['url']);
                $folder = '/images/models/';
                $model = CarModel::create([
                    'name' => request()->draft['name'],
                    'metakey' => request()->draft['metakey'],
                    'alias' => request()->draft['alias'],
                    'description' => request()->draft['description'],
                    'metadesc' => request()->draft['metadesc'],
                    'status' => request()->draft['status'],
                    'make_id' => request()->draft['make_id'],
                    'image' => $folder . request()->images[0]['url'],
                ]);

                return [
                    'message' => trans('app.model.store_message'),
                    'id' => $model->idv, 'data' => $model
                ];

            }else{

                $model = CarModel::create([
                    'name' => request()->draft['name'],
                    'metakey' => request()->draft['metakey'],
                    'alias' => request()->draft['alias'],
                    'description' => request()->draft['description'],
                    'metadesc' => request()->draft['metadesc'],
                    'make_id' => request()->draft['make_id'],
                    'status' => request()->draft['status'],
                ]);

                return [
                    'message' => trans('app.model.store_message_error'),
                    'id' => $model->id, 'data' => $model
                ];

            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.store_error')], 500);
        }
    }

	public function update($id)
    {
        try {

            if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);
                rename(public_path().'/'.request()->images[0]['url'],public_path().'/images/models/'.request()->images[0]['url']);
                $folder = '/images/models/';
                $model = CarModel::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'metakey' => request()->draft['metakey'],
                    'alias' => request()->draft['alias'],
                    'description' => request()->draft['description'],
                    'status' => request()->draft['status'],
                    'metadesc' => request()->draft['metadesc'],
                    'make_id' => request()->draft['make_id'],
                    'image' => $folder . request()->images[0]['url'],
                ]);

                return ['message' => trans('app.model.update_message')];

            }else{
                $model = CarModel::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'metakey' => request()->draft['metakey'],
                    'alias' => request()->draft['alias'],
                    'description' => request()->draft['description'],
                    'metadesc' => request()->draft['metadesc'],
                    'make_id' => request()->draft['make_id'],
                    'status' => request()->draft['status'],
                ]);

                return ['message' => trans('app.model.update_message_error')];
            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.update_error')], 500);
        }
    }

    public function delete($id)
    {
        $model = CarModel::destroy($id);

        return ['message' => trans('app.model.delete_message')];
    }

}
