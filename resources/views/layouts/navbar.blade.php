<div class="navbar">
    <div class="container sp-cont">
        <a href="#" class="visible-sm visible-xs" id="menu-toggle"><i class="fa fa-bars"></i></a>
        <!-- Main Navigation -->
        <nav class="main-navigation dd-menu toggle-menu" role="navigation">
            <ul class="sf-menu">
                <li><a class="menu @if(\Request::route()->getName() == 'index') active @endif" href="/">Home</a></li>
                <li><a class="menu
                        @if(in_array(\Request::route()->getName(), ['gallery', 'galleryChilds', 'filter']) && strpos(Request::capture()->fullUrl(), 'by-state') == '' )
                            active
                        @endif" href="/gallery"
                    >
                        Inventory
                    </a>
                </li>
                <li><a class="menu @if(in_array(\Request::route()->getName(), ['blog', 'blogChilds'])) active @endif" href="/blog">Blog</a></li>
                @if(! auth()->guest())
                    @if(auth()->user()->isAdmin)
                        <li><a class="menu @if(\Request::route()->getName() == 'dealer') active @endif" href="/dealer">Dealers Page</a></li>
                        <li><a href="/playfair"">Admin Page </a></li>
                    @endif
                    @if(auth()->user()->isDealer)
                        <li><a class="menu @if(\Request::route()->getName() == 'dealer') active @endif" href="/dealer">Dealers Page</a></li>
                    @endif
                @endif
                <li><a class="menu @if(\Request::capture()->fullUrl() == url('contact')) active @endif" href="/contact">Contact</a></li>
                <li><a class="menu @if(\Request::capture()->fullUrl() == url('/gallery/by-state/FL')) active @endif" href="/gallery/by-state/FL">FLORIDA</a></li>
                <li><a class="menu @if(\Request::capture()->fullUrl() == url('/gallery/by-state/CA')) active @endif" href="/gallery/by-state/CA">CALIFORNIA</a></li>
            </ul>
        </nav>

    </div>
</div>