<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WishList extends Model
{
    protected $fillable = ['user_id', 'ads_manager_id'];

    public function ads_manager()
    {
        return $this->BelongsTo(AdManager::class);
    }

    //Scopes
    /**
     * Method to search by any column
     * @param  Query $query
     * @param  string $target [description]
     * @return Query
     */
    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->
                where('id', $target)
                ->orWhereHas('ads_manager', function ($query) use ($target) {
                        $query->whereHas('make', function($query) use($target){
                            $query->where('name', 'like',"%$target%");
                        })
                        ->orWhereHas('model', function($query) use($target){
                            $query->where('name', 'like',"%$target%");
                        });
                    });
        }
    }
}
