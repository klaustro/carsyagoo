<?php

namespace Tests\Feature;

use App\Category;
use App\Color;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ColorTest extends TestCase
{
    use RefreshDatabase;

    protected $user;
    protected $category;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->actingAs($this->user);

        $this->category = factory(Category::class)->create();
    }

    /** @test */
    function if_color_is_paginated()
    {
        $colors = factory(Color::class, 6)->create();

        $this->get('admin/color?page=1&search=&orderBy=id&desc=true')
             ->assertSuccessful()
             ->assertSee($colors[5]->name)
             ->assertDontSee($colors[0]->name);

    }

    /** @test */
    function if_color_is_finded()
    {
        $searched = factory(Color::class)->create([
            'name' => 'Searched color',
        ]);

        $color = factory(Color::class)->create();

        $this->get('admin/color?search=Searched&orderBy=id&desc=true')
             ->assertSuccessful()
             ->assertSee($searched->name)
             ->assertDontSee($color->name);

    }
    /** @test */
/*    function if_color_can_be_stored()
    {
        $response = $this->post('admin/color', [
            'name' => 'blue',
            'images' => [
                0 => [
                    'url' => '51523.jpg',
                    'Base64Img' => 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxESEhMSExASFRUVEBYVExIYEBcSDxURGBUXGBcYFRYaHiogGBolHhUVIjEhJSkrLi4uFx8zOjMuNyktLjcBCgoKDg0OFxAQFi0ZFR0rKy0tKy0tLSstKy0tLS03LS03LS0rKy03LS0rNystLS0tKzcrLS0tLSs3Nzc3LS0rK//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABwgEBQYDAgH/xABREAABAwIBBggJCAQMBwEAAAABAAIDBBEFBgcSITFRCBNBYXGRobEiIzJScoGSwcIkQmJjc6Ky0RQzU+FDVGR0gpOjs8PS4vAVJTSDlKTyRP/EABYBAQEBAAAAAAAAAAAAAAAAAAABAv/EABkRAQEBAQEBAAAAAAAAAAAAAAABEUESIf/aAAwDAQACEQMRAD8AnFERAREQEREBERAREQEREHA47naoKad9OWVEpjcWvfGxhjDxqLQXPaSQdRsLL8gzwYU7a+dnpU7j+C6gTHoiyqqWHa2qmaee0rhf17VgrOt5Fm6fOVhD9lawekySP8TQs+DLTDH+TiFKeb9IYD1Eqq11+XTU8rcxY3Su8mqgd0TMPcVlMqGHY9p6HAqnuiNwX0wW2ADsTTyuKiqA2d42PcOhxC+/+JTjZUTjoneO4q6eVvEVQhi9V/Gqn/yZf8y6/NdlBVjEqaL9Ime2V7mSMfK+RpZoON7OJsRog35jvKaeVjkRFWRERAREQEREBERAREQEREBERAREQEREFcM6uSlXDV1dWYD+jPnDmzabC28gaLaIdpDwyRsXB6X+7EKfs/1Xo4fFH+0q2A+ixj394auEzVZMxV5qYZh4JpXaLx5UchkZoPbzjRPTrHKstS/EftjcRpBriL20g0lt91968jKN461IuRmNz4HiElLVaoXPDZx8wfs6hn0bWv8AROvW2yn99JDIATHG8EXBLGuBB5QeUJhap6DderSrX1GS9A/y6Gld000Z9ywpMgsKO3Dqb1RBvcrhqrxK+HFWXrs32CsY6SSkiYxjS57+NkY1rQLkkhwsFCVdh8NfWiDDaQxxk6MYL5HOc0eVLIXk6DebkFuU2Uw1yYUoZg8J4ysmqSNUEOg08nGSnvDWu9pcLlNg7aatlpGyFwieyMv1AucWML9XINJzrcwCsLmtwWKmoIjG0gzeOkJJJc8gAbdg0Wt1JIWuvREWmRERAREQEREBERAREQEREBERAREQEREEL8Imq10MXJaeQ9Pi2t73rJzAwf8AVO3RwAesyk9w61zefup0sSYy+qOijFtznSSk9mj1Lusx0FqWodvqAz2YmH41OtcZedrIr9Pp+Ohb8pgaSwDbLFtdEeflbz6vnFc9mRy00mjDp3eE0H9FcdRLBrMRvytFyOYEfNUvqC88GST6OduJ0t2MfKHSaOow1N7tkG4PO36XpIkTovmR4aC5xAABJJNgANZJPIFzOb/K1mI0glNmyx+DUMvYNeB5Q+i4ax6xyKMM5GXcmIyjD6HSdCX6Dizyql9/Jb9UPvWv5O2mPjL7LGXFp20NEHOh4wBoGo1Eg+ed0YtcX3aR5LShkBkbHhsNtT53gGaW3L5jNzB27ViZt8hWYdHpyaL6mRvjH7Qxu3i2HdvPKR0LsaiUMY5x2NaXHoAuoVVTG5+NxKpf51bOR0CR9uwBWcyZh0KOmbupoh9wKqmEOMkukdrg53rd/wDStzTR6LGN81gHULJFr1REVZEREBERAREQEREBERAREQEREBERAREQVnzrT8ZjFTua+KMdAijv2kqYMz0VsPv59RI7qs34VBmUs/G4nVP31s3U17gOxoVgs2MOjhlNziR3tSvPvUjVdSsbEqKOeKSGVodHIwte07C0jX0dKyCVBmc7OK6secPw8udG52hJIzW+ocTbi4rbY95HlejtrKPsRlNLLVQU1U58Li6J0jCQ2eEG4Dt45CRqOu1wVOeafImGkgZVOcyWeaMOEgOkyONwuGxnvdy7Ni5ukzN/8vfpu+XOAfHZ3ioyNfE7jpbC7kNrahr8symV5ikOGVBLbvdxGlqcyUE6cJvs1gkDfpDlCjVTWtLlrVcVh9bJyto5iPS4t1u2y3S47O9U8XhFWfObHH/WSsYexxVZV+yOp9KoY3e+NntPAVslV/NtDpVkA31cPUHhx7FaBSLRERVBERAREQEREBERAREQEREBERAREQF+PdYE7hdfq1uU1TxVHVS/s6WZ/sxuPuQVWppeMmMnnl7/AGiT8Ss/kXGGYfSA6rUsZJ2DW0OJ7VV/DwA7oYfcu2yxy6lrWQ4ZQB5i4uOFxaDxtS8NA0WjaI9Xrtr1bZGqz85ucV9a/wD4fQaTonO4t8jNb6lx1cXHb+D3n53o+V2ua/N0zD2ionDXVbm9LIGn5jDyu3u9Q1bfvNjm6ZhzRPNovq3N1u2shadrI+fe7l2bF36qaKFM9mSLopBidOC0FzRUaOoslBAjmG65sCd4aeUqa141tKyWN8UjQ9j2Fj2HY5rhYgojmc22VoxGlDnEcfHZk7Rq8K2p4HmuAv03HItLn5qNHDA39pVxM6g6T/DUds47J3Fdek6E8v7akcdu4vaR1t3OXWZ+a9ktHQmNwcyWcyscDqcwREAj+sCi59cfmihvXU3PO8+zC53uVjlAWZaG9ZCfNZM7sLPiU+pCiIiqCIiAiIgIiICIiAiIgIiICIiAiIgLmc5k2jhdaT86nLP6whnxLplxGeWW2FTDzpIW/wBqx3woK3TtOkLAnwbbL7T+5Z2EY1VYfLxsLuJkLdHSdAwu0SbkDjGm17a7WK2eS0elUQjzquFvW9n5q0r4wdoB6RdRrVcqbPJiw/hKaT0oO/QcFtafPfXDy6Smd0GRnvKmeqydopdUlHTP9Knjd3haufN5hL9uH049FnFn7lkTUfU2fN3z8OH9GqPvjWxhz4U3z6KoHovjf3lq30uaTBzsppG+jVT/ABPKwJ8zGGu8mSqZ0SsI+8wofHIZw8ucNxOm0RDUx1ER0oJHRRltz5THFshOi4Acmohp5FH0+KyyQQ0z3XjgdI6K+1ok0S5vRdtx0lTDLmOpvm11SPSZG7uaFG+XmSzcNqRTiYy3gbKXFmgRpOe0C1z5hRY7HMfD8qvupHn1ukj/AHqblEGY+Lxsx82mjHW7/SpfSJRERVBERAREQEREBERAREQEREBF41NVHG3SkexjfOc4Nb1lc3iOcCgiuBI6U7o2aQ9o2b2oOqRRjX503m4hpWjc6R5cfZba3tLnq3LrEJP4fQG6NjWdti7tU1cTc5wAuTYb9gUXZ78bpzRRxMnjc41TC5rZGucAGSHWAdWuy4Wqq5JTeSWSQ/TeX95WjylbeIczr9iaSPzJLEYo6qmc8kNbVxPcdEnwWvaeTb5KsPFlxhzv/wBIHTG9ve1Vbwz9YzmcF2QkCatifosp6F2ysg9crW95WbDiEL/Imjd6MjXdxVddIISE1MWTRVxhqHM8h7m+i4t7lnQY/WN8mrnH/eeR1EppiwCrxnqk0sUk+jTxN7HO+PtW3psssQaRaqcfSax3e264vKl9RVVlRM8X/Vl0hs1tmxMbYDfq5OVCJSzJx66o/RhH94pSUa5lm+Lqj9ZGOprvzUlKwoiIiCIiAiIgIiICIiAiIgIiIITz6Uc8VRDVNmuySPimxeY5l3Ei4IsdMdSjKPGJteu9jbWG7exS/wAIL9VR/ay/gaoVhHlemVK1Gxbjkg2tHsn3FejcoN8Y6yPhWtsvwqDcNx+Pla71Oae8heOI4hHIywv6x+V1rV8lg80dSD5pW6LgTvW+jqmn57faC0XFj/ZITQ5z1/mi66Rr7r7BXLaHP2Bfrnv5HkdBI96I6nSRsl9mvtWnwylLtF7nuOs6rnkJC3zG21DUg9IxbWT6gtZlHKSxg2Azs1cnlcu9bONhcQGgknUABdxPMBtWlymk0ZW072ubIyaMva5paW7CAb8tnBVEyZk3ExVdxa1Q0DnHFg37T1KSVHWZf9TU/bt/AFIqqUREQEREBERAREQEREBERAREQRJwg/1VF9rL+Bqi7JXDY56mmieCWy1TGPAcQSwygOsRs1X1hShwhT4qi+1l/A1RlkpiLaaaCoc0ubFLplo8o2cTYX57KLxMVXmZw53kS1UfM2Vjh99hPatXVZkIvmV8rfThY/taWrT4tnkrJLingigHnOJmkHc3sXLSYhiWJOLTJVVJvrYwOMbTztZ4LfWgysdyLp6W4OMUj3C/gNjeZL7rML7HpsuTdEb6tfPs71IeDZpa+Wxk4qnby6TtOX2WautwWRl9m7hw7DpKhs8skzXxN0iGtiAc8NOiyxPLyuKGo2oaSSZ7WRMc9zn6DWi1y/ZYa1nVOTVfH5dBVi3L+iylvtBpHaszNk/5bSk/x1l/WW/mrRJhqn08bmantczmc0sPUV4F4Owg9CuO5oOogEbiLhQpwgqaNjqIsjY0u4/SLWBpdbirXI27T1oa4PJujfIWNYHPc92ixg33dsUq4HmwkdZ1VLoD9nHZ0nrefBHquuFzWi9ZRfau7BIVYqWVrQXOcGtAuXEgNA3knYkK1+D4DTUotDC1ptrf5Uh6XnWehVvztDRxuq+1gPXBCVNWOZzKSK7IAah45WnRhB9Pl9QI51B+W+lUzzV8hAke+LwGi0YDQyMbbk6moRMWZU+Jqftm/gUjqN8yp8VU/as/CpIVQREQEREBERAREQEREBERAREQRDwhj4ui+1l/CxRxkVhArKiClc8sEr3guABcA1r3mwOr5ikThEHwKEfWTfhjXK5nI74lSnzRM7+ykHxKLxLODZssMp7EwGZw+dM7jL/0NTPurroIWsaGsa1rRsa1oa0DmA2L0RVBcNnrZfB6rmdAf/YiHvXcrlM6sOnhNcN0Bd7Dmu+FBAebZ1qyl/n0Pa9gVp1VLN861ZTc1dSn1cc2/crWqLRQrwij4VAOao74VNShLhGHxlB6FT3wKpHGZJYlJTOhnia1z49Mta6+hch7ddterSv6lt6+vra94E0sk7ibsgY0iIH6MTdtt5uVj5tMIZVzwQvLg0iRzi0gOs0ONgSOhWBwjBaelbowQtZvIF3u9Jx1n1qKjTJ/NnO+zqhwhZ+zbZ0xHP8ANZ29Cj3PHhrKWvEEWkIxTxvDS4u8IlwLjflOirOqu3CDZbEozvoY+sSzfuQd/mTPiqr7SP8AC5SUoxzHu8VU9MJ+69ScqgiIgIiICIiAiIgIiICIiAiIghrhEu/6Ec857IlocyDb4hHzU8x7h8S3HCKd4dAPoVB7YVrcwkd697vNopO2WL8ip1eJ9REVQWgy/j0sMrx/IZz6xE4juW/WDjkOnTVDPOp5G9bCEFWchn2q4P51TnqlaraKn+SMujUwH62I9UjVcBSLRQjwjP1lB9nUd8Cm5QfwjHeOoR9VP2ui/JVGHmRbesi5oJT2ge9T4oLzFMvVA7qOTtkj/ep0QooC4RcfyyldvpXDqk/1KfVB3CPZ4ygdvjnF+gxH3lCN7mIfeKo9GA/depUUR5gneBOPqoP8QKXEKIiICIiAiIgIiICIiAiIgIiIIP4RT/HUI3RTnrdF+S8+D029TVHzaZg9qS/wr54Q7vlVGN1NIeuRv5LM4OsXhVz/AKNO3+9KnV4mpERVBfL23BG8WX0iCmtDeKUA7WPseljtfcrlKv2IZmcSfUyFr6YRSTyOD+NddsbnEgluhcmx2Dl5eVWACiv1QVwij8oohuglPW9n5KdVA3CIPyukH8mf/eD8lUZmYRt55DupO+Rv5KbVDHB+bd9Qd0EI9pzz8KmdCihvhHxeKoX7pZm+0xp+FTIop4RMfyGmdurQOuGU/ChGDwfZNU4+qZ2SSD3qZFB/B5k8bUN+oB/tP3qcEi0RERBERAREQEREBERAREQEREEB8IN966nG6jv1yv8A8q3/AAdovk9Y/fPG32Y7/GuWz+vvibBuoYuvjZz+S7bg+Q2w+d3nVrrdAhhHfdTq8SgiIqgiIgIiICgHhDO+XU43Ud+uV/5KflXnhAu/5lGN1BH2yz/uSrHT8HyPwKp3NAOyQ+9S+or4P8fyWodvnY32Y2n4lKiJRcjnQyUlxOjEET42yNnZK0vLgzwQ5pBLQSNTzyFdciCM802QFVhsk0lRJCdJmg1kbnPuNJrtIuIFtlrW5VJiIgIiICIiAiIgIiICIiAiIgIiIOZymyCw+vk42ohc6QMDBI2WSN2iCSBZrrHado5Vt8Ewano4WwU8QjjbrDRckk7S4nW4neVnogIiICIiAiIgKuWfp98Utuo4h96U+9WNUaZwc1jsRqTVMrBETExnFug02+DfWHB4tt2WKD9zCR2w+U+dVu6hFEO+6kpaHIrJpuHUrKZshkIcXPkLQ3Se7bZo2DYALnZtW+QEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQf/Z',
                ]
            ],
            'csscode' => '#8940d3',
            'category_id' => $this->category->id,
            'status' => '1',
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.color.store_message')]);

        $this->assertDatabaseHas('colors',[
            'name' => 'blue',
            'image' => 'https://lorempixel.com/640/480/?51523',
            'csscode' => '#8940d3',
            'category_id' => $this->category->id,
            'status' => '1',
        ]);

    }*/
    /** @test */
/*    function if_color_can_be_updated()
    {
        $color = factory(Color::class)->create();

        $this->put('admin/color/' . $color->id, [
            'name' => 'blue',
            'image' => 'https://lorempixel.com/640/480/?51523',
            'csscode' => '#8940d3',
            'category_id' => $this->category->id,
            'status' => '1',
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.color.update_message')]);

        $this->assertDatabaseHas('colors',[
            'name' => 'blue',
            'image' => 'https://lorempixel.com/640/480/?51523',
            'csscode' => '#8940d3',
            'category_id' => $this->category->id,
            'status' => '1',
        ]);

    }*/
    /** @test */
    function if_color_can_be_deleted()
    {
        $color = factory(Color::class)->create();

        $this->delete('admin/color/' . $color->id)
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.color.delete_message')]);

        $this->assertDatabaseMissing('colors',[
            'name' => $color->name,
        ]);

    }
    /** @test */
    function if_color_is_ordered_by_any_column()
    {
        $first = factory(Color::class)->create([
            'name' => 'aaaaa',
            'csscode' => '#AAAA',
        ]);


        $colors = factory(Color::class, 4)->create([
            'name' => 'bbbbb',
            'csscode' => '#BBBBB',
        ]);

        $last = factory(Color::class)->create([
            'name' => 'zzzzz',
            'csscode' => '#ZZZZZ',
        ]);

        $this->get('admin/color?orderBy=name&desc=false')
             ->assertSuccessful()
             ->assertSee($first->name)
             ->assertDontSee($last->name);

        $this->get('admin/color?orderBy=name&desc=true')
             ->assertSuccessful()
             ->assertSee($last->name)
             ->assertDontSee($first->name);

        $this->get('admin/color?orderBy=csscode&desc=false')
             ->assertSuccessful()
             ->assertSee($first->csscode)
             ->assertDontSee($last->csscode);

        $this->get('admin/color?orderBy=csscode&desc=true')
             ->assertSuccessful()
             ->assertSee($last->csscode)
             ->assertDontSee($first->csscode);
    }
}
