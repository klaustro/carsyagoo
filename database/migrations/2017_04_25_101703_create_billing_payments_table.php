<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_payments', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('dealer_id');
            $table->foreign('dealer_id')->references('id')->on('dealers')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->string('type');
            $table->longText('cardnumber')->nullale();
            $table->string('expire_month')->nullale();
            $table->string('expire_year')->nullale();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('cvc')->nullale();
            $table->string('zipcode')->nullable();
            $table->longText('account_number')->nullable();
            $table->longText('routing_number')->nullable();
            $table->boolean('primary')->default(true);
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billingpayments');
    }
}
