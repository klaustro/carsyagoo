<?php

use Illuminate\Database\Seeder;


class SocialMediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\SocialMedia::class,10)->create();
    }
}
