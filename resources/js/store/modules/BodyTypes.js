let state = {
    bodyTypes: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading: false,
}

let getters = {
        findBodyType(state){
            return function(id){
                let bodyType = state.bodyTypes.find(bodyType => bodyType.id == id)
                return bodyType;
            }
        },
}

let actions = {
    getBodyTypes(context, params){
        context.state.loading = true
        axios.get('/admin/bodyType?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getBodyTypes', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    storeBodyType(context, payload){
        context.state.loading = true
        axios.post('/admin/bodyType', payload)
            .then(response => {
                console.log(payload)
                let newBodyType = {
                    id: response.data.id,
                    name: payload.draft.name,
                    metakey: payload.draft.metakey,
                    metadesc: payload.draft.metadesc,
                    image: payload.image,
                    category_id: payload.draft.category_id,
                    status: payload.draft.status,
                }
                Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                context.commit('storeBodyType', newBodyType)
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateBodyType(context, payload){
        context.state.loading = true
        axios.put('/admin/bodyType/' + payload.id, payload)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateBodyType', payload)
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeBodyType(context, id){
        context.state.loading = true
        axios.delete('/admin/bodyType/' + id)
            .then(response => {
                context.commit('removeBodyType', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })

    },

    listBodyTypes(context){
        axios.get('/admin/bodyType-list')
            .then(response => {
                context.commit('listBodyTypes', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getBodyTypes(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.bodyTypes = data.data;
    },

    storeBodyType(state, newBodyType){
        console.log(newBodyType)
        state.bodyTypes.unshift(newBodyType);
    },

    updateBodyType(state, {id, draft}){
        draft.image = draft.image ? '/images/body_types/' + draft.image : ''
        let index = state.bodyTypes.findIndex(bodyType => bodyType.id == id);
        state.bodyTypes.splice(index, 1, draft);
    },

    removeBodyTpe(state, id)    {
        let index = state.bodyTypes.findIndex(bodyType => bodyType.id == id);
        state.bodyTypes.splice(index, 1);
    },

    listBodyTypes(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}