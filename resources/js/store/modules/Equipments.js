let state = {
    Equipments: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading:false
}

let getters = {
        findEquipment(state){
            return function(id){
                let Equipment = state.Equipments.find(Equipment => Equipment.id == id)
                return Equipment;
            }
        },
}

let actions = {
    getEquipments(context, params){
        context.state.loading = true
        axios.get('/admin/equipament?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc )
            .then(response => {
                context.commit('getEquipments', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    storeEquipment(context, payload){
        context.state.loading = true
        axios.post('/admin/equipament', payload)
            .then(response => {
                if (payload.draft.iconimage != '') {
                      payload.draft.iconimage= '/images/equipments/'+payload.draft.iconimage
                }
                let newEquipment = {
                    id: response.data.data.id,
                    name: payload.draft.name,
                    code: payload.draft.code,
                    iconimage: payload.draft.iconimage,
                    category_id: payload.draft.category_id,
                    status: payload.draft.status,
                }
                Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                context.commit('storeEquipment', newEquipment)
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateEquipment(context, payload){
        context.state.loading = true
        axios.put('/admin/equipament/' + payload.id, payload)
            .then(response => {
                if (payload.draft.iconimage != '') {
                      payload.draft.iconimage= '/images/equipments/'+payload.draft.iconimage
                }
                console.log(payload.draft.iconimage)
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateEquipment', payload)
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeEquipment(context, id){
        context.state.loading = true
        axios.delete('/admin/equipament/' + id)
            .then(response => {
                context.commit('removeEquipment', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })

    },

    listEquipments(context){
        axios.get('/admin/equipmentList')
            .then(response => {
                context.commit('listEquipments', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getEquipments(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.Equipments = data.data;
    },

    storeEquipment(state, newEquipment){
        state.Equipments.unshift(newEquipment);
    },

    updateEquipment(state, {id, draft}){
        let index = state.Equipments.findIndex(Equipment => Equipment.id == id);
        state.Equipments.splice(index, 1, draft);
    },

    removeEquipment(state, id)    {
        let index = state.Equipments.findIndex(Equipment => Equipment.id == id);
        state.Equipments.splice(index, 1);
    },

    listEquipments(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}