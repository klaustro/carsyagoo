<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

    protected $fillable = ['user_id', 'country', 'state', 'locality', 'street_number', 'route', 'secondary', 'postal_code', 'phone', 'latitude', 'longitude',];

}
