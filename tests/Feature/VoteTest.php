<?php

namespace Tests\Feature;

use App\Post;
use App\User;
use App\Vote;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class VoteTest extends TestCase
{
    use RefreshDatabase;

    protected $user;
    protected $post;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->actingAs($this->user);
        $this->post = factory(Post::class)->create();
    }

    /** @test */
    function if_user_can_vote()
    {

        $this->post('api/vote', ['post_id' => $this->post->id])
             ->assertSuccessful()
             ->assertJson(['message' => trans('app.vote.vote_message')]);

        $this->assertDatabaseHas('votes',[
            'user_id' => $this->user->id,
            'post_id' => $this->post->id,
        ]);

    }

    /** @test */
    function if_user_can_unvote()
    {
        $vote = factory(Vote::class)->create([
            'user_id' => $this->user->id,
            'post_id' => $this->post->id,
        ]);

        $this->post('api/vote', ['post_id' => $this->post->id])
             ->assertSuccessful()
             ->assertJson(['message' => trans('app.vote.unvote_message')]);

        $this->assertDatabaseMissing('votes',[
            'user_id' => $this->user->id,
            'post_id' => $this->post->id,
        ]);
    }

    /** @test */
    function if_vote_count_is_showed()
    {
        $vote = factory(Vote::class, 5)->create([
            'user_id' => $this->user->id,
            'post_id' => $this->post->id,
        ]);

        $this->get('api/vote?post_id='. $this->post->id)
             ->assertSuccessful()
             ->assertJson(['votes' => 5]);
    }

    /** @test */
    function if_user_vote_is_counted()
    {
        $other_user = factory(User::class)->create();

        $vote = factory(Vote::class, 5)->create([
            'user_id' => $other_user->id,
            'post_id' => $this->post->id,
        ]);

        factory(Vote::class)->create([
            'user_id' => $this->user->id,
            'post_id' => $this->post->id,
        ]);

        $this->get('api/vote?post_id='. $this->post->id)
             ->assertSuccessful()
             ->assertJson([
                'votes' => 6,
                'own_vote' => 1,
            ]);
    }
}
