<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Twilio\Rest\Client;

Route::get('clear-region', function(){
    request()->session()->forget('region');
    echo "Ready";
});

Route::view('/credit-application', 'credit.index');

Route::view('/appointment', 'appointment.index');

Route::get('/', 'HomeController@front')->name('index');

Route::get('/contact', 'HomeController@contact')->name('contact');

Route::match(['get','post', 'put'], '/gallery/{make_id?}', [
    'uses' => 'HomeController@filter',
    'as' => 'filter',
]);
Route::get('models', 'MakeController@getModels')->name('models');

Route::get('/sendMessage', function() {


    $sid = 'ACd3c815b21e5d33084e9c6319867f12a6';
    $token = '4a34b060fafbfd2f337ce88ca0441c07';
    $client = new Client($sid, $token);
    // +14704278471
    $client->messages->create(
    '+17863550502',

      array(
            'from' => '+15005550006',
            'body' => 'Thanks for your order! On a scale of 1-10 would you recommend ',
        ));
});


Route::prefix('playfair')->middleware(['auth','admin'])->group(function () {
    Route::view('/', 'home')->name('playfair');
    Route::view('/{any}', 'home')->name('playfairChilds')->where('any', '.*');
});

Route::prefix('dealer')->middleware(['auth', 'dealer'])->group(function () {
    Route::get('/', 'DealerController@front')->name('dealer');
    Route::get('/print-invoice/{id}', 'InvoiceController@print')->name('invoice.print');
    Route::get('/pdf-invoice/{id}', 'InvoiceController@pdf')->name('invoice.pdf');
    Route::get('/{any}', 'DealerController@front')->name('dealerChilds')->where('any', '.*');
});

Route::prefix('blog')->group(function(){
    Route::get('/', 'HomeController@blog')->name('blog');
    Route::get('/{any}', 'HomeController@blog')->name('blogChilds')->where('any', '.*');
});

Route::prefix('gallery')->group(function(){
    Route::get('/', 'HomeController@anouncement')->name('gallery');
    Route::get('/{any}', 'HomeController@anouncement')->name('galleryChilds')->where('any', '.*');
});

Route::view('/wish-list', 'wishlist.index');

//Payments
Route::prefix('payments')->middleware(['auth'])->group(function(){
    Route::get('/', 'PaymentController@index');
    Route::post('/subscribe', 'PaymentController@subscribe');
    Route::get('/success', 'PaymentController@paypalSuccess')->name('paypal.success');
    Route::get('/fails', 'PaymentController@paypalFails')->name('paypal.fails');
});
Route::get('payments/billing', 'PaymentController@billing');

Route::view('/subscription', 'subscription.index')->name('subscribe');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->group(function () {

    Route::get('user',['as' => 'user.index','uses' => 'UserController@index']);

    Route::get('userList',['as' => 'user.list','uses' => 'UserController@userList']);

    Route::get('cityList',['as' => 'city.list','uses' => 'UserController@cityList']);

    Route::get('stateList',['as' => 'state.list','uses' => 'UserController@stateList']);

    Route::get('userGroupList',['as' => 'userGroup.list','uses' => 'UserController@userGroupList']);

    Route::delete('user/{id}', ['as' => 'api.user.delete', 'uses' => 'UserController@delete']);


    Route::put('user/{id}', ['name' => 'admin.user.update', 'uses' => 'UserController@update']);




    Route::get('category',['as' => 'category.index','uses' => 'CategoryController@index']);

    Route::get('categories',['as' => 'category.list','uses' => 'CategoryController@categoryList']);

    Route::put('category/{id}', ['name' => 'admin.category.update', 'uses' => 'CategoryController@update']);

    Route::post('category', ['name' => 'admin.category.store', 'uses' => 'CategoryController@store']);
    Route::delete('category/{id}', ['as' => 'api.category.delete', 'uses' => 'CategoryController@delete']);


    //catEquipments
    Route::get('cat-equipament',['as' => 'catEquipment.index','uses' => 'CatEquipmentController@index']);

    Route::get('cat-equipmentList',['as' => 'get.CatEquipment','uses' => 'CatEquipmentController@catEquipmentList']);

    Route::put('cat-equipament/{id}', ['name' => 'admin.catEquipment.update', 'uses' => 'CatEquipmentController@update']);

    Route::post('cat-equipament', ['name' => 'admin.catEquipment.store', 'uses' => 'CatEquipmentController@store']);

    Route::delete('cat-equipament/{id}', ['as' => 'api.catEquipament.delete', 'uses' => 'CatEquipmentController@delete']);

    //models
    Route::get('model',['as' => 'model.index','uses' => 'ModelController@index']);

    Route::get('model-list',['as' => 'get.model','uses' => 'ModelController@modelList']);

    Route::put('model/{id}', ['name' => 'admin.model.update', 'uses' => 'ModelController@update']);

    Route::post('model', ['name' => 'admin.model.store', 'uses' => 'ModelController@store']);

    Route::delete('model/{id}', ['as' => 'api.model.delete', 'uses' => 'ModelController@delete']);

    //fuels
    Route::get('fuel',['as' => 'fuel.index','uses' => 'FuelController@index']);

    Route::get('fuel-list',['as' => 'get.fuel','uses' => 'FuelController@fuelList']);

    Route::put('fuel/{id}', ['name' => 'admin.fuel.update', 'uses' => 'FuelController@update']);

    Route::post('fuel', ['name' => 'admin.fuel.store', 'uses' => 'FuelController@store']);

    Route::delete('fuel/{id}', ['as' => 'api.fuel.delete', 'uses' => 'FuelController@delete']);


    Route::get('ad-manager-edit/{id}',['as' => 'adManager.show','uses' => 'AdManagerController@getAds']);

    Route::put('ad-manager-update/{id}', ['name' => 'admin.adManager.update', 'uses' => 'AdManagerController@update']);

    //Equipments
    Route::get('equipament',['as' => 'equipment.index','uses' => 'EquipmentController@index']);

    Route::get('equipmentList',['as' => 'get.equipment','uses' => 'EquipmentController@EquipmentList']);

    Route::put('equipament/{id}', ['name' => 'admin.equipment.update', 'uses' => 'EquipmentController@update']);

    Route::post('equipament', ['name' => 'admin.equipment.store', 'uses' => 'EquipmentController@store']);

    Route::delete('equipament/{id}', ['as' => 'api.equipament.delete', 'uses' => 'EquipmentController@delete']);

    //Transmissions
    Route::get('transmission',['as' => 'transmission.index','uses' => 'TransmissionController@index']);

    Route::get('transmissionList',['as' => 'get.transmission','uses' => 'TransmissionController@transmissionList']);

    Route::put('transmission/{id}', ['name' => 'admin.transmission.update', 'uses' => 'TransmissionController@update']);

    Route::post('transmission', ['name' => 'admin.transmission.store', 'uses' => 'TransmissionController@store']);

    Route::delete('transmission/{id}', ['as' => 'api.transmission.delete', 'uses' => 'TransmissionController@delete']);

    //BillingTerms
    Route::get('billingTerm',['as' => 'billingTerm.index','uses' => 'BillingTermController@index']);

    Route::get('billingTermList',['as' => 'get.billingTerm','uses' => 'BillingTermController@billingTermList']);

    Route::put('billingTerm/{id}', ['name' => 'admin.billingTerm.update', 'uses' => 'BillingTermController@update']);

    Route::post('billingTerm', ['name' => 'admin.billingTerm.store', 'uses' => 'BillingTermController@store']);

    Route::delete('billingTerm/{id}', ['as' => 'admin.billingTerm.delete', 'uses' => 'BillingTermController@delete']);

    //BlogCategories
    Route::get('blogCategory',['as' => 'blogCategory.index','uses' => 'BlogCategoryController@index']);

    Route::get('blogCategoryList',['as' => 'get.blogCategory','uses' => 'BlogCategoryController@blogCategoryList']);

    Route::put('blogCategory/{id}', ['name' => 'admin.blogCategory.update', 'uses' => 'BlogCategoryController@update']);

    Route::post('blogCategory', ['name' => 'admin.blogCategory.store', 'uses' => 'BlogCategoryController@store']);

    Route::delete('blogCategory/{id}', ['as' => 'admin.blogCategory.delete', 'uses' => 'BlogCategoryController@delete']);


    //Invoices
    Route::get('invoice',['as' => 'invoice.index','uses' => 'InvoiceController@index']);

    Route::get('InvoiceList',['as' => 'get.invoice','uses' => 'InvoiceController@invoiceList']);

    Route::put('invoice/{id}', ['name' => 'admin.invoice.update', 'uses' => 'InvoiceController@update']);

    Route::post('invoice', ['name' => 'admin.invoice.store', 'uses' => 'InvoiceController@store']);

    Route::delete('invoice/{id}', ['as' => 'admin.invoice.delete', 'uses' => 'InvoiceController@delete']);


    //BillingPayments
    Route::get('billingPayment',['as' => 'billingPayment.index','uses' => 'BillingPaymentController@index']);

    Route::get('billingPaymentList',['as' => 'get.billingPayment','uses' => 'BillingPaymentController@billingPaymentList']);

    Route::put('billingPayment/{id}', ['name' => 'admin.billingPayment.update', 'uses' => 'BillingPaymentController@update']);

    Route::post('billingPayment', ['name' => 'admin.billingPayment.store', 'uses' => 'BillingPaymentController@store']);

    Route::delete('billingPayment/{id}', ['as' => 'admin.billingPayment.delete', 'uses' => 'BillingPaymentController@delete']);

    //BillingServices
    Route::get('billingService',['as' => 'billingService.index','uses' => 'BillingServiceController@index']);

    Route::get('billingService/{id}',['as' => 'billingService.show','uses' => 'BillingServiceController@show']);

    Route::get('billingServiceList',['as' => 'get.billingService','uses' => 'BillingServiceController@list']);

    Route::put('billingService/{id}', ['name' => 'admin.billingService.update', 'uses' => 'BillingServiceController@update']);

    Route::post('billingService', ['name' => 'admin.billingService.store', 'uses' => 'BillingServiceController@store']);

    Route::delete('billingService/{id}', ['as' => 'admin.billingService.delete', 'uses' => 'BillingServiceController@delete']);


    //condition
    Route::get('condition',['as' => 'get.Condition','uses' => 'ConditionController@index']);

    Route::get('condition-list',['as' => 'get.condition','uses' => 'ConditionController@conditionList']);

    Route::put('condition/{id}', ['name' => 'admin.condition.update', 'uses' => 'ConditionController@update']);

    Route::post('condition', ['name' => 'admin.condition.store', 'uses' => 'ConditionController@store']);

    Route::delete('condition/{id}', ['as' => 'api.condition.delete', 'uses' => 'ConditionController@delete']);


    //makes
    Route::get('make',['as' => 'get.make','uses' => 'MakeController@index']);

    Route::get('makeList',['as' => 'make.list','uses' => 'MakeController@makeList']);

    Route::put('make/{id}', ['name' => 'admin.make.update', 'uses' => 'MakeController@update']);

    Route::post('make', ['name' => 'admin.make.store', 'uses' => 'MakeController@store']);

    Route::delete('make/{id}', ['as' => 'api.make.delete', 'uses' => 'MakeController@delete']);


    //drives
    Route::get('driver',['as' => 'get.driver','uses' => 'DriverController@index']);

    Route::get('driver-list',['as' => 'driver.list','uses' => 'DriverController@driverList']);

    Route::put('driver/{id}', ['name' => 'admin.driver.update', 'uses' => 'DriverController@update']);

    Route::post('driver', ['name' => 'admin.driver.store', 'uses' => 'DriverController@store']);

    Route::delete('driver/{id}', ['as' => 'api.driver.delete', 'uses' => 'DriverController@delete']);



    //dealer
    Route::get('dealer',['as' => 'get.dealer','uses' => 'DealerController@index']);

    Route::get('dealer/{id}',['as' => 'dealer.show','uses' => 'DealerController@show']);

    Route::get('dealerList',['as' => 'dealer.list','uses' => 'DealerController@dealerList']);

    Route::put('dealer/{id}', ['name' => 'admin.dealer.update', 'uses' => 'DealerController@update']);

    Route::post('dealer', ['name' => 'admin.dealer.store', 'uses' => 'DealerController@store']);

    Route::delete('dealer/{id}', ['as' => 'api.dealer.delete', 'uses' => 'DealerController@delete']);

    //socialMedia
    Route::get('social-media',['as' => 'get.socialMedia','uses' => 'SocialMediaController@index']);

    Route::get('social-mediaList',['as' => 'socialMedia.list','uses' => 'SocialMediaController@dealerList']);

    Route::put('social-media/{id}', ['name' => 'admin.socialMedia.update', 'uses' => 'SocialMediaController@update']);

    Route::post('social-media', ['name' => 'admin.socialMedia.store', 'uses' => 'SocialMediaController@store']);

    Route::delete('social-media/{id}', ['as' => 'api.socialMedia.delete', 'uses' => 'SocialMediaController@delete']);

    //dealerSocial
    Route::get('dealerSocial',['as' => 'get.dealerSocial','uses' => 'DealerSocialController@index']);

    Route::put('dealerSocial/{id}', ['name' => 'admin.dealerSocial.update', 'uses' => 'DealerSocialController@update']);

    Route::post('dealerSocial', ['name' => 'admin.dealerSocial.store', 'uses' => 'DealerSocialController@store']);

    Route::delete('dealerSocial/{id}', ['as' => 'api.dealerSocial.delete', 'uses' => 'DealerSocialController@delete']);


    //Color
    Route::get('color', ['as' => 'api.color.index', 'uses' => 'ColorController@index']);
    Route::get('colorList',['as' => 'dealer.list','uses' => 'ColorController@colorList']);
    Route::post('color', ['as' => 'api.color.store', 'uses' => 'ColorController@store']);
    Route::put('color/{id}', ['as' => 'api.color.update', 'uses' => 'ColorController@update']);
    Route::delete('color/{id}', ['as' => 'api.color.delete', 'uses' => 'ColorController@delete']);

    //bodyType
    Route::get('bodyType', ['as' => 'api.boduType.index', 'uses' => 'BodyTypeController@index']);

    Route::get('bodyType-list',['as' => 'bodyType.list','uses' => 'BodyTypeController@bodyTypeList']);

    Route::post('bodyType', ['as' => 'api.boduType.store', 'uses' => 'BodyTypeController@store']);
    Route::put('bodyType/{id}', ['as' => 'api.boduType.update', 'uses' => 'BodyTypeController@update']);
    Route::delete('bodyType/{id}', ['as' => 'api.boduType.delete', 'uses' => 'BodyTypeController@delete']);

    //SettingGroups
    Route::get('settingGroup', ['as' => 'api.settingGroup.index', 'uses' => 'SettingGroupController@index']);
    Route::post('settingGroup', ['as' => 'api.settingGroup.store', 'uses' => 'SettingGroupController@store']);
    Route::put('settingGroup/{id}', ['as' => 'api.settingGroup.update', 'uses' => 'SettingGroupController@update']);
    Route::delete('settingGroup/{id}', ['as' => 'api.settingGroup.delete', 'uses' => 'SettingGroupController@delete']);

    //Settings
    Route::get('setting', ['as' => 'api.setting.index', 'uses' => 'SettingController@index']);
    Route::get('setting/{key}', ['as' => 'api.setting.show', 'uses' => 'SettingController@show']);
    Route::post('setting', ['as' => 'api.setting.store', 'uses' => 'SettingController@store']);
    Route::put('setting/{id}', ['as' => 'api.setting.update', 'uses' => 'SettingController@update']);
    Route::put('settingAll/{id}', ['as' => 'api.settingAll.update', 'uses' => 'SettingController@updateAll']);
    Route::delete('setting/{id}', ['as' => 'api.setting.delete', 'uses' => 'SettingController@delete']);

    //Posts
    Route::post('post', ['as' => 'api.post.store', 'uses' => 'PostController@store']);
    Route::put('post/{id}', ['as' => 'api.post.update', 'uses' => 'PostController@update']);
    Route::delete('post/{id}', ['as' => 'api.post.delete', 'uses' => 'PostController@delete']);

    //Tags
    Route::get('tag', ['as' => 'api.tag.index', 'uses' => 'TagController@index']);
    Route::get('tagList', ['as' => 'api.tag.list', 'uses' => 'TagController@list']);
    Route::post('tag', ['as' => 'api.tag.store', 'uses' => 'TagController@store']);
    Route::put('tag/{id}', ['as' => 'api.tag.update', 'uses' => 'TagController@update']);
    Route::delete('tag/{id}', ['as' => 'api.tag.delete', 'uses' => 'TagController@delete']);

    Route::get('contact', ['as' => 'api.contact.index', 'uses' => 'ContactController@index']);
});

Route::prefix('api')->group(function(){
    Route::post('/contact', 'ContactController@store');

    //Blog routes
    Route::get('/post', 'PostController@index')->name('post.index');
    Route::get('/post/{slug}', 'PostController@show')->name('post.show');
    Route::get('/blogCategory', 'BlogCategoryController@list')->name('blogCategory.index');

    //Comments
    Route::get('/comment', 'CommentController@index')->name('comment.index');
    Route::post('/comment', 'CommentController@store')->name('comment.store');

    //Votes
    Route::get('/vote', 'VoteController@count')->name('vote.count');
    Route::post('/vote', 'VoteController@toggle')->name('vote.toggle');


    //ad-manager
    Route::get('ad-manager',['as' => 'adManager.index','uses' => 'AdManagerController@index']);

    Route::get('anouncements',['as' => 'anouncements.index','uses' => 'AdManagerController@anouncements']);


    Route::get('ad-manager-list',['as' => 'get.adManager','uses' => 'AdManagerController@adManagerlList']);

    Route::put('ad-manager/{id}', ['name' => 'admin.adManager.update', 'uses' => 'AdManagerController@update']);

    Route::post('ad-manager', ['name' => 'admin.adManager.store', 'uses' => 'AdManagerController@store']);

    Route::delete('ad-manager/{id}', ['as' => 'api.adManager.delete', 'uses' => 'AdManagerController@delete']);

    Route::get('/anouncement/{slug}', 'AdManagerController@detail')->name('anouncement.detail');

    //Invoice
    Route::get('/dealer-invoice/{dealer_id}', 'InvoiceController@dealerList')->name('dealer.invoice');

    //Wish Lists
    Route::get('wish-list',['as' => 'get.WishList','uses' => 'WishListController@index']);
    Route::post('wish-list', ['name' => 'admin.WishList.store', 'uses' => 'WishListController@store']);
    Route::delete('wish-list/{ads_manager_id}', ['as' => 'api.WishList.delete', 'uses' => 'WishListController@delete']);

    //Offers
    Route::get('offer',['as' => 'offer.index','uses' => 'OfferController@index']);
    Route::post('offer', ['name' => 'offer.store', 'uses' => 'OfferController@store']);

    //Review
    Route::get('review/{ads_manager_id}',['as' => 'review.average','uses' => 'ReviewController@average']);
    Route::post('review', ['name' => 'review.store', 'uses' => 'ReviewController@store']);
    Route::put('review/{id}', ['name' => 'review.update', 'uses' => 'ReviewController@update']);

    //Addresses
    Route::get('address',['as' => 'address.show','uses' => 'AddressController@show']);
    Route::post('address', ['name' => 'address.store', 'uses' => 'AddressController@store']);
    Route::put('address/{id}', ['name' => 'address.update', 'uses' => 'AddressController@update']);

    Route::post('credit-application', ['name' => 'api.credit.application', 'uses' => 'CreditController@store']);

});


// Localization
Route::get('/js/lan.js', function () {
    $strings = Cache::remember('lan.js', 0, function () {
        $lang = config('app.locale');
        $files   = glob(resource_path('lang/' . $lang . '/*.php'));
        $strings = [];

        foreach ($files as $file) {
            $name           = basename($file, '.php');
            $strings[$name] = require $file;
        }

        return $strings;
    });

    header('Content-Type: text/javascript');
    echo('window.i18n = ' . json_encode($strings) . ';');
    exit();
})->name('assets.lang');

//error vue
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('{key}', function($key){
    return view('default-page', compact('key'));
})->name('default.page');

Route::get('{any}',function(){
    return view('errors.404');
})->where('any','.*');
