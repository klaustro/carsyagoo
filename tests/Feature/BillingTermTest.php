<?php

namespace Tests\Feature;

use App\BillingTerm;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BillingTermTest extends TestCase
{
    use RefreshDatabase;

    protected $user, $category, $tags;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->actingAs($this->user);
    }

    /** @test */
    function if_billing_term_is_paginated()
    {
        $billing_terms = factory(BillingTerm::class, 4)->create();

        $this->get('admin/billingTerm?page=1&search=&orderBy=id&desc=true&rows=3')
             ->assertSuccessful()
             ->assertJsonFragment(['name' => $billing_terms[3]->name])
             ->assertJsonMissing(['name' => $billing_terms[0]->name]);

    }

    /** @test */
    function if_billing_term_is_finded()
    {
        $searched = factory(BillingTerm::class)->create([
            'name' => 'Searched billing term',
        ]);

        $billing_term = factory(BillingTerm::class)->create([
            'name' => 'weekly'
        ]);

        $this->get('admin/billingTerm?search=Searched&orderBy=id&desc=true')
             ->assertSuccessful()
             ->assertSee($searched->name)
             ->assertDontSee($billing_term->name);
    }


    /** @test */
    function if_billing_term_can_be_stored()
    {

        $response = $this->post('admin/billingTerm', [
            'draft' =>
                [
                    'name' => 'weekly',
                    'days' => 7,
                ]
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.billingTerm.store_message')]);

        $this->assertDatabaseHas('billing_terms',[
            'name' => 'weekly',
            'days' => 7,
        ]);

    }

    /** @test */
    function if_billing_term_can_be_updated()
    {
        $billing_term = factory(BillingTerm::class)->create();

        $this->put('admin/billingTerm/' . $billing_term->id, [
            'draft' =>
                [
                    'name' => 'weekly',
                    'days' => 7,
                ]
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.billingTerm.update_message')]);

        $this->assertDatabaseHas('billing_terms',[
            'name' => 'weekly',
            'days' => 7,
        ]);

    }

    /** @test */
    function if_billing_term_can_be_deleted()
    {
        $billing_term = factory(BillingTerm::class)->create();

        $this->delete('admin/billingTerm/' . $billing_term->id)
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.billingTerm.delete_message')]);

        $this->assertDatabaseMissing('billing_terms',[
            'name' => 'weekly',
            'days' => 7,
        ]);

    }
}
