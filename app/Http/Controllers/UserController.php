<?php

namespace App\Http\Controllers;

use App\City;
use App\State;
use App\User;
use App\UserGroup;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getAllUsers()
	{
		return response()->json( [
            'users' => User::search(request()->search)
                    ->orderBy('id','desc')
                    ->get(),
            'cities' => City::all(),
            'groups' => UserGroup::all(),
             ], 200);
	}

	public function getUsers()
	{
		$users = User::search(request()->search)
					->orderBy('id','desc')
					->paginate(5);

		return $categories;
	}

    public function userList()
    {
        $users = User::all();
        $users->map(function($user){
            $user->full_name = $user->full_name;
        });

        return $users;
    }

    public function cityList()
    {
        return City::where('state_id', request()->state_id)
        ->orderBy('name')
        ->get();
    }

    public function stateList()
    {
        return State::all();
    }

    public function userGroupList()
    {
        return UserGroup::all();
    }

    public function index()
    {
        return User::search(request()->search)
            ->orderBy('id','desc')
            ->paginate(5);
    }

	public function update($id)
    {
           // return request()->all();
        try {

            // $v = \Validator::make(request()->all(), [
            //     'name' => 'required',
            //     'number' => 'required|numeric',
            //     'transfer_key'    => 'required',
            // ]);

            // $errors = $v->errors();
            // $message=[];

            // foreach ($errors->all() as  $mess) {
            //     $message[]=$mess.'  ';
            // }

            // if ($v->fails())
            // {
            //     return ['message' => $message , 'status' => 0];
            // }
            $user = User::where('id',$id)
            ->update(request()->all());

         	return [
                    'message' => trans('app.user.update_message'),
                ];

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.store_error')], 500);
        }

    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->blocked_user = true;
        $user->save();
        return ['message' => trans('app.user.delete_message')];
    }

    public function authUser()
    {
        if (auth()->guest()) {
            return ['first_name' => 'guest'];
        }

        return auth()->user();
    }
}
