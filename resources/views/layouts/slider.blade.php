<div class="hero-area hidden hidden-sm hidden-xs">
    <!-- Start Hero Slider -->
    <div class="hero-slider heroflex flexslider clearfix" data-autoplay="yes" data-pagination="no" data-arrows="yes" data-style="fade" data-speed="7000" data-pause="yes">
        <ul class="slides">
            @foreach(getConfigStartWith('slider') as $slider)
            <li class="parallax" style="background-image:url({{ url($slider) }});"></li>
            @endforeach
        </ul>
    </div>
    <!-- End Hero Slider -->
</div>