<?php

use Illuminate\Database\Seeder;

class BillingTermTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\BillingTerm::class)->create([
            'name' => 'weekly',
            'days' => 7,
        ]);

        factory(App\BillingTerm::class)->create([
            'name' => 'monthly',
            'days' => 30,
        ]);

        factory(App\BillingTerm::class)->create([
            'name' => 'yearly',
            'days' => 365,
        ]);

        factory(App\BillingTerm::class)->create([
            'name' => 'daily',
            'days' => 1,
        ]);
    }
}
