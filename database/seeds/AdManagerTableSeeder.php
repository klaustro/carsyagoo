<?php

use App\AdManager;
use App\CarModel;
use App\Make;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class AdManagerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $makes = Make::whereIn('name', ['Audi', 'BMW', 'Chevrolet', 'Honda', 'Suzuki', 'Toyots', 'Volvo'])->get();

        $makes->each(function($make){
            $models = CarModel::where('make_id', $make->id)->get();
            $models->each(function($model) use($make){
                factory(AdManager::class)->create([
                    'make_id' => $make->id,
                    'model_id' => $model->id,
                    'bodytype_id' => rand(1, 13)
                ]);
            });
        });
    }
}

