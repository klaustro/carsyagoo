let state = {
    billingTerms: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading: false,
}

let getters = {
        findBillingTerm(state){
            return function(id){
                let billingTerm = state.billingTerms.find(color => billingTerm.id == id)
                return billingTerm;
            }
        },
}

let actions = {
    getBillingTerms(context, params){
        context.state.loading = true
        axios.get('/admin/billingTerm?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getBillingTerms', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    storeBillingTerm(context, payload){
        context.state.loading = true
        axios.post('/admin/billingTerm', payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    let newBillingTerm = {
                        id: response.data.id,
                        name: response.data.data.name,
                        days: response.data.data.days,
                    }
                    context.commit('storeBillingTerm', newBillingTerm)
                    Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                }

                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateBillingTerm(context, payload){
        context.state.loading = true
        axios.put('/admin/billingTerm/' + payload.id, payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                    context.commit('updateBillingTerm', payload)
                }
                context.state.loading = false

            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeBillingTerm(context, id){
        context.state.loading = true
        axios.delete('/admin/billingTerm/' + id)
            .then(response => {

                context.commit('removeBillingTerm', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                context.state.loading = false
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })

    },

    listBillingTerms(context){
        axios.get('/admin/billingTermList')
            .then(response => {
                context.commit('listBillingTerms', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getBillingTerms(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.billingTerms = data.data;
    },

    storeBillingTerm(state, newBillingTerm){
        state.billingTerms.unshift(newBillingTerm);
    },

    updateBillingTerm(state, {id, draft}){
        let index = state.billingTerms.findIndex(billingTerm => billingTerm.id == id);
        state.billingTerms.splice(index, 1, draft);
    },

    removeBillingTerm(state, id)    {
        let index = state.billingTerms.findIndex(billingTerm => billingTerm.id == id);
        state.billingTerms.splice(index, 1);
    },

    listBillingTerms(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}