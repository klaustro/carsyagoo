
<?php
use App\Make;
use Illuminate\Database\Seeder;


class MakeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
factory(Make::class)->create(['category_id' => 1, 'name' => 'Acura', 'image' => '/images/makes/acura.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Aston Martin', 'image' => '/images/makes/aston-martin.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Audi', 'image' => '/images/makes/audi.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Bentley', 'image' => '/images/makes/bentley.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'BMW', 'image' => '/images/makes/bmw.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Buick', 'image' => '/images/makes/buick.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Cadillac', 'image' => '/images/makes/cadillac.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Chevrolet', 'image' => '/images/makes/chevrolet.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Chrysler', 'image' => '/images/makes/chrysler.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Daewoo', 'image' => '/images/makes/daewoo.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Dodge', 'image' => '/images/makes/dodge.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Eagle', 'image' => '/images/makes/eagle.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Ferrari', 'image' => '/images/makes/ferrari.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Ford', 'image' => '/images/makes/ford.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Geo', 'image' => '/images/makes/geo.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'GMC', 'image' => '/images/makes/gmc.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Honda', 'image' => '/images/makes/honda.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Hummer', 'image' => '/images/makes/hummer.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Hyundai', 'image' => '/images/makes/hyundai.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Infiniti', 'image' => '/images/makes/infiniti.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Isuzu', 'image' => '/images/makes/isuzu.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Jaguar', 'image' => '/images/makes/jaguar.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Jeep', 'image' => '/images/makes/jeep.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Kia', 'image' => '/images/makes/kia.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Lamborghini', 'image' => '/images/makes/lamborghini.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'LandRover', 'image' => '/images/makes/landrover.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Lexus', 'image' => '/images/makes/lexus.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Lincoln', 'image' => '/images/makes/lincoln.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Mazda', 'image' => '/images/makes/alias.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Mercedes-Benz', 'image' => '/images/makes/mercedes-benz.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Mercury', 'image' => '/images/makes/mercury.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Mini', 'image' => '/images/makes/mini.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Mitsubishi', 'image' => '/images/makes/mitsubishi.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Nissan', 'image' => '/images/makes/nissan.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Oldsmobile', 'image' => '/images/makes/oldsmobile.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Plymouth', 'image' => '/images/makes/plymouth.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Pontiac', 'image' => '/images/makes/pontiac.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Porsche', 'image' => '/images/makes/porsche.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Saab', 'image' => '/images/makes/saab.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Saturn', 'image' => '/images/makes/saturn.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Scion', 'image' => '/images/makes/scion.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Subaru', 'image' => '/images/makes/subaru.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Suzuki', 'image' => '/images/makes/suzuki.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Toyota', 'image' => '/images/makes/toyota.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Volkswagen', 'image' => '/images/makes/volkswagen.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Volvo', 'image' => '/images/makes/volvo.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Citroen', 'image' => '/images/makes/citroen.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Fiat', 'image' => '/images/makes/fiat.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'GAZ', 'image' => '/images/makes/gaz.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Lada', 'image' => '/images/makes/lada.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Lancia', 'image' => '/images/makes/lancia.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Lotus', 'image' => '/images/makes/lotus.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Maybach', 'image' => '/images/makes/naybach.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Maserati', 'image' => '/images/makes/Maserati.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'MG', 'image' => '/images/makes/mg.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Opel', 'image' => '/images/makes/opel.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Peugeot', 'image' => '/images/makes/peugeot.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Proton', 'image' => '/images/makes/proton.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Renault', 'image' => '/images/makes/renault.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Rolls-Royce', 'image' => '/images/makes/rolls-royce.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Rover', 'image' => '/images/makes/rover.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Santana', 'image' => '/images/makes/santana.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Seat', 'image' => '/images/makes/seat.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Skoda', 'image' => '/images/makes/skoda.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Smart', 'image' => '/images/makes/smart.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Ssangyong', 'image' => '/images/makes/ssangyong.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Acura', 'image' => '/images/makes/acura.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Aston Martin', 'image' => '/images/makes/aston-martin.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Audi', 'image' => '/images/makes/audi.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Bentley', 'image' => '/images/makes/bentley.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'BMW', 'image' => '/images/makes/bmw.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Buick', 'image' => '/images/makes/buick.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Cadillac', 'image' => '/images/makes/cadillac.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Chevrolet', 'image' => '/images/makes/chevrolet.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Chrysler', 'image' => '/images/makes/chrysler.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Daewoo', 'image' => '/images/makes/daewoo.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Dodge', 'image' => '/images/makes/dodge.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Ferrari', 'image' => '/images/makes/ferrari.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Fiat', 'image' => '/images/makes/fiat.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Ford', 'image' => '/images/makes/ford.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Geo', 'image' => '/images/makes/geo.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'GMC', 'image' => '/images/makes/gmc.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Honda', 'image' => '/images/makes/honda.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Hummer', 'image' => '/images/makes/hummer.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Hyundai', 'image' => '/images/makes/hyundai.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Infiniti', 'image' => '/images/makes/infiniti.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Isuzu', 'image' => '/images/makes/isuzu.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Jaguar', 'image' => '/images/makes/jaguar.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Jeep', 'image' => '/images/makes/jeep.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Kia', 'image' => '/images/makes/kia.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Lamborghini', 'image' => '/images/makes/lamborghini.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'LandRover', 'image' => '/images/makes/landrover.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Lexus', 'image' => '/images/makes/lexus.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Lincoln', 'image' => '/images/makes/lincoln.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Lotus', 'image' => '/images/makes/lotus.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Maserati', 'image' => '/images/makes/Maserati.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Mazda', 'image' => '/images/makes/alias.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Mercedes-Benz', 'image' => '/images/makes/mercedes-benz.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Mercury', 'image' => '/images/makes/mercury.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Mini', 'image' => '/images/makes/mini.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Mitsubishi', 'image' => '/images/makes/mitsubishi.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Nissan', 'image' => '/images/makes/nissan.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Oldsmobile', 'image' => '/images/makes/oldsmobile.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Plymouth', 'image' => '/images/makes/plymouth.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Pontiac', 'image' => '/images/makes/pontiac.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Porsche', 'image' => '/images/makes/porsche.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Renault', 'image' => '/images/makes/renault.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Rolls-Royce', 'image' => '/images/makes/rolls-royce.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Saab', 'image' => '/images/makes/saab.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Saturn', 'image' => '/images/makes/saturn.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Scion', 'image' => '/images/makes/scion.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Smart', 'image' => '/images/makes/smart.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Subaru', 'image' => '/images/makes/subaru.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Suzuki', 'image' => '/images/makes/suzuki.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Toyota', 'image' => '/images/makes/toyota.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Volkswagen', 'image' => '/images/makes/volkswagen.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Volvo', 'image' => '/images/makes/volvo.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'RANGE ROVER', 'image' => '/images/makes/range-rover.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Ducaty', 'image' => '/images/makes/ducaty.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'RAM', 'image' => '/images/makes/ram.png']);
factory(Make::class)->create(['category_id' => 1, 'name' => 'Workhorse', 'image' => '/images/makes/workhorse.png']);

    }
}
