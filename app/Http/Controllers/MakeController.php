<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Make;
use App\CarModel;

class MakeController extends Controller
{
    public function index()
	{
        return Make::search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);
	}
    public function getModels(Request $request){
        if ($request->ajax()) {
            $models = CarModel::models($request->id);
            return response()->json($models);
        }
    }

    public function makeList()
    {
        $make = Make::filterCategory(request()->category_id)
        ->orderBy('name')
        ->get();

/*        $make->map(function($make){
            $make->anouncement_count = $make->anouncements->count();
        });*/

        return $make;

    }

	public function store()
    {

        try {

            $v = \Validator::make(request()->draft, [
                'name' => 'required',
                'alias' => 'required|max:255|unique:makes',
                'metakey' => 'required',
                'metadesc' => 'required',
                'category_id' => 'required|integer|exists:categories,id',
                'description' => 'required',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }

            if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);

                $folder = '/images/makes/';
                rename(public_path().'/'.request()->images[0]['url'],public_path(). $folder .request()->images[0]['url']);

                $make = Make::create([
                    'name' => request()->draft['name'],
                    'metakey' => request()->draft['metakey'],
                    'alias' => request()->draft['alias'],
                    'description' => request()->draft['description'],
                    'metadesc' => request()->draft['metadesc'],
                    'category_id' => request()->draft['category_id'],
                    'image' =>  $folder . request()->images[0]['url'],
                ]);

                return [
                    'message' => trans('app.make.store_message'),
                    'make' => $make
                ];

            }else{

                    $make = Make::create([
                        'name' => request()->draft['name'],
                        'metakey' => request()->draft['metakey'],
                        'alias' => request()->draft['alias'],
                        'description' => request()->draft['description'],
                        'metadesc' => request()->draft['metadesc'],
                        'category_id' => request()->draft['category_id'],

                    ]);

                return [
                    'message' => trans('app.make.store_message_error'),
                    'make' => $make
                ];
            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.store_error')], 500);
        }
    }

	public function update($id)
    {
        $v = \Validator::make(request()->draft, [
            'name' => 'required',
            'metakey' => 'required',
            'metadesc' => 'required',
            'category_id' => 'required|integer|exists:categories,id',
            'description' => 'required',
        ]);

        $errors = $v->errors();
        $message=[];

        foreach ($errors->all() as  $mess) {
            $message[]=$mess.'  ';
        }

        if ($v->fails())
        {
            return ['message' => $message , 'status' => 0];
        }

        try {

            if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);
                $folder = '/images/makes/';
                rename(public_path().'/'.request()->images[0]['url'],public_path() . $folder . request()->images[0]['url']);

                $make = Make::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'metakey' => request()->draft['metakey'],
                    'alias' => request()->draft['alias'],
                    'description' => request()->draft['description'],
                    'metadesc' => request()->draft['metadesc'],
                    'category_id' => request()->draft['category_id'],
                    'image' => $folder . request()->images[0]['url'],
                ]);

                return ['message' => trans('app.make.update_message')];

            }else{
                $make = Make::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'metakey' => request()->draft['metakey'],
                    'alias' => request()->draft['alias'],
                    'description' => request()->draft['description'],
                    'metadesc' => request()->draft['metadesc'],
                    'category_id' => request()->draft['category_id'],
                ]);

                return ['message' => trans('app.make.update_message_error')];
            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.update_error')], 500);
        }
    }

    public function delete($id)
    {
        $make = Make::destroy($id);

        return ['message' => trans('app.make.delete_message')];
    }

}
