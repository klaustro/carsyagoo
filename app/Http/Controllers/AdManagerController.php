<?php

namespace App\Http\Controllers;

use App\AdManager;
use App\AdManagerDetail;
use App\Scopes\AdManagerScope;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class AdManagerController extends Controller
{
    public function index()
	{
        return AdManager::with(['images', 'equipments'])
            ->filterDealer(auth()->user()->dealer->id)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->withoutGlobalScope(AdManagerScope::class)
            ->paginate(5);
	}
    public function show($id)
    {
        return AdManager::with('images')->find($id);
    }

    public function detail($id)
    {
        $anouncement = AdManager::with(['bodytype','model', 'transmission', 'fuel','model.make','images', 'condition','intColor','extColor','specifictColor','driver','dealer'])
          ->withoutGlobalScope(AdManagerScope::class)
          ->find($id);

        $relatedVehicles = AdManager::relateds($id)->with(['bodytype','model', 'transmission', 'fuel','model.make','images', 'condition','intColor','extColor','specifictColor','driver'])
        ->orderBy('id', 'desc')
        ->take(getConfig('related_rows'))->get();

        return [
            'anouncement' => $anouncement,
            'relatedVehicles' => $relatedVehicles,
        ];
    }

    public function getAds($id)
    {
        return AdManager::with('images')->find($id);
    }

    public function anouncements(){
        return AdManager::
            modelSearch(request()->model_id)
            ->bodytypeSearch(request()->bodytype_id)
            ->makeSearch(request()->make_id)
            ->zipcodeSearch(request()->zipcode)
            ->transmissionSearch(request()->transmission_id)
            ->fuelSearch(request()->fuel_id)
            ->priceSearch(request()->min_price, request()->max_price)
            ->yearSearch(request()->min_year, request()->max_year)
            ->mileageSearch(request()->min_mileage, request()->max_mileage)
            ->colorSearch(request()->ext_color)
            ->orderBy(request()->orderBy, request()->desc == true ? 'DESC' : 'ASC')
            ->byState(request()->state)
            ->specificModelFilter(request()->specific_model)
            ->byDistance(request()->latitude, request()->longitude, 20)
            ->paginate(request()->rows);
    }

    public function adManagerList()
    {
        return AdManager::all();
    }

    public function store()
    {
     $v = \Validator::make(request()->draft, [

                'category_id' => 'required|numeric',
                'make_id' => 'required|numeric',
                'model_id' => 'required|numeric',
                'condition_id' => 'required|numeric',
                'dealer_id' => 'required|numeric',
                'bodytype_id' => 'required|numeric',
                'driver_id' => 'required|numeric',
                'fuel_id' => 'required|numeric',
                'transmission_id' => 'required|numeric',
                'ext_color_id' => 'required|numeric',
                'int_color_id' => 'required|numeric',
                'specific_color' => 'required|numeric',
                'year' => 'required|numeric',
                'vincode' => 'required',
                'mileage' => 'required',
                'price' => 'required|numeric',
                'doors' => 'required',
                'seats' => 'required',
                'engine' => 'required',
                'status' => 'required',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }

            $ads_manager = AdManager::create(request()->draft);
            if (isset(request()->images['main'][0]['dataURL'])) {
                $image = request()->images['main'][0];
                $Base64Img = $image['dataURL'];
                $file_name = $image['upload']['filename'];
                $folder = "/images/ad_managers/main/";
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents($file_name, $Base64Img);
                rename(public_path().'/'.$file_name,public_path().$folder.$file_name);
                $ads_manager->main_image = $folder.$file_name;


            }
            $ads_manager->save();

            if (request()->images['images']) {
                foreach (request()->images['images'] as $image) {
                    if (isset($image['dataURL'])) {
                        $Base64Img = $image['dataURL'];
                        $file_name = $image['upload']['filename'];
                        list(, $Base64Img) = explode(';', $Base64Img);
                        list(, $Base64Img) = explode(',', $Base64Img);
                        $Base64Img = base64_decode($Base64Img);
                        $url_photo = "/images/ad_managers/detail/$file_name";
                        $file = file_put_contents($file_name, $Base64Img);
                        rename(public_path().'/'.$file_name,public_path() . $url_photo);
                    }
                    $images = AdManagerDetail::create([
                        'ads_manager_id' => $ads_manager->id,
                        'urlphoto' => $url_photo,
                        'state' => true,
                    ]);
                    $images->save();
                }
            }

            return [
                'message' => trans('app.adManager.store_message'),
                'id' => $ads_manager->id, 'data' => $ads_manager,
            ];
    }

    public function update($id)
    {
        $v = \Validator::make(request()->draft, [
            'category_id' => 'required|numeric',
            'make_id' => 'required|numeric',
            'model_id' => 'required|numeric',
            'condition_id' => 'required|numeric',
            'dealer_id' => 'required|numeric',
            'bodytype_id' => 'required|numeric',
            'driver_id' => 'required|numeric',
            'fuel_id' => 'required|numeric',
            'transmission_id' => 'required|numeric',
            'ext_color_id' => 'required|numeric',
            'int_color_id' => 'required|numeric',
            'specific_color' => 'required|numeric',
            'year' => 'required|numeric',
            'vincode' => 'required',
            'mileage' => 'required',
            'price' => 'required|numeric',
            'doors' => 'required',
            'seats' => 'required',
            'engine' => 'required',
            'status' => 'required',
            'main_image' => 'required',
        ]);

        $errors = $v->errors();
        $message=[];

        foreach ($errors->all() as  $mess) {
            $message[]=$mess.'  ';
        }

        if ($v->fails())
        {
            return ['message' => $message , 'status' => 0];
        }

        $ads_manager = AdManager::find($id);
        $ads_manager->fill(request()->draft);
        if (isset(request()->images['main'][0]['dataURL'])) {
            $image = request()->images['main'][0];
            $Base64Img = $image['dataURL'];
            $file_name = $image['upload']['filename'];
            list(, $Base64Img) = explode(';', $Base64Img);
            list(, $Base64Img) = explode(',', $Base64Img);
            $Base64Img = base64_decode($Base64Img);
            $main_image = "/images/ad_managers/main/$file_name";
            $file = file_put_contents($file_name, $Base64Img);
            rename(public_path() . '/' . $file_name, public_path() . $main_image);
            $ads_manager->main_image = $main_image;
        }
        $ads_manager->save();

        //Delete all images
        AdManagerDetail::where('ads_manager_id', $id)->delete();
        //Add images
        if (request()->images) {
            foreach (request()->images['images'] as $image) {
                if (isset($image['dataURL'])) {
                    $Base64Img = $image['dataURL'];
                    $file_name = $image['upload']['filename'];
                    list(, $Base64Img) = explode(';', $Base64Img);
                    list(, $Base64Img) = explode(',', $Base64Img);
                    $Base64Img = base64_decode($Base64Img);
                    $url_photo = "/images/ad_managers/detail/$file_name";
                    $file = file_put_contents($file_name, $Base64Img);
                    rename(public_path().'/'.$file_name,public_path() . $url_photo);
                } else {
                    $url_photo = $image['name'];
                }

                $images = AdManagerDetail::create([
                    'ads_manager_id' => $id,
                    'urlphoto' => $url_photo,
                    'state' => true,
                ]);
                $images->save();
            }
        }

        return ['message' => trans('app.adManager.update_message')];
    }

    public function delete($id)
    {
        $adManager = AdManager::destroy($id);

        return ['message' => trans('app.adManager.delete_message')];
    }
}
