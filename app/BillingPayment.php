<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingPayment extends Model
{
    protected $fillable = [
        'dealer_id', 'type', 'cardnumber', 'expire_month', 'expire_year', 'first_name', 'last_name', 'cvc',
        'zipcode','account_number','routing_number','primary','status'
    ];

    public function dealer()
    {
        return $this->BelongsTo('App\Dealer');
    }


    public function scopeSearch($query, $target)
    {

        if ($target != '') {
            return $query->where('cardnumber', 'like', "%$target%")
                 ->orWhere('type', 'like', "%$target%")
                 ->orWhere('expire_month', 'like', "%$target%")
                 ->orWhere('expire_year', 'like', "%$target%")
                 ->orWhere('first_name', 'like', "%$target%")
                 ->orWhere('last_name', 'like', "%$target%")
                 ->orWhere('cvc', 'like', "%$target%")
                 ->orWhere('zipcode', 'like', "%$target%")
                 ->orWhere('account_number', 'like', "%$target%");
        }
    }

    public function scopeByDealer($query, $dealer_id)
    {
        if ($dealer_id != '') {
            return $query->where('dealer_id', $dealer_id);
        }
    }
    //Mutators
    public function setCardnumberAttribute($value)
    {
        if (!isset($this->attributes['cardnumber'])) {
            $this->attributes['cardnumber'] = $value != '' ? encrypt($value) : '';
        }
    }


    public function setAccountNumberAttribute($value)
    {
        if (! isset($this->attributes['account_number'])) {
            $this->attributes['account_number'] = $value != '' ? encrypt($value) : '';
        }
    }

    public function setRoutingNumberAttribute($value)
    {
        if (! isset($this->attributes['routing_number'])) {
            $this->attributes['routing_number'] = $value != '' ? encrypt($value) : '';
        }
    }

    //Getters
    public function getDecryptedCardNumberAttribute()
    {
        if (! isset($this->cardnumber)) {
            return '';
        }
        return decrypt($this->cardnumber);
    }

    public function getHideCardNumberAttribute()
    {
        return $this->lastFourDigits($this->cardnumber);
    }

    public function getHideAccountNumberAttribute()
    {
        return $this->lastFourDigits($this->account_number);
    }

    public function getHideRoutingNumberAttribute()
    {
        return $this->lastFourDigits($this->routing_number);
    }


    protected function lastFourDigits($value)
    {
        if ($value == '') {
            return '';
        }
        $decrypt = decrypt($value);
        $length = strlen($decrypt);
        return str_repeat("*", $length - 4) . substr($decrypt, $length - 4, $length) ;
    }
}
