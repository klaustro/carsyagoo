@extends('layouts.app')
@section('content')
<!-- Start Body Content -->
<div class="main" role="main">
    <div id="content" class="content full">
        <div class="container">
            <div class="listing-header margin-40">
                {!! getConfig('appointment_intro') !!}
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-8">
                    <contact-form subject="Book an Appointment"></contact-form>
                    <div class="clearfix"></div>
                    <div id="message"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection