<?php

use App\BillingService;
use App\Dealer;
use Faker\Generator as Faker;

$factory->define(App\Payment::class, function (Faker $faker) {
    return [
        'dealer_id' => function(){
            return firstOrFactory(Dealer::class);
        },
        'billing_service_id' => function(){
            return firstOrFactory(BillingService::class);
        },
        'result_id' => $faker->ean13,
        'type' => $faker->randomElement(['credit_card', 'paypal']),
        'transaction_id' => $faker->ean13,
        'state' => $faker->randomElement(['approved', 'created']),
        'invoice' => uniqid(),
        'currency' => $faker->currencyCode,
        'tax' => $faker->randomFloat(2, 0, $max = 5),
        'price' => $faker->randomFloat(2, 10, 50),
        'response' => $faker->paragraph,
    ];
});
