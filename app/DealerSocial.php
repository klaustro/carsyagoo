<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DealerSocial extends Model
{
    protected $fillable = [
        'dealer_id','socialname', 'icon','link','status'
    ];

    public function dealer()
    {
        return $this->BelongsTo('App\Dealer');
    }

    public function scopeSearch($query, $target)
    {

        if ($target != '') {
            return $query->where('socialname', 'like', "%$target%")
            		->orWhere('link', 'like', "%$target%")
                    ->orWhereHas('dealer', function ($query) use ($target) {
                        $query->Where('dealer_shortname', 'like',"%$target%");
                    });
        }
    }

    public function scopeByDealer($query, $dealer_id)
    {
        if ($dealer_id != '') {
            return $query->where('dealer_id', $dealer_id);
        }
    }
}

