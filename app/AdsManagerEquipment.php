<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdsManagerEquipment extends Model
{
    public $table = 'ads_manager_equipment';

    public $timestamps = false;
}
