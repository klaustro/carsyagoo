import BootstrapVue from 'bootstrap-vue'
import wysiwyg from "vue-wysiwyg";
import VueGallery from 'vue-gallery';
import VueSweetalert2 from 'vue-sweetalert2';
import _ from 'lodash'
import Toasted from 'vue-toasted';
import VueFormWizard from 'vue-form-wizard'
import Multiselect from 'vue-multiselect'
import VueGoogleAutocomplete from 'vue-google-autocomplete'
import Carousel3d from 'vue-carousel-3d';
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
import VueCarousel from 'vue-carousel';
import SweetModal from 'sweet-modal-vue/src/plugin'
import Rate from 'vue-rate';
import ProductZoomer from 'vue-product-zoomer'
import PayPal from 'vue-paypal-checkout'

// import Viewer from 'v-viewer'


//Vue Awesome Swiper
Vue.use(VueAwesomeSwiper, /* { default global options } */)

//Vue carousel
Vue.use(Carousel3d);
Vue.use(VueCarousel);

//Bootstrap Vue
Vue.use(BootstrapVue);

//Wysiwyg
Vue.use(wysiwyg, {  maxHeight: "200px" })

//VueGallery
Vue.component('vueGallery', VueGallery)
Vue.use(require('@websanova/vue-upload'))

//Swal alerts
Vue.use(VueSweetalert2)

//Toast alerts
Vue.use(Toasted,  {duration: 3000, iconPack : 'fontawesome', theme: 'outline'})

//Vue form Wizard
Vue.use(VueFormWizard)

//Vue Moment
Vue.use(require('vue-moment'))

//Vue multiselect
Vue.component('multiselect', Multiselect)

//Vue Google Autocomplete
Vue.component('vue-google-autocomplete', VueGoogleAutocomplete)

//Sweet Modal
Vue.use(SweetModal)

//Rate
Vue.use(Rate)

//Carousel
Vue.use(ProductZoomer)

//Paypal Checkout
Vue.component('paypal-checkout', PayPal)