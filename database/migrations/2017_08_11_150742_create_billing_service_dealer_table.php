<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingServiceDealerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_service_dealer', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('dealer_id')->references('id')->on('dealers');
            $table->foreign('dealer_id')->references('id')->on('dealers')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->unsignedInteger('billing_service_id')->reference('id')->on('billing_services');
            $table->foreign('billing_service_id')->references('id')->on('billing_services')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->date('expires');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_service_dealer');
    }
}
