<script src="{{url('js/app.js')}}"></script>
<script src="{{url('/assets/js/jquery-2.0.0.min.js')}}"></script>
<script src="{{ url('assets/js/prettyphoto.js') }}"></script> <!-- PrettyPhoto Plugin -->
<script src="{{ url('assets/js/ui-plugins.js') }}"></script> <!-- UI Plugins -->
<script src="{{ url('assets/js/helper-plugins.js') }}"></script> <!-- Helper Plugins -->
<script src="{{ url('assets/js/owl.carousel.min.js') }}"></script> <!-- Helper Plugins -->
<script src="{{ url('assets/js/password-checker.js') }}"></script> <!-- Password Checker -->
<script src="{{ url('/assets/js/bootstrap.js')}}"></script> <!-- All Scripts -->
<script src="{{ url('assets/js/init.js')}}"></script> <!-- All Scripts -->
<script src="{{ url('assets/js/jquery.flexslider.js') }}"></script> <!-- FlexSlider -->
@stack('footer_scripts')