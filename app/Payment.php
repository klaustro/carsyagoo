<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['dealer_id', 'result_id', 'transaction_id', 'state', 'currency', 'tax', 'price', 'response',];
}
