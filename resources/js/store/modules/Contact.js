let state = {
    data: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    loading: false,
}

let actions = {
    getContacts(context, params){
        context.state.loading = true
        axios.get('/admin/contact?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc + '&rows=' + params.rows)
            .then(response => {
                context.commit('getContacts', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getContacts(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.data = data.data;
    },
}

export default {
    state,
    actions,
    mutations
}