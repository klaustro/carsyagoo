<?php

namespace App;

use App\Scopes\AdManagerScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Dealer extends Model
{
    protected $fillable = ['user_id', 'dealer_shortname', 'dealer_EIN', 'dealer_manager',
        'sunbizzurl', 'latitude', 'longitude' , 'website', 'original_phone',
        'redirect_phone', 'fax', 'mission','vision','overview','about','logo_icon','biographies',
        'logo_image', 'logo_square', 'header_image', 'photo_image', 'mail_receive','status'];

    protected $appends = ['full_name', 'city', 'state', 'zipcode', 'address', 'phone',];

    public function images()
    {
        return $this->hasMany(DealerImage::class);
    }

    public function ads()
    {
        return $this->hasMany(AdManager::class)->withoutGlobalScope(AdManagerScope::class);
    }

    public function services()
    {
        return $this->belongsToMany(BillingService::class)->withPivot('expires');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //Attributes
    public function getActiveAttribute()
    {
        return $this->services->count() > 0
            ? $this->services->first()->pivot->expires > Carbon::now()
            : false;
    }

    public function getServiceAttribute()
    {
        return $this->services->count() > 0
            ? $this->services->first()
            : null;
    }

    public function getFullNameAttribute()
    {
        return $this->user->first_name . ' ' . $this->user->last_name;
    }

    public function getCityAttribute()
    {
        if (! $this->user->address) {
            return null;
        }
            return $this->user->address->locality;
    }

    public function getStateAttribute()
    {
        if (! $this->user->address) {
            return null;
        }
        return $this->user->address->state;
    }

    public function getZipcodeAttribute()
    {
        if (! $this->user->address) {
            return null;
        }
        return $this->user->address->postal_code;
    }

    public function getLatitudeAttribute()
    {
        if (! $this->user->address) {
            return null;
        }
        return $this->user->address->latitude;
    }

    public function getLongitudeAttribute()
    {
        if (! $this->user->address) {
            return null;
        }
        return $this->user->address->longitude;
    }

    public function getAddressAttribute()
    {
        if (! $this->user->address) {
            return null;
        }
        return $this->user->address->street_number . ' ' . $this->user->address->route;
    }

    public function getPhoneAttribute()
    {
        return $this->user->phone;
    }
    //Scopes

     /**
     * Method to search by any column
     * @param  Query $query
     * @param  string $target [description]
     * @return Query
     */
    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->
                where('id', $target)
                ->orWhere('latitude', 'like', "%$target%")
                ->orWhere('longitude', 'like', "%$target%")
                ->orWhereHas('user', function ($query) use ($target) {
                    $query->Where('first_name', 'like',"%$target%")
                        ->orWhere('last_name', 'like',"%$target%")
                        ->orWhere('login', 'like',"%$target%")
                        ->orWhere('zipcode', 'like',"%$target%");
                });

        }
    }
}
