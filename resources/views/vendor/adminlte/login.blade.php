@extends('layouts.app')


@section('content')
<div class="main" role="main">
    <div id="content" class="content full">
        <div class="container">
            <div class="listing-header margin-40">
                <h2>Login</h2>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <form action="{{ url(route('login')) }}" method="post">
                        {!! csrf_field() !!}
                        <div class="form-group has-feedback {{ $errors->has('login') ? 'has-error' : '' }}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" name="login" class="form-control" value="{{ old('email') }}"
                                   placeholder="{{ trans('adminlte::adminlte.login') }}">
                            </div>
                            @if ($errors->has('login'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('login') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                <input type="password" name="password" class="form-control"
                                   placeholder="{{ trans('adminlte::adminlte.password') }}">
                            </div>

                            @captcha

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block">
                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                            </span>
                        @endif
                        <div class="row">
                            <div class="col-xs-7">
                                <div class="checkbox icheck">
                                    <label>
                                        <input type="checkbox" name="remember"> {{ trans('adminlte::adminlte.remember_me') }} &nbsp
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <input type="submit" class="btn btn-primary" value="{{ trans('adminlte::adminlte.sign_in') }}">
                            </div>
                        </div>
                    </form>
            <div class="auth-links">
                <a href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}"
                   class="text-center"
                >{{ trans('adminlte::adminlte.i_forgot_my_password') }}</a>
                <br>
                @if (config('adminlte.register_url', 'register'))
                    <a href="{{ url(config('adminlte.register_url', 'register')) }}"
                       class="text-center"
                    >{{ trans('adminlte::adminlte.register_a_new_membership') }}</a>
                @endif
            </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection