@extends('layouts.app')
@push('slider')
    @include('layouts.slider')
    <!-- Utiity Bar -->
    <!-- Start Body Content -->
@endpush
@section('content')
    @include('layouts.utilityBar')
  <div id="content" class="content full padding-b0">
    <div class="container">
        <!-- Welcome Content and Services overview -->
      <div class="row">
          {!! getConfig('welcome_section') !!}
          </div>
          <div class="spacer-75"></div>
          <!-- Recently Listed Vehicles -->
          <section class="listing-block recent-vehicles">
              <div class="listing-header">
                  <h3>Latest Cars Listed</h3>
              </div>
              <div class="listing-container">
                  <div class="carousel-wrapper">
                      <div class="row">
                          <!-- <ul class="owl-carousel carousel-fw" id="vehicle-slider" data-columns="4" data-autoplay="" data-pagination="yes" data-arrows="no" data-single-item="no" data-items-desktop="4" data-items-desktop-small="3" data-items-tablet="2" data-items-mobile="1"> -->
                              @if (!empty($footer['recentlyVehicles']))
                                  @foreach ($footer['recentlyVehicles'] as $recently)
                                      <div class="col-md-3 col-sm-12">
                                        <div class="scroll-latests" style="height: 350px; overflow: auto;">
                                          <a href="{{ url('gallery/anouncement/' . $recently->id) }}" class="media-box">
                                          <img src="{{ asset($recently->main_image) }}" alt=""></a>
                                          <div class="vehicle-block-content">
                                            <h5 class="vehicle-title"><a href="{{ url('gallery/anouncement/' . $recently->id) }}">
                                            {{$recently->year}} {{$recently->make_name}}  {{$recently->model_name}} {{$recently->specific_model}} </a></h5>
                                            <span class="vehicle-meta">by {{$recently->dealer->full_name}} <a href="{{'tel:+' . $recently->dealer->original_phone }}" style="color:blue;">{{ $recently->dealer->original_phone }}</a> </span>
                                            <span class="vehicle-cost">${{number_format($recently->price, 0)}}</span>
                                          </div>
                                        </div>
                                      </div>
                                  @endforeach
                              @endif
                          <!-- </ul> -->
                      </div>
                  </div>
              </div>
          </section>
          <div class="spacer-60"></div>
          <div class="row">
              <!-- Latest News -->
              <div class="col-md-12 col-sm-12">
                  @include('blog.posts')
                  <div class="spacer-40"></div>
              </div>
              <!-- Latest Reviews -->
          </div>
    </div>
    <div class="spacer-50"></div>
    <div class="lgray-bg make-slider">
      <div class="container">
      <!-- @include('layouts.makes') -->
      </div>
    </div>
  </div>
@stop