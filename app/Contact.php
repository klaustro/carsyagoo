<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['subject', 'first_name', 'last_name', 'email', 'phone', 'message', ];

    /**
     * Method to search by any column
     * @param  Query $query
     * @param  string $target [description]
     * @return Query
     */
    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->
                where('id', $target)
                ->orWhere('subject', 'like', "%$target%")
                ->orWhere('first_name', 'like', "%$target%")
                ->orWhere('last_name', 'like', "%$target%")
                ->orWhere('email', 'like', "%$target%")
                ->orWhere('phone', 'like', "%$target%")
                ->orWhere('message', 'like', "%$target%");
        }
    }
}
