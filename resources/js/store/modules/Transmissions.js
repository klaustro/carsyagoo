let state = {
    transmissions: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading:false
}

let getters = {
        findTransmission(state){
            return function(id){
                let Transmission = state.transmissions.find(Transmission => Transmission.id == id)
                return Transmission;
            }
        },
}

let actions = {
    getTransmissions(context, params){
        context.state.loading = true
        axios.get('/admin/transmission?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc )
            .then(response => {
                context.commit('getTransmissions', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    storeTransmission(context, payload){
        context.state.loading = true
        axios.post('/admin/transmission', payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    if (payload.draft.iconimage != '') {
                          payload.draft.iconimage= '/images/transmissions/'+payload.draft.iconimage
                    console.log(payload.draft.iconimage)
                    }
                    let newTransmission = {
                        id: response.data.data.id,
                        name: payload.draft.name,
                        code: payload.draft.code,
                        iconimage: payload.draft.iconimage,
                        category_id: payload.draft.category_id,
                        status: payload.draft.status,
                    }
                    Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                    context.commit('storeTransmission', newTransmission)
                }
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateTransmission(context, payload){
        context.state.loading = true
        axios.put('/admin/transmission/' + payload.id, payload)
            .then(response => {
              if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

              }else{
                if (payload.draft.image != '') {
                  payload.draft.iconimage= '/images/transmissions/'+payload.draft.iconimage
                }
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateTransmission', payload)
              }
              context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeTransmission(context, id){
        context.state.loading = true
        axios.delete('/admin/transmission/' + id)
            .then(response => {
                context.commit('removeTransmission', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'info'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })

    },

    listTransmissions(context){
        axios.get('/admin/transmissionList')
            .then(response => {
                context.commit('listTransmissions', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getTransmissions(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.transmissions = data.data;
    },

    storeTransmission(state, newTransmission){
        state.transmissions.unshift(newTransmission);
    },

    updateTransmission(state, {id, draft}){
        let index = state.transmissions.findIndex(Transmission => Transmission.id == id);
        state.transmissions.splice(index, 1, draft);
    },

    removeTransmission(state, id)    {
        let index = state.transmissions.findIndex(Transmission => Transmission.id == id);
        state.transmissions.splice(index, 1);
    },

    listTransmissions(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}