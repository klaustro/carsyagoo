<?php

namespace App;

use App\Dealer;
use App\Notifications\BillingDealer;
use App\Payment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use Omnipay\Common\CreditCard;
use Omnipay\Omnipay;
use PayPal\Exception\PayPalConnectionException;

/**
 * Class to process payments with paypal
 */
class Charge
{
    private $dealer_id;
    private $method;
    private $credit_card;
    private $items;


    function __construct($dealer_id, $method, $credit_card = null, $items)
    {
        $this->dealer_id = $dealer_id;
        $this->method = $method;
        $this->credit_card = $credit_card;
        $this->items = $items;
    }

    public function payflow()
    {
        $gateway = Omnipay::create('Payflow_Pro');

        $gateway->initialize([
            'username'       => env('PAYPAL_PAYFLOW_USERNAME'),
            'password'       => env('PAYPAL_PAYFLOW_PASSWORD'),
            'vendor'         => env('PAYPAL_PAYFLOW_VENDOR'),
            'partner'        => 'Paypal',
            'testMode'       => env('PAYPAL_PAYFLOW_TEST_MODE'), // Or false for live transactions.
        ]);

        $card = new CreditCard([
            'firstName'    => $this->credit_card['first_name'],
            'lastName'     => $this->credit_card['last_name'],
            'number'       => $this->credit_card['cardnumber'],
            'expiryMonth'  => $this->credit_card['expire_month'],
            'expiryYear'   => $this->credit_card['expire_year'],
            'cvv'          => $this->credit_card['cvc'],
        ]);

        $taxes = 0;
        $purchase = 0;
        foreach ($this->items as $key => $item) {
            $purchase += $item['price'] * $item['quantity'];
            $taxes += $item['tax'];
        }

        $transaction = $gateway->purchase([
            'amount'                   => $purchase,
            'currency'                 => 'USD',
            'card'                     => $card,
        ]);

        try {
            $response = $transaction->send();
            if ($response->isSuccessful()) {
                \Log::info('Purchase transaction was successful!');
                $sale_id = $response->getTransactionReference();
                \Log::info('Transaction reference = ' . $sale_id);

                $this->storePayment($response, $purchase, $taxes);

                return [
                    'state' => $response->getMessage(),
                    'transaction_id' => $response->getTransactionReference(),
                ];
            } else {
                \Log::info('Purchase transaction error!');
            }

        } catch (\Exception $e) {
            \Log::info('Purchase transaction error!');
            return ["error" => 'Purchase transaction error!'];
        }

    }

    public function expressCheckout()
    {
        $paypal_response = request()->paypal_response;

        $response = [
            'orderID' => $paypal_response['orderID'],
            'payerID' => $paypal_response['payerID'],
            'paymentID' => $paypal_response['paymentID'],
            'transaction_id' => $paypal_response['paymentID'],
            'paymentToken' => $paypal_response['paymentToken'],
            'paymentToken' => $paypal_response['paymentToken'],
            'state' => 'Approved',
        ];

        $taxes = 0;
        $purchase = 0;
        foreach ($this->items as $key => $item) {
            $purchase += $item['price'] * $item['quantity'];
            $taxes += $item['tax'];
        }

        $this->storePayment($response, $purchase, $taxes, true);
        return $response;
    }
    protected function storePayment($response, $purchase, $taxes, $paypal = false)
    {
        $token = uniqid();
        //Save payment in DB
        $payment = new Payment;
        $payment->dealer_id = $this->dealer_id;
        $payment->billing_service_id = $this->items[0]['id'];
        $payment->type = $this->method;
        $payment->invoice = $token;
        $payment->currency = 'USD';
        $payment->tax = $taxes;
        $payment->price = $purchase;
        $payment->response = json_encode($response);

        if ($paypal) {
            $payment->result_id = $response['payerID'];
            $payment->transaction_id = $response['orderID'];
            $payment->state = 'Approved';
        } else {
            $payment->result_id = $response->getTransactionReference();
            $payment->transaction_id = $response->getTransactionReference();
            $payment->state = $response->getMessage();
        }

        $payment->save();

        $user = Dealer::find($this->dealer_id)->user;

        //Store Invoice
        $data = [
            'dealer_id' => $user->dealer->id,
            'billing_service_id' => $this->items[0]['id'],
            'subtotal' => $purchase,
            'tax' => $taxes,
            'total' => $purchase + $taxes,
            'date' => Carbon::now(),
            'token' => $token,
            'note' => '',
        ];
        $invoice = $this->storeInvoice($data);

        Notification::send($user, new BillingDealer($invoice));
    }

    protected function storeInvoice($data)
    {
        $invoice = Invoice::create($data);
        $invoice->save();

        return $invoice;
    }
}