<?php

namespace App;

use App\BillingService;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'dealer_id','billing_service_id','date', 'subtotal','tax','total', 'token', 'note'
    ];


    public function dealer()
    {
        return $this->BelongsTo('App\Dealer');
    }

    public function billingService()
    {
        return $this->BelongsTo(BillingService::class);
    }

    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->where('tax', 'like', "%$target%")
                         ->orWhere('total', 'like', "%$target%")
                         ->orWhere('note', 'like', "%$target%")
                         ->orWhere('subtotal', 'like', "%$target%")
                         ->orWhereHas('dealer', function ($query) use ($target) {
                            $query->Where('dealer_shortname', 'like',"%$target%");
                          })
                         ->orWhereHas('billingService', function ($query) use ($target) {
                            $query->Where('name', 'like',"%$target%");
                          })
                         ;
        }
    }

    public function scopeFilterDealer($query, $dealer_id)
    {
        if ($dealer_id != '') {
            return $query->where('dealer_id', $dealer_id);
        }
    }
}
