<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vote;

class VoteController extends Controller
{

    public function count()
    {
        $votes = Vote::where('post_id', request()->post_id)->get();
        $own_vote = Vote::where(['post_id' => request()->post_id, 'user_id' => auth()->user()->id])->get()->count();


        return [
            'votes' => $votes->count(),
            'own_vote' => $own_vote,
        ];
    }

    /**
     * Method to Vote/Unvote for a Post
     * @return Response
     */
    public function toggle()
    {
        $vote = Vote::where(['post_id' => request()->post_id, 'user_id' => auth()->user()->id])
            ->first();

        if ($vote) {
            $vote->delete();
            return [
                'message' => trans('app.vote.unvote_message'),
                'vote' => -1,
            ];
        } else {
            $vote = new Vote();
            $vote->post_id = request()->post_id;
            $vote->user_id = auth()->user()->id;
            $vote->save();
            return [
                'message' => trans('app.vote.vote_message'),
                'vote' => 1,

            ];
        }
    }
}
