@extends('layouts.app')
@section('content')
<!-- Start Body Content -->
<div class="main" role="main">
    <div id="content" class="content full">
        <div class="container">
            <div class="listing-header margin-40">
                {!! getConfig('credit_intro') !!}
            </div>
            <div class="row">
                    <credit-application :ads_id="{{ json_encode(request()->ads_id) }}"></credit-application>
                    <div class="clearfix"></div>
                    <div id="message"></div>
            </div>
        </div>
    </div>
</div>
@endsection