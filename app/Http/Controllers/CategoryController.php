<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Carbon\Carbon;
use Image;

class CategoryController extends Controller
{

    public function index()
    {
        return [
            'categories'=>Category::search(request()->search)->orderBy('id','desc')
            ->paginate(5),
            'categoryList'=> Category::all()
        ];
    }

    public function categoryList()
    {
        return Category::all();
    }

	public function store()
    {
        $v = \Validator::make(request()->all(), [
                'name' => 'required',
                'title' => 'required',
                'alias' => 'required|max:255|unique:categories',
                'metadata'    => 'required',
                'metakey'    => 'required',
                'metadesc'    => 'required',
                'status'    => 'required',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }

        $category = Category::create(request()->all());
        $category->save();

        return [
            'message' => trans('app.category.store_message'),
            'category' => $category
        ];
    }

	public function update($id)
    {
        try {

            $v = \Validator::make(request()->all(), [
                'name' => 'required',
                'title' => 'required',
                'alias'    => 'required',
                'metadata'    => 'required',
                'metakey'    => 'required',
                'metadesc'    => 'required',
                'status'    => 'required',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }

            $category = Category::where('id',$id)
            ->update(request()->all());

            if ( $category) {
                return [
                    'message' => trans('app.category.update_message'),
                ];
            }else{
                return [
                    'message' => trans('app.category.update_message_error'),
                ];
            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.error')], 500);
        }

    }

    public function delete($id)
    {
        $category = Category::destroy($id);

        return ['message' => trans('app.category.delete_message')];
    }

}
