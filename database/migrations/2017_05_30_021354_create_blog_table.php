<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('blog_category_id');
            $table->foreign('blog_category_id')->references('id')->on('blog_categories')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->string('title');
            $table->longtext('htmlcontent');
            $table->string('image_intro');
            $table->string('image_content');
            $table->string('image_intro_alt');
            $table->string('image_content_alt');
            $table->string('tags');
            $table->string('created_by');
            $table->boolean('featured')->default(true);
            $table->boolean('show_social_share')->default(true);
            $table->boolean('show_print_icon')->default(true);
            $table->boolean('show_publish_date')->default(true);
            $table->boolean('show_created_date')->default(true);
            $table->boolean('show_author')->default(true);
            $table->boolean('show_tags')->default(true);
            $table->string('robots');
            $table->integer('access');
            $table->datetime('publish_date');
            $table->datetime('star_publish_date');
            $table->datetime('finish_publish_date');
            $table->integer('hits');
            $table->string('metadesc');
            $table->string('metakeywords');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog');
    }
}
