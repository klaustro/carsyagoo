<?php

namespace App\Http\Controllers;

use App\AdManager;
use App\Address;
use App\Dealer;
use App\DealerImage;
use App\User;
use Auth;
use Illuminate\Http\Request;

class DealerController extends Controller
{


    public function index()
	{
        return Dealer::with(['images', 'user.address'])->search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);
	}

    public function dealerList()
    {
        $dealers = Dealer::with('user')->get();
        return $dealers;
    }

	public function store()
    {
        $request_user = request()->draft['user'];
        $request_user['password'] = '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm';
        $request_user['user_group_id'] = 2;
        $user = User::create($request_user);
        $user->save();

        $request_dealer = request()->draft;
        $request_dealer['user_id'] = $user->id;
        $dealer = Dealer::create($request_dealer);
        $dealer->save();

        $this->storeAddress($dealer->user_id, request()->draft['user']['address']);

        return [
                'message' => trans('app.dealer.store_message'),
                'dealer' => $dealer,
                'user' => $user,
        ];
    }

    public function update($id)
    {
        $dealer = Dealer::find($id);

        $dealer->fill(request()->draft);

        $dealer->save();

        $user = User::find($dealer->user_id);

        $user->fill(request()->draft['user']);

        $user->save();

        if ($user->address) {
            $user->address->delete();
        }

        $this->storeAddress($dealer->user_id, request()->draft['user']['address']);

        return ['message' => trans('app.dealer.update_message')];
    }

    public function delete($id)
    {
        $dealer = Dealer::destroy($id);

        return ['message' => trans('app.dealer.delete_message')];
    }

    public function front()
    {
        $dealer = auth()->user()->dealer;

        if ($dealer) {

            $footer = loadFooter();
            $dealer->active = $dealer->active;
            $dealer->user = $dealer->user;
            $dealer->adsCount = $dealer->ads->count();

            return view('dealer.index', compact('dealer', 'footer'));
        }

    }

    public function show($id)
    {
        return Dealer::with(['images', 'user'])->find($id);
    }

    protected function storeAddress($user_id, $data)
    {
        $address = new Address();
        $address->user_id = $user_id;
        $address->state = $data['state'];
        $address->country = $data['country'];
        $address->latitude = $data['latitude'];
        $address->locality = $data['locality'];
        $address->longitude = $data['longitude'];
        $address->postal_code = $data['postal_code'];
        $address->route = $data['route'];
        $address->street_number = $data['street_number'];
        $address->secondary = $data['secondary'];
        $address->save();
    }
}
