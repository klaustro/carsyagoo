<?php

namespace App;

use App\BillingPayment;
use App\BillingService;
use App\BillingServiceDealer;
use App\Payment;
use App\Charge;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

/**
 * Class to bill services
 */
class Billing
{
    function recurrent()
    {
        $due_services = BillingServiceDealer::where('expires', '<', Carbon::now()->toDateString())->get();

        $due_services->map(function($service){
            $dealer_id = $service->dealer_id;
            $method = 'credit_card';
            $billing_payment = BillingPayment::where(['dealer_id' => $dealer_id, 'primary' => 1])->first();

            if ($billing_payment) {
                $credit_card = [
                    'type' => $billing_payment->type,
                    'cardnumber' => $billing_payment->decrypted_card_number,
                    'expire_month' => $billing_payment->expire_month,
                    'expire_year' => $billing_payment->expire_year,
                    'cvc' => $billing_payment->cvc,
                    'first_name' => $billing_payment->first_name,
                    'last_name' => $billing_payment->last_name,
                ];

                $items = $this->setBillingService($service->billing_service_id);

                $charge_payment = new Charge($dealer_id, $method, $credit_card, $items);
                $response = $charge_payment->payflow();

                if (! array_key_exists('error', $response)) {
                    $billing_service = BillingService::find($service->billing_service_id);
                    $new_date = $billing_service->billing_term->renew_date($service->expires);
                    $service->expires = $new_date;
                    $service->save();
                    Log::info("DEALER ID: $dealer_id approved");
                } else {
                    //Save payment in DB
                    $local_payment = new Payment;
                    $local_payment->dealer_id = $dealer_id;
                    $local_payment->billing_service_id = $service->billing_service_id;
                    $local_payment->type = $method;
                    $local_payment->state = 'error';
                    $local_payment->response = json_encode($response);
                    $local_payment->save();
                    Log::error("DEALER ID: $dealer_id error");
                }
            }
        });

        Log::info('Billing run at ' . Carbon::now());

        return response()->json(['message' => 'Ended'], 200);
    }

    /**
     * Method to set items for Paypal API by Billing Service requested
     * @param integer $id
     * @return  array
     */
    function setBillingService($id)
    {
        $billing_service = BillingService::find($id);


        $items = [
            0 => [
                'id' => $id,
                'name' => $billing_service->name,
                'description' => $billing_service->description,
                'currency' => 'USD',
                'quantity' => 1,
                'tax' => 0,
                'price' => $billing_service->price,
            ],
        ];

        return $items;
    }
}