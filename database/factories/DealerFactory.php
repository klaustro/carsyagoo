<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(App\Dealer::class, function (Faker $faker) {
    return [
        'user_id' => function(){
            return factory(User::class)->create([
                'user_group_id' => 2,
            ])->id;
        },
        'dealer_shortname' => $faker->name,
        'dealer_EIN' => $faker->unique()->randomNumber(),
        'dealer_manager' => $faker->name,
        'sunbizzurl' => $faker->name,
        'website' => $faker->url,
        'latitude' => $faker->latitude(-90, 90) ,
        'longitude' => $faker->longitude(-90, 90) ,
        'original_phone' => $faker->phoneNumber,
        'redirect_phone' => $faker->phoneNumber,
        'fax' => $faker->phoneNumber,
        'mission' => $faker->name,
        'vision' => $faker->name,
        'overview' => $faker->name,
        'about' => $faker->text,
        'biographies' => $faker->text,
        'mail_receive' => $faker->boolean,
        'status' => $faker->boolean,

    ];
});
