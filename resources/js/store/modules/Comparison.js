export default {
	state:{
		comparisons:[],
		loading:false
	},
	actions:{
		getComparisons( context ,payload){
			context.state.loading = false
			axios.post('admin/playfair/getComparisons',payload)
			.then(response => {
				context.commit('getComparisons' , { list: response.data })
				context.state.loading = false
			}).catch(error => {
				console.log(error.data)
				context.state.loading = false
			})
		},
		createComparison(context,payload){
			context.state.loading = true
			axios.post('/admin/playfair/createComparison',payload)
			.then(response => {
				let newComparison = {
					id: response.data.data.id,
                    title: payload.data.title,
                    content:payload.data.content,
                    product_id:payload.id
				}
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
				context.commit('createComparison', newComparison)
				context.state.loading = false
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
				  context.state.loading = false	
			})
		},
		removeComparison(context, id){
			context.state.loading = true
			axios.delete('/admin/playfair/comparison/'+id)
			.then(response => {
                context.commit('removeComparison', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
				context.state.loading = false
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
				context.state.loading = false
			})
		},
		updateComparison(context, payload){
			context.state.loading = true
	        axios.put('/admin/playfair/comparison/' + payload.data.id, payload.data)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateComparison', payload)
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            	context.state.loading = false
            })
    	},
	},
	mutations:{
		getComparisons(state,{ list }){
			state.comparisons = list
		},
		createComparison(state,newComparison){
       		state.comparisons.unshift(newComparison);
		},
		removeComparison(state, id){
			let index = state.comparisons.findIndex(comparison => comparison.id == id)
			state.comparisons.splice(index,1)
		},
		updateComparison(state, {id,data}){
        	let index = state.comparisons.findIndex(comparison => comparison.id == data.id);
        	state.comparisons.splice(index, 1, data);
		}
	},
	
}