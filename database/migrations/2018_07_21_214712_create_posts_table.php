<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('blog_category_id');
            $table->foreign('blog_category_id')->references('id')->on('blog_categories')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->string('title');
            $table->longText('intro');
            $table->string('image');
            $table->longText('content');
            $table->string('slug')->unique();
            $table->string('image_alt');
            $table->string('metadesc');
            $table->string('metakeywords');
            $table->datetime('publish_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
