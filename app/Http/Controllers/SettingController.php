<?php

namespace App\Http\Controllers;

use App\Setting;
use App\SettingGroup;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        return SettingGroup::with('settings')
            ->tab(request()->tab)
            ->get();
    }

    public function store()
    {
        $setting = new Setting();
        $setting->setting_group_id = request()->setting_group_id;
        $setting->param = request()->param;
        $setting->key = snake_case(request()->param);
        $setting->type = request()->type;
        $setting->options = request()->options;
        $setting->value = request()->value;
        $setting->save();

        return [
            'message' => trans('app.setting.store_message'),
            'id' => $setting->id,
        ];
    }

    public function update($id)
    {
        $setting = Setting::find($id);
        $setting->fill(request()->all());
        $setting->save();

        return ['message' => trans('app.setting.update_message')];
    }

    public function delete($id)
    {
        $setting = Setting::destroy($id);

        return ['message' => trans('app.setting.delete_message')];
    }

    public function updateAll($id)
    {
        foreach (request()->payload['settings'] as $item) {
            $setting = Setting::find($item['id']);
            $setting->fill($item);
            $setting->save();
        }
        return ['message' => trans('app.setting.update_message')];
    }

    public function show($key)
    {
        return Setting::where('key', $key)->first();
    }
}
