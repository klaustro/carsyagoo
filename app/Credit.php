<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    protected $fillable = ['ads_id', 'first_name', 'last_name', 'day_phone', 'email', 'street_number', 'route', 'locality', 'administrative_area_level_1', 'country', 'postal_code', 'latitude', 'longitude', 'evening_phone', 'birthday', 'residence', 'mortgage', 'license', 'social_security', 'employer_name', 'street_number', 'route', 'locality', 'administrative_area_level_1', 'country', 'postal_code', 'latitude', 'longitude', 'business_phone', 'employment_status', 'occupation', 'job_time', 'income', 'consent', 'desired_monthly_payment', 'desired_term_length', 'desired_down_payment', 'desired_loan_amount', 'trade_make', 'trade_model', 'trade_year', 'trade_mileage', 'trade_amount', 'trade_comments', ];
}
