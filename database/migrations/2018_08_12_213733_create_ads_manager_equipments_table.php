<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsManagerEquipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads_manager_equipment', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ads_manager_id');
            $table->foreign('ads_manager_id')->references('id')->on('ads_managers')->onUpdate('CASCADE')->onDelete('CASCADE');;
            $table->unsignedInteger('equipment_id');
            $table->foreign('equipment_id')->references('id')->on('equipment')->onUpdate('CASCADE')->onDelete('CASCADE');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads_manager_equipment');
    }
}
