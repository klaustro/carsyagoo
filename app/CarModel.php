<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
	protected $table = 'models';
    protected $fillable = ['name', 'image', 'description', 'make_id', 'alias','metakey','metadesc','status'];


    public function make()
    {
        return $this->BelongsTo('App\Make');
    }

    public static function models($id){
        return CarModel::where('make_id',$id)->orderBy('name', 'DESC')->get();
    }

    public function anouncements()
    {
        return $this->hasMany(AdManager::class, 'model_id')->withoutGlobalScope(AdManagerScope::class);
    }

    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            $query->where('name', 'like', "%$target%")
            ->orWhere('alias', 'like', "%$target%")
            ->orWhere('metakey', 'like', "%$target%")
            ->orWhere('metadesc', 'like', "%$target%")
            ->orWhereHas('make', function ($query) use ($target) {
                $query->Where('name', 'like',"%$target%");
            });
        }

        return $query;
    }
}
