<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fuel;
use Auth;

class FuelController extends Controller
{
    public function index()
    {
        return Fuel::search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);
    }

    public function fuelList()
    {
        $fuel = Fuel::orderBy('name')->get();

/*        $fuel->map(function($fuel){
            $fuel->anouncement_count = $fuel->anouncements->count();
        });*/

        return $fuel;
    }

    public function store()
    {
        $v = \Validator::make(request()->draft, [
            'name' => 'required',
            'letter' => 'required',
            'csscode' => 'required',
            'status' => 'required',
            'category_id' => 'required|integer|exists:categories,id',
        ]);

        $errors = $v->errors();
        $message=[];

        foreach ($errors->all() as  $mess) {
            $message[]=$mess.'  ';
        }

        if ($v->fails())
        {
            return ['message' => $message , 'status' => 0];
        }

        if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);

                rename(public_path().'/'.request()->images[0]['url'],public_path().'/images/fuels/'.request()->images[0]['url']);

                $fuel = fFel::create([
                    'name' => request()->draft['name'],
                    'csscode' => request()->draft['csscode'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                    'image' => request()->images[0]['url'],
                ]);

                return [
                    'message' => trans('app.fuel.store_message'),
                    'id' => $fuel->id, 'data'=> $fuel
                ];

            }else{

                $fuel = Fuel::create([
                    'name' => request()->draft['name'],
                    'csscode' => request()->draft['csscode'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                ]);

                return [
                    'message' => trans('app.fuel.store_message'), 'id'=> $fuel->id, 'data'=> $fuel

                ];
            }

    }

    public function update($id)
    {
        $v = \Validator::make(request()->draft, [
            'name' => 'required',
            'letter' => 'required',
            'csscode' => 'required',
            'status' => 'required',
            'category_id' => 'required|integer|exists:categories,id',
        ]);

        $errors = $v->errors();
        $message=[];

        foreach ($errors->all() as  $mess) {
            $message[]=$mess.'  ';
        }

        if ($v->fails())
        {
            return ['message' => $message , 'status' => 0];
        }
        if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);
                rename(public_path().'/'.request()->images[0]['url'],public_path().'/images/fuels/'.request()->images[0]['url']);

                $fuel = Fuel::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                    'csscode' => request()->draft['csscode'],
                    'image' => request()->images[0]['url'],
                ]);

                return ['message' => trans('app.fuel.update_message')];

            }else{
                $fuel = Fuel::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'category_id' => request()->draft['category_id'],
                    'csscode' => request()->draft['csscode'],
                    'status' => request()->draft['status'],
                ]);

                return ['message' => trans('app.fuel.update_message')];
            }

    }

    public function delete($id)
    {
        $fuel = Fuel::destroy($id);

        return ['message' => trans('app.fuel.delete_message')];
    }


}
