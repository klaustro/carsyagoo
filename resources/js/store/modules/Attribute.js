export default {
	state:{
		attributes:[],
	},
	actions:{
		getAttributes( context ,payload){
			axios.get('admin/playfair/getAttributes',payload)
			.then(response => {
				context.commit('getAttributes' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createAttribute(context,payload){
			axios.post('/admin/playfair/createAttribute',payload)
			.then(response => {
				let newComparison = {
					id: response.data.data.id,
                    title: payload.data.title,
                    content:payload.data.content,
                    product_id:payload.id
				}
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
				context.commit('createAttribute', newComparison)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removeAttribute(context, id){
			axios.delete('/admin/playfair/attribute/'+id)
			.then(response => {
                context.commit('removeAttribute', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		updateAttribute(context, payload){
	        axios.put('/admin/playfair/attribute/' + payload.data.id, payload.data)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateAttribute', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	},
	},
	mutations:{
		getAttributes(state,{ list }){
			return state.attributes = list
		},
		createAttribute(state,newComparison){
       		state.attributes.unshift(newComparison);
		},
		removeAttribute(state, id){
			let index = state.attributes.findIndex(comparison => comparison.id == id)
			state.attributes.splice(index,1)
		},
		updateAttribute(state, {id,data}){
        	let index = state.attributes.findIndex(comparison => comparison.id == data.id);
        	state.attributes.splice(index, 1, data);
		}
	},

}