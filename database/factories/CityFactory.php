<?php

use Faker\Generator as Faker;

$factory->define(\App\City::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
        'acronym' => $faker->word,
        'flagimg' => $faker->word,
        'state_id' => function(){
            return firstOrFactory(\App\State::class);
        },
    ];
});
