@extends('layouts.app')


@section('content')

<div class="main" role="main">
    <div id="content" class="content full">
        <div class="container">
            <div class="listing-header margin-40">
                <h2>Register</h2>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="regular-signup">
                        <form action="{{ url(config('adminlte.register_url', 'register')) }}" method="post">
                            {!! csrf_field() !!}

                            <div class="form-group has-feedback {{ $errors->has('login') ? 'has-error' : '' }}">
                                <input v-on:keydown.32.capture.prevent.stop type="text" name="login" id="login" class="form-control" value="{{ old('login') }}"
                                       placeholder="{{ trans('adminlte::adminlte.login') }}">
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                @if ($errors->has('login'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('login') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                                <input type="text" name="first_name" class="form-control" value="{{ old('name') }}"
                                       placeholder="{{ trans('adminlte::adminlte.first_name') }}">
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                                <input type="text" name="last_name" class="form-control" value="{{ old('name') }}"
                                       placeholder="{{ trans('adminlte::adminlte.last_name') }}">
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                                <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                                       placeholder="{{ trans('adminlte::adminlte.email') }}">
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                <input type="password" name="password" class="form-control"
                                       placeholder="{{ trans('adminlte::adminlte.password') }}">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                <input type="password" name="password_confirmation" class="form-control"
                                       placeholder="{{ trans('adminlte::adminlte.retype_password') }}">
                                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Do you want to be a Dealer?</label><br>
                                    <select class="form-control" name="dealer">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit"
                                    class="btn btn-primary btn-block btn-flat"
                            >{{ trans('adminlte::adminlte.register') }}</button>
                            @if ($errors->has('g-recaptcha-response'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                            @endif
                            @captcha
                        </form>
                        <div class="auth-links">
                            <a href="{{ url(config('adminlte.login_url', 'login')) }}"
                               class="text-center">{{ trans('adminlte::adminlte.i_already_have_a_membership') }}</a>
                        </div>
                   </div>
                </section>
            </div>
        </div>
    </div>
</div>

@stop

@section('adminlte_js')
    @yield('js')
@stop
