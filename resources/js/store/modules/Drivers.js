let state = {
    drivers: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading:false
}

let getters = {
        findDriver(state){
            return function(id){
                let driver = state.drivers.find(make => driver.id == id)
                return driver;
            }
        },
}

let actions = {
    getDrivers(context, params){
        context.state.loading = true
        axios.get('/admin/driver?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getDrivers', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    storeDriver(context, payload){
        axios.post('/admin/driver', payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    let newDriver = {
                        id: response.data.id,
                        name: payload.draft.name,
                        category_id: payload.draft.category_id,
                        status: payload.draft.status,
                        image: payload.draft.image ? '/images/drivers/' + payload.draft.image : '',
                    }
                    Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                    context.commit('storeDriver', newDriver)
                }
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateDriver(context, payload){
        context.state.loading = true
        axios.put('/admin/driver/' + payload.id, payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                    context.commit('updateDriver', payload)
                }
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeDriver(context, id){
        context.state.loading = true
        axios.delete('/admin/driver/' + id)
            .then(response => {
                context.commit('removeDriver', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    listDrivers(context){
        axios.get('/admin/driver-list')
            .then(response => {
                context.commit('listDrivers', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getDrivers(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.drivers = data.data;
    },

    storeDriver(state, newDriver){
        state.drivers.unshift(newDriver);
    },

    updateDriver(state, {id, draft}){
        draft.image = draft.image != '' ? '/images/drivers/' + draft.image : ''
        let index = state.drivers.findIndex(driver => driver.id == id);
        state.drivers.splice(index, 1, draft);
    },

    removeDriver(state, id)    {
        let index = state.drivers.findIndex(driver => driver.id == id);
        state.drivers.splice(index, 1);
    },

    listDrivers(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}