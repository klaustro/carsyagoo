<?php

use App\AdManager;
use App\Setting;

if (! function_exists('firstOrFactory')) {

    /**
     * Get first model, otherwise factory one up
     *
     * @param string $modelClass
     */
    function firstOrFactory(string $modelClass)
    {
        return $modelClass::first() ?? factory($modelClass)->create();
    }

    /**
     * Method to get a value of setting
     * @return string
     */
    function getConfig($key)
    {
        $setting = Setting::where('key', $key)->first();
        if (! isset($setting)) {
            return '';
        }

        return $setting->value;
    }
    /**
     * Method to get a value of setting
     * @return array
     */
    function getConfigStartWith($key)
    {
        $settings = Setting::where('key', 'like', "$key%")->get();
        if (! isset($settings)) {
            return [];
        }

        return $settings->pluck('value');
    }

    /**
     * Method to get a value of setting
     * @return string
     */
    function getPage($key)
    {
        $setting = Setting::where([
            'key' => $key,
            //Group Id 8 = Pages
            'setting_group_id' => 8,
            ])->first();
        if (! isset($setting)) {
            return view('errors.404');
        }

        return $setting->value;
    }
    /**
     * Method to get options of setting
     * @return  array
     */
    function getConfigOptions($key)
    {
        $setting = Setting::where(['key' => $key, 'type' => 'list'])->first();
        if (! isset($setting)) {
            return [];
        }

        return  explode(', ',$setting->options);
    }

    /**
     * Method to save files in staorage
     * @param  string $base64 [description]
     * @param  string $folder [description]
     * @param  string $name   [description]
     * @return null
     */
    function saveFile($base64, $folder, $name)
    {
        try {
            $exploded = explode(',', $base64);

            $decoded = base64_decode($exploded[1]);

            $folder = public_path() . $folder;

            if( is_dir($folder) == false )
            {
                mkdir($folder, 0755, true);
            }

            $path = $folder . $name;

            file_put_contents($path, $decoded);

            return true;

        } catch (Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

    function loadFooter()
    {
        $latestsSedans = AdManager::with(['model','model.make','extColor','specifictColor'])->where('status',1)
            ->where('bodytype_id', 2)
            ->orderBy('id', 'desc')->paginate(3);

        $popularTrucks = AdManager::with(['model','model.make','extColor','specifictColor'])->where('status',1)
            ->where('bodytype_id', 11)
            ->orderBy('id', 'desc')->paginate(3);

        $sportCar = AdManager::with(['model','model.make','extColor','specifictColor'])->where('status',1)
            ->where('bodytype_id', 4)
            ->orderBy('id', 'desc')->paginate(3);

        $recentlyVehicles = AdManager::with(['bodytype','model', 'transmission', 'fuel','model.make','images', 'condition','intColor','extColor','specifictColor','driver'])->where('status',1)->orderBy('id', 'desc')
                ->paginate(getConfig('latest_count'));

        return [
            'latestsSedans' => $latestsSedans,
            'popularTrucks' => $popularTrucks,
            'sportCar' => $sportCar,
            'recentlyVehicles' => $recentlyVehicles,
        ];
    }
}
