<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\BillingPayment;

class BillingPaymentController extends Controller
{
     public function index()
    {
        $billing_payments = BillingPayment::search(request()->search)
            ->byDealer(request()->dealer)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);

        $billing_payments->map(function($billing_payment){
            $billing_payment->hide_card_number = $billing_payment->hide_card_number;
            $billing_payment->hide_routing_number = $billing_payment->hide_routing_number;
            $billing_payment->hide_account_number = $billing_payment->hide_account_number;
        });

        return $billing_payments;

    }

    public function billingPaymentList()
    {
        return BillingPayment::all();
    }

	public function store()
    {
        try {
            $request = request()->draft;
            if (! isset($request['dealer_id'])){
                $request['dealer_id'] = auth()->user()->dealer->id;
            }
            $v = \Validator::make($request, [
                'cardnumber' => 'required',
                'expire_month' => 'required',
                'expire_year' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'cvc' => 'required',
                'primary' => 'required',
                'status' => 'required',
                'dealer_id' => 'required',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }

            $billingPayment = BillingPayment::create($request);


            if ( $billingPayment->save()) {
            $billingPayment->hide_card_number = $billingPayment->hide_card_number;
            $billingPayment->hide_routing_number = $billingPayment->hide_routing_number;
            $billingPayment->hide_account_number = $billingPayment->hide_account_number;
                return [
                    'message' => trans('app.billingPayment.store_message'),
                    'id' => $billingPayment->id, 'data'=>$billingPayment, 'status' => 1,
                ];

            }else{
                return [
                    'message' => trans('app.billingPayment.store_message_error'), 'status' => 1,

                ];

            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.store_error')], 500);
        }
    }

	public function update($id)
    {
        try {
            $v = \Validator::make(request()->draft, [
                'cardnumber' => 'required',
                'expire_month' => 'required|numeric',
                'expire_year' => 'required|numeric',
                'first_name' => 'required',
                'last_name' => 'required',
                'cvc' => 'required',
                'primary' => 'required',
                'status' => 'required',
                'dealer_id' => 'required',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }

            $billingPayment = BillingPayment::find($id);
            $billingPayment->type = request()->draft['type'];
            $billingPayment->expire_month = request()->draft['expire_month'];
            $billingPayment->expire_year = request()->draft['expire_year'];
            $billingPayment->first_name = request()->draft['first_name'];
            $billingPayment->last_name = request()->draft['last_name'];
            $billingPayment->cvc = request()->draft['cvc'];
            $billingPayment->zipcode = request()->draft['zipcode'];
            $billingPayment->primary = request()->draft['primary'];
            $billingPayment->status = request()->draft['status'];
            $billingPayment->dealer_id = request()->draft['dealer_id'];
            $billingPayment->fill(request()->draft);
            $billingPayment->save();

            return [
                'message' => trans('app.billingPayment.update_message'), 'status' => 1
            ];

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.error')], 500);
        }

    }

    public function delete($id)
    {
        $billingPayment = BillingPayment::destroy($id);

        return ['message' => trans('app.billingPayment.delete_message')];
    }
}
