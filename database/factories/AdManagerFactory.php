<?php

use Faker\Generator as Faker;

$factory->define(App\AdManager::class, function (Faker $faker) {
    return [
        'embedcode' => uniqid(),
        'expire' => $faker->randomElement([null, 25]),
        'engine' => $faker->word,
        'seats' => $faker->numberBetween($min = 1, $max = 100),
        'doors' => $faker->numberBetween($min = 1, $max = 10),
        'bargain_price' => $faker->randomNumber(),
        'price' => rand(1000, 100000),
        'frequency' => 'None',
        'mileage' => rand(10000, 100000),
        'vincode' => $faker->word,
        'year' => rand(1980, 2018),
        'equipment_detail' => $faker->text,
        'category_id' => function(){
            return firstOrFactory(\App\Category::class);
        },
        'make_id' => function(){
            return firstOrFactory(\App\Make::class);
        },
        'model_id' => function(){
            return firstOrFactory(\App\CarModel::class);
        },
        'condition_id' => function(){
            return firstOrFactory(\App\Condition::class);
        },
        'dealer_id' => function(){
            return firstOrFactory(\App\Dealer::class);
        },
        'driver_id' => function(){
            return firstOrFactory(\App\Driver::class);
        },
        'transmission_id' => function(){
            return firstOrFactory(\App\Transmission::class);
        },
        'bodytype_id' => function(){
            return firstOrFactory(\App\BodyType::class);
        },
        'fuel_id' => function(){
            return firstOrFactory(\App\Fuel::class);
        },
        'ext_color_id' => function(){
            return firstOrFactory(\App\Color::class);
        },
        'int_color_id' => function(){
            return firstOrFactory(\App\Color::class);
        },
        'specific_color' => function(){
            return firstOrFactory(\App\Color::class);
        },
        'is_commercial' => $faker->boolean,
        'is_featured' => $faker->boolean,
        'is_top' => $faker->boolean,
        'is_reserved' => $faker->boolean,
        'is_special' => $faker->boolean,
        'hits' => $faker->randomNumber(),
        'status' => $faker->boolean,
        'otherinfo' => $faker->text,
        'unfaden_weight' => $faker->randomNumber(),
        'gross_weight' => $faker->randomNumber(),
        'length' => $faker->randomNumber(),
        'width' => $faker->randomNumber(),
        'disclaimer' => $faker->text,
        'is_metalic_color' => $faker->boolean,
        'specific_model' => $faker ->randomElement($array = array ('sedan','sport','wagon','truck','mini', 'suv','convertible')),
        'specific_trans' =>  $faker->name,
        'export_price' => $faker->randomNumber(),
        'fuel_consum_city' => $faker->randomNumber(),
        'fuel_consum_freeway' => $faker->randomNumber(),
        'fuel_consum_combined' => $faker->randomNumber(),
        'acceleration' => $faker->numberBetween($min = 10, $max = 100),
        'max_speed' => $faker->numberBetween($min = 100, $max = 800),
        'C02' => $faker->randomNumber(),
        'video_link' => $faker->url,
        'main_image' => '/assets/images/default-car.png',
    ];


});
