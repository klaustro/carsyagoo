<footer class="site-footer">
    <div class="site-footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 footer_widget widget widget_custom_menu widget_links">
                    <h4 class="widgettitle">Latest New Car Listing</h4>
                    <div style="padding-bottom:13px;text-align: left;">
                      @php
                        $flag=0;
                      @endphp
                      @if (!empty($footer['recentlyVehicles']))
                          @foreach ($footer['recentlyVehicles'] as $element)
                              @php
                                $flag++;
                              @endphp
                              <a href="{{ url('gallery/anouncement/' . $element->id) }}">New {{$element->model->make->name}} {{$element->model->name}}</a>
                              <div class="metaLabel"> {{$element->specifictColor->name}} Color {{$element->engine}} Engine ${{$element->price}} Price</div>

                                @break($flag == 3)
                          @endforeach
                      @endif
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 footer_widget widget widget_custom_menu widget_links">
                    <h4 class="widgettitle">Latests Sedans</h4>
                    <div style="padding-bottom:13px;text-align: left;">
                        @if (!empty($footer['latestsSedans']))
                          @foreach ($footer['latestsSedans'] as $element)
                              @if ($element->bodytype->name == 'Sedan')
                                <a href="{{ url('gallery/anouncement/' . $element->id) }}">New {{$element->model->make->name}} {{$element->model->name}}
                                </a>
                                <div class="metaLabel">{{$element->specifictColor->name}} Color {{$element->engine}} Engine ${{$element->price}} Price
                                </div>
                              @endif
                          @endforeach
                        @endif
                    </div>
                  </div>
                <div class="col-md-3 col-sm-6 footer_widget widget widget_custom_menu widget_links">
                    <h4 class="widgettitle">Popular Trucks</h4>
                    <div style="padding-bottom:13px;text-align: left;">
                        @if (!empty($footer['popularTrucks']))
                            @foreach ($footer['popularTrucks'] as $element)
                              <a href="{{ url('gallery/anouncement/' . $element->id) }}">New {{$element->model->make->name}} {{$element->model->name}}</a>
                              <div class="metaLabel"> {{$element->specifictColor->name}} Color {{$element->engine}} Engine ${{$element->price}} Price</div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 footer_widget widget widget_custom_menu widget_links">
                    <h4 class="widgettitle">Sport Car Listing</h4>
                    <div style="padding-bottom:13px;text-align: left;">
                        @if (!empty($footer['sportCar']))
                            @foreach ($footer['sportCar'] as $element)
                              <a href="{{ url('gallery/anouncement/' . $element->id) }}">New {{$element->model->make->name}} {{$element->model->name}}</a>
                              <div class="metaLabel"> {{$element->specifictColor->name}} Color {{$element->engine}} Engine ${{$element->price}} Price</div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-6 menu-footer-inner">
                    <ul class="">
                        <li><a href="{{ url('about') }}">About CarsYagoo.com</a></li>
                        <li><a href="{{ url('blog') }}">Blog</a></li>
                        <li><a href="{{ url('contact') }}">Contact Us</a></li>
                        <li><a href="{{ url('policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ url('credit-application') }}">Credit Application</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 copyrights-left">
                    <p>
                      Copyright CarsYaGoo ® {{ date("Y") }}. All Right Reserved
                    <br>Powered by Nissi Technology Enterprises Inc
                    </p>
                </div>
                <div class="col-md-6 col-sm-6 copyrights-right">
                    <ul class="social-icons social-icons-colored pull-right">
                        <li class="facebook"><a href="{{ getConfig('facebook_site') }}"><i class="fa fa-facebook"></i></a></li>
                        <li class="twitter"><a href="{{ getConfig('twitter_site') }}"><i class="fa fa-twitter"></i></a></li>
                        <li class="linkedin"><a href="{{ getConfig('linkedin_site') }}"><i class="fa fa-linkedin"></i></a></li>
                        <li class="youtube"><a href="{{ getConfig('youtube_site') }}"><i class="fa fa-youtube"></i></a></li>
                        <li class="instagram"><a href="{{ getConfig('instagram_site') }}"><i class="fa fa-instagram"></i></a></li>
                        <li class="googleplus"><a href="{{ getConfig('googleplus_site') }}"><i class="fa fa-google"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>