<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BillingTerm extends Model
{
    protected $fillable = [
        'name', 'days'
    ];

    //Scopes
    public function scopeSearch($query, $target)
    {

        if ($target != '') {
            return $query->where('name', 'like', "%$target%")
            			 ->orWhere('days', 'like', "%$target%");
        }
    }

    //Methods
    public function renew_date($due_date)
    {
        $due_date = Carbon::createFromFormat('Y-m-d', $due_date);
        if ($this->name == 'monthly') {
          return $due_date->addMonth()->toDateString();
        }

        if ($this->name == 'yearly') {
          return $due_date->addYear()->toDateString();
        }

        return $due_date->addDays($this->days)->toDateString();
    }

}
