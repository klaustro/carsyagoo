<section class="listing-block latest-news">
    <div class="listing-header">
        <a href="/blog" class="btn btn-sm btn-default pull-right">All posts</a>
        <h3>Latest Blog Posts</h3>
    </div>
    <div class="listing-container">
        <div class="carousel-wrapper">
            <div class="row">
                <ul class="owl-carousel" id="news-slider" data-columns="{{ getConfig('home_posts')}}" data-autoplay="" data-pagination="yes" data-arrows="yes" data-single-item="no" data-items-desktop="2" data-items-desktop-small="1" data-items-tablet="2" data-items-mobile="1">
                    @foreach($posts as $post)
                    <li class="item">
                        <div class="post-block format-standard">
                            <a href="{{url('blog/post/' . $post->slug)}}" class="media-box post-image"><img src="{{ $post->image }}" alt="" width="300"></a>
                            <div class="post-actions">
                                <div class="post-date">{{ \Carbon\Carbon::parse($post->created_at)->toFormattedDateString() }}</div>
                                <div class="comment-count">
                                    <a href="{{url('blog/post/' . $post->slug)}}">
                                        <i class="icon-dialogue-text"></i> {{ $post->comments->count() }}
                                        <i class="fa fa-heart"></i> {{ $post->votes->count() }}
                                    </a>
                                </div>
                            </div>
                            <h3 class="post-title"><a href="{{url('blog/post/' . $post->slug)}}">{{ $post->title }}</a></h3>
                            <div class="post-content">
                                <p>{!! substr($post->intro, 0, 300) !!} ...</p>
                                <a href="{{url('blog/post/' . $post->slug)}}" class="continue-reading">Continue reading <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                            <div class="post-meta">
                                Posted in: <a href="{{ url('blog/by-category/' . $post->blog_category_id) }}">{{ $post->category->name }}</a>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>