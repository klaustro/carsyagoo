<link href="{{ url('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
<script src="{{url('js/app.js')}}"></script>

<!------ Include the above in your HEAD tag ---------->

 <div class="content-wrapper">
  <div class="col-md-6 col-md-offset-3">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Invoice
        <small>#{{ $invoice->id }}</small>
      </h1>

    </section>

    <div class="pad margin no-print">
      <div class="callout callout-info" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info"></i> Note:</h4>
        {{ $invoice->note}}
      </div>
    </div>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> {{ getConfig('site_name') }}
            <small class="pull-right">Date: {{ \Carbon\Carbon::parse($invoice->date)->format('m-d-Y') }}</small>
          </h2>
        </div><!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
           <strong>{{ getConfig('site_name') }}</strong><br>
            543 suthpark drive<br>
            Boston, MA 94107<br>
            Phone: (555) 539-1444<br/>
            Email: mekoya@example.com
          </address>
        </div><!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong>{{ $invoice->dealer->user->first_name }} {{ $invoice->dealer->user->last_name }}</strong>
            <br>
            {{ $invoice->dealer->user->address_1 }}<br>
            {{ $invoice->dealer->user->address_2 }}<br>
            Phone: {{ $invoice->dealer->user->phone }}<br/>
            Email: {{ $invoice->dealer->user->email }}
          </address>
        </div><!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice #{{ $invoice->id }}</b><br/>
          <br/>
          <b>Token:</b> {{ $invoice->token }}<br/>
          <b>Service Due:</b> {{ \Carbon\Carbon::parse($invoice->dealer->services->first()->pivot->expires)->format('m-d-Y') }}<br/>
          <b>Account:</b> {{ $invoice->dealer->user->login }}
        </div><!-- /.col -->
      </div><!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Qty</th>
                <th>Product</th>
                <th>ID</th>
                <th>Description</th>
                <th>Subtotal</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>{{ $invoice->dealer->services->first()->name }}</td>
                <td>{{ $invoice->dealer->services->first()->id }}</td>
                <td>{{ substr($invoice->dealer->services->first()->description, 0, 70 )}}</td>
                <td>${{ number_format($invoice->dealer->services->first()->price, 2) }}</td>
              </tr>
            </tbody>
          </table>
        </div><!-- /.col -->
      </div><!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Payment Methods:</p>
          <img src="{{url('images/payment_methods.png')}}" width="300" alt="Visa"/>
          <hr>
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            {{getConfig('invoice_footer')}}
          </p>
        </div><!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Service Due {{ \Carbon\Carbon::parse($invoice->dealer->services->first()->pivot->expires)->format('m-d-Y') }}</p>
          <br>
          <br>
          <br>
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td>${{ number_format($invoice->dealer->services->first()->price, 2) }}</td>
              </tr>
<!--
              <tr>
                <th>Tax (9.3%)</th>
                <td>$10.34</td>
              </tr>
-->
              <tr>
                <th>Total:</th>
                <td>${{ number_format($invoice->dealer->services->first()->price, 2) }}</td>
              </tr>
            </table>
          </div>
        </div><!-- /.col -->
      </div><!-- /.row -->

      <!-- this row will not appear when printing -->
      @if(request()->route()->getName() != 'invoice.pdf')
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="{{ url('dealer/pdf-invoice/' . $invoice->id) }}" target="_blank" class="btn btn-success pull-right"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Create PDF</a>
        </div>
      </div>
      @endif
    </section><!-- /.content -->
    <div class="clearfix"></div>
  </div>
</div><!-- /.content-wrapper -->