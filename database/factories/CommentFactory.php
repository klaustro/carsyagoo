<?php

use App\Post;
use App\User;
use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'user_id' => function(){
            return firstOrFactory(User::class);
        },
        'post_id' => function(){
            return firstOrFactory(Post::class);
        },
        'content' => $faker->paragraph(2),
    ];
});
