<?php

namespace App\Http\Controllers;

use App\WishList;
use Illuminate\Http\Request;

class WishListController extends Controller
{
    public function index()
    {
        return WishList::with('ads_manager')
            ->where('user_id', auth()->user()->id)
            ->orderBy('id', 'desc')
            ->paginate(5);
    }

    public function store()
    {
        $wish_list = new WishList();
        $wish_list->user_id = auth()->user()->id;
        $wish_list->ads_manager_id = request()->ads_manager_id;
        $wish_list->save();

        return [
            'message' => trans('app.wishList.store_message'),
            'id' => $wish_list->id,
        ];
    }

    public function delete($ads_manager_id)
    {
        $wish_list = WishList::where([
            'ads_manager_id'=> $ads_manager_id,
            'user_id' => auth()->user()->id,
        ])->delete();

        return ['message' => trans('app.wishList.delete_message')];
    }

}
