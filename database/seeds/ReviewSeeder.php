<?php

use App\AdManager;
use App\Review;
use Illuminate\Database\Seeder;

class ReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdManager::all()->map(function($ads_manager){
            factory(Review::class, 3)->create([
                'ads_manager_id' => $ads_manager->id,

            ]);
        });
    }
}
