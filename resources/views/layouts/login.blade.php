<div class="modal fade" id="loginModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4>{{ trans('adminlte::adminlte.login_message') }}</h4>
            </div>
            <div class="modal-body">
                <form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                    {!! csrf_field() !!}
                    <div class="form-group has-feedback {{ $errors->has('login') ? 'has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" name="login" class="form-control" value="{{ old('email') }}"
                               placeholder="{{ trans('adminlte::adminlte.login') }}">
                        </div>
                        @if ($errors->has('login'))
                            <span class="help-block">
                                <strong>{{ $errors->first('login') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                            <input type="password" name="password" class="form-control"
                               placeholder="{{ trans('adminlte::adminlte.password') }}">
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-xs-7">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox" name="remember"> {{ trans('adminlte::adminlte.remember_me') }}** &nbsp
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <input type="submit" class="btn btn-primary" value="{{ trans('adminlte::adminlte.sign_in') }}">
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

@push('head_scripts')
<script type="text/javascript">
    $(document).ready(function(){
        if ($('.has-error').text() != '') {
            $('#loginModal').modal('show')
        }
    });
</script>
@endpush