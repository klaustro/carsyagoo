<?php

namespace Tests\Feature;

use App\AdManager;
use App\Dealer;
use App\Review;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReviewTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->dealer = factory(Dealer::class)->create([
            'user_id' => $this->user->id
        ]);

        $this->actingAs($this->user);
    }

    /**
     * @test
     */
    function if_review_average_can_be_showed()
    {
        $ads_manager = factory(AdManager::class)->create([
            'dealer_id' => $this->dealer->id,
        ]);

        $ads_manager->review()->saveMany([
            new Review(['user_id' => $this->user->id, 'value' => 5, 'comment' => 'comment 5']),
            new Review(['user_id' => $this->user->id, 'value' => 4, 'comment' => 'comment 4']),
            new Review(['user_id' => $this->user->id, 'value' => 3, 'comment' => 'comment 3']),
            new Review(['user_id' => $this->user->id, 'value' => 2, 'comment' => 'comment 2']),
            new Review(['user_id' => $this->user->id, 'value' => 1, 'comment' => 'comment 1']),
        ]);

        $this->get('/api/anouncement/' . $ads_manager->id)
            ->assertSuccessful()
            ->assertJsonFragment(['review_average' => 3]);
    }

    /**
     * @test
     */
    function if_review_can_be_stored()
    {
        $ads_manager = factory(AdManager::class)->create([
            'dealer_id' => $this->dealer->id,
        ]);

        $this->post('api/review', [
            'ads_manager_id' => $ads_manager->id,
            'value' => 5,
            'comment' => 'Some comment',
        ])
            ->assertSuccessful()
            ->assertJsonFragment(['message' => trans('app.review.store_message')]);

        $this->assertDatabaseHas('reviews', [
            'ads_manager_id' => $ads_manager->id,
            'user_id' => $this->user->id,
            'value' => 5,
            'comment' => 'Some comment',
        ]);

    }

    /**
     * @test
     */
    function if_review_can_be_updated()
    {
        $ads_manager = factory(AdManager::class)->create([
            'dealer_id' => $this->dealer->id,
        ]);

        $review = factory(Review::class)->create([
            'ads_manager_id' => $ads_manager->id,
            'user_id' => $this->user->id,
            'value' => 5,
            'comment' => 'Some comment',
        ]);

        $this->put('/api/review/' . $review->id, [
            'value' => 3,
            'comment' => 'Other comment',
        ])
            ->assertSuccessful()
            ->assertJsonFragment(['message' => trans('app.review.update_message')]);

        $this->assertDatabaseHas('reviews', [
            'ads_manager_id' => $ads_manager->id,
            'user_id' => $this->user->id,
            'value' => 3,
            'comment' => 'Other comment',
        ]);
    }
}
