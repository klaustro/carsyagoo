		<form action="{{route('gallery')}}" method="POST" enctype="multipart/form-data" >
			{{ csrf_field() }}
			<h3>Let’s Find Your Car</h3>
			<div class="row">
				<div class="col-md-12">
<!--
					<div class="row">
						<div class="col-md-6">
							<label>Min {{ trans('app.adManager.year') }}</label>
							<select name="min_year" class="form-control">
								<option value="" selected>{{ trans('app.common.select') }}</option>
								@foreach(getConfigOptions('cars_years') as $year)
									<option value="{{$year}}">{{ $year }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-6">
							<label>Max {{ trans('app.adManager.year') }}</label>
							<select name="max_year" class="form-control">
								<option value="" selected>{{ trans('app.common.select') }}</option>
								@foreach(getConfigOptions('cars_years') as $year)
									<option value="{{$year}}">{{ $year }}</option>
								@endforeach
							</select>
						</div>

						<div class="col-md-6">
							<label>Specific model</label>
							<input type="text" name="specific_model" class="form-control">
						</div>

						<div class="col-md-6">
							<label>Zip Code</label>
							<search-postal-code></search-postal-code>
						</div>
					</div>
				</div>
			-->
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-3">
							<label>{{ trans('app.make.title') }}</label>
							<select name="make_id" id="make" class="form-control">
								<option value="" selected>{{ trans('app.common.select') }}</option>
								@foreach($makes as $make)
									<option value="{{$make->id}}">{{ $make->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-3">
							<label>{{ trans('app.model.title') }}</label>
							<select name="model_id" id="model" class="form-control">
								<option value="" selected>{{ trans('app.common.select') }}</option>
							</select>
						</div>

						<div class="col-md-3">
							<label>Min {{ trans('app.adManager.price') }}</label>
							<select name="min_price" class="form-control">
								<option value="" selected>{{ trans('app.common.select') }}</option>
								@foreach(getConfigOptions('cars_prices') as $price)
									<option value="{{$price}}">${{ number_format($price, 2) }}</option>
								@endforeach
							</select>
						</div>

						<div class="col-md-3">
							<label>Max {{ trans('app.adManager.price') }}</label>
							<select name="max_price" class="form-control">
								<option value="" selected>{{ trans('app.common.select') }}</option>
								@foreach(getConfigOptions('cars_prices') as $price)
									<option value="{{$price}}">${{ number_format($price, 2) }}</option>
								@endforeach
							</select>
						</div>

					</div>

					<br>
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<input type="submit" class="btn btn-block btn-info btn-lg router-link-active" value="Find my vehicle now">
						</div>
					</div>
				</div>
			</div>
		</form>
@push('footer_scripts')
<script type="text/javascript">
		$('#make').change(function(event){
			var id = event.target.value;
			$.ajax({
				type:"GET",
				url:"{{ route('models') }}",
				dataType:"json",
				data: {id:id},
				success:function(response){
					$('#model').empty();
					$('#model').append("<option value=''>Please select</option>");
					for (var i = response.length - 1; i >= 0; i--) {
						$('#model').append("<option value='"+response[i].id+"'>"+response[i].name+"</option>");
					}
				}
			});

		});

</script>
@endpush