let state = {
    billingPayments: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading: false,
}

let getters = {
        findBillingPayment(state){
            return function(id){
                let billingPayment = state.billingPayments.find(billingPayment => billingPayment.id == id)
                return billingPayment;
            }
        },
}

let actions = {
    getBillingPayments(context, params){
        context.state.loading = true
        axios.get('/admin/billingPayment?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc + '&dealer=' + params.dealer)
            .then(response => {
                context.commit('getBillingPayments', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    storeBillingPayment(context, payload){
        context.state.loading = true
        new Promise((resolve, reject) => {
            axios.post('/admin/billingPayment', payload)
                .then(response => {
                    if (response.data.status == 0) {
                        for (var i = response.data.message.length - 1; i >= 0; i--) {
                            Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                        }

                    }else{
                        let newBillingPayment = {
                            id: response.data.id,
                            cardnumber: response.data.data.cardnumber,
                            hide_card_number: response.data.data.hide_card_number,
                            type: response.data.data.type,
                            expire_month: response.data.data.expire_month,
                            expire_year: response.data.data.expire_year,
                            first_name: response.data.data.first_name,
                            last_name: response.data.data.last_name,
                            cvc: response.data.data.cvc,
                            zipcode: response.data.data.zipcode,
                            account_number: response.data.data.account_number,
                            hide_account_number: response.data.data.hide_account_number,
                            routing_number: response.data.data.routing_number,
                            hide_routing_number: response.data.data.hide_routing_number,
                            primary: response.data.data.primary,
                            status: response.data.data.status,
                            dealer_id: response.data.data.dealer_id,
                        }
                        context.commit('storeBillingPayment', newBillingPayment)
                        Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                    }
                    resolve(response)
                    context.state.loading = false
                })
                .catch(error => {
                    Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                    context.state.loading = false
                    reject(error)
                })
        })
    },

    updateBillingPayment(context, payload){
        context.state.loading = true
        new Promise((resolve, reject) => {
            axios.put('/admin/billingPayment/' + payload.id, payload)
                .then(response => {
                    if (response.data.status == 0) {
                        for (var i = response.data.message.length - 1; i >= 0; i--) {
                            Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                        }

                    }else{
                        Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                        context.commit('updateBillingPayment', payload)
                    }
                    context.state.loading = false
                    resolve(response)
                })
                .catch(error => {
                    Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                    context.state.loading = false
                    reject(error)
                })
        })
    },

    removeBillingPayment(context, id){
        context.state.loading = true
        axios.delete('/admin/billingPayment/' + id)
            .then(response => {

                context.commit('removeBillingPayment', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                context.state.loading = false
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })

    },

    listBillingPayments(context){
        axios.get('/admin/billingPaymentList')
            .then(response => {
                context.commit('listBillingPayments', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getBillingPayments(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.billingPayments = data.data;
    },

    storeBillingPayment(state, newBillingPayment){
        state.billingPayments.unshift(newBillingPayment);
    },

    updateBillingPayment(state, {id, draft}){
        let index = state.billingPayments.findIndex(billingPayment => billingPayment.id == id);
        state.billingPayments.splice(index, 1, draft);
    },

    removeBillingPayment(state, id)    {
        let index = state.billingPayments.findIndex(billingPayment => billingPayment.id == id);
        state.billingPayments.splice(index, 1);
    },

    listBillingPayments(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}