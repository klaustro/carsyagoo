<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $fillable = ['name', 'image', 'csscode', 'category_id', 'status',];

    /**
     * Method to search by any column
     * @param  Query $query
     * @param  string $target [description]
     * @return Query
     */
    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->
                where('id', $target)
                ->orWhere('name', 'like', "%$target%")
                ->orWhere('image', 'like', "%$target%")
                ->orWhere('csscode', 'like', "%$target%");
        }
    }
}
