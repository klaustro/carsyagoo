<?php

namespace Tests\Feature;

use App\User;
use App\UserGroup;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function admin_user_see_admin_panel_when_login()
    {
        $user_group = factory(UserGroup::class)->create([
            'id' => 1,
            'title' => 'admin',
        ]);

        $user = factory(User::class)->create([
            'user_group_id' => $user_group->id,
        ]);

        $this->actingAs($user)
            ->get(route('home'))
            ->assertStatus(302)
            ->assertRedirect(route('playfair'));
    }

    /**
     * @test
     */
    public function dealer_user_see_admin_panel_when_login()
    {
        $user_group = factory(UserGroup::class)->create([
            'id' => 2,
            'title' => 'dealer',
        ]);

        $user = factory(User::class)->create([
            'user_group_id' => $user_group->id,
        ]);

        $this->actingAs($user)
            ->get(route('home'))
            ->assertStatus(302)
            ->assertRedirect(route('dealer'));
    }

    /**
     * @test
     */
    public function admin_can_access_admin_dashboard()
    {
        $user_group = factory(UserGroup::class)->create([
            'id' => 1,
            'title' => 'admin',
        ]);

        $admin = factory(User::class)->create([
            'user_group_id' => $user_group->id,
        ]);

        $this->actingAs($admin)
            ->get(route('playfair'))
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function admin_can_access_dealer_dashboard()
    {
        $user_group = factory(UserGroup::class)->create([
            'id' => 1,
            'title' => 'admin',
        ]);

        $admin = factory(User::class)->create([
            'user_group_id' => $user_group->id,
        ]);

        $this->actingAs($admin)
            ->get(route('dealer'))
            ->assertStatus(200);
    }


    /**
     * @test
     */
    public function dealer_can_access_dealer_dashboard()
    {
        $user_group = factory(UserGroup::class)->create([
            'id' => 2,
            'title' => 'dealer',
        ]);

        $dealer = factory(User::class)->create([
            'user_group_id' => $user_group->id,
        ]);

        $this->actingAs($dealer)
            ->get(route('dealer'))
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function normal_user_can_not_access_admin_dashboard()
    {
        $user_group = factory(UserGroup::class)->create([
            'id' => 3,
            'title' => 'user',
        ]);

        $user = factory(User::class)->create([
            'user_group_id' => $user_group->id,
        ]);

        $this->actingAs($user)
            ->get(route('playfair'))
            ->assertStatus(403);
    }

    /**
     * @test
     */
    public function normal_user_can_not_access_dealer_dashboard()
    {
        $user_group = factory(UserGroup::class)->create([
            'id' => 3,
            'title' => 'user',
        ]);

        $user = factory(User::class)->create([
            'user_group_id' => $user_group->id,
        ]);

        $this->actingAs($user)
            ->get(route('dealer'))
            ->assertStatus(403);
    }

    /**
     * @test
     */
    public function dealer_can_not_access_admin_dashboard()
    {
        $user_group = factory(UserGroup::class)->create([
            'id' => 2,
            'title' => 'dealer',
        ]);

        $dealer = factory(User::class)->create([
            'user_group_id' => $user_group->id,
        ]);

        $this->actingAs($dealer)
            ->get(route('playfair'))
            ->assertStatus(403);
    }

    /**
     * @test
     */
    public function guest_can_not_access_admin_dashboard()
    {
        $this->get(route('playfair'))
            ->assertStatus(302)
            ->assertRedirect('login');
    }

    /**
     * @test
     */
    public function guest_can_not_access_dealer_dashboard()
    {
        $this->get(route('dealer'))
            ->assertStatus(302);
    }
}
