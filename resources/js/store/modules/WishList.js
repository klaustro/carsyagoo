let state = {
    data: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    loading: false,
}

let actions = {
    getWishLists(context, params){
        console.log(params)
        context.state.loading = true
        axios.get('/api/wish-list?page=' + params.page)
            .then(response => {
                context.commit('getWishLists', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    storeWishList(context, payload){
        context.state.loading = true
        axios.post('/api/wish-list', payload)
            .then(response => {
                let newWishList = {
                    id: response.data.id,
                    ads_manager_id: payload.ads_manager_id,
                }
                context.commit('storeWishList', newWishList)
                Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeWishList(context, payload){
        context.state.loading = true
        axios.delete('/api/wish-list/' + payload.ads_manager_id)
            .then(response => {
                console.log(payload)
                context.commit('removeWishList', payload)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                context.state.loading = false
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })

    },
}

let mutations = {
    getWishLists(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.data = data.data;
    },

    storeWishList(state, newWishList){
        state.data.unshift(newWishList);
    },

    removeWishList(state, payload)    {
        //
    },
}

export default {
    state,
    actions,
    mutations
}