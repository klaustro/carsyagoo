<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingServiceDealer extends Model
{
    public $table = 'billing_service_dealer';

    public $timestamps = false;
}
