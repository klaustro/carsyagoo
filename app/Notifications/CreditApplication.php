<?php

namespace App\Notifications;

use App\AdManager;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CreditApplication extends Notification
{
    public $draft;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($draft)
    {
        //
        $this->draft = $draft;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $ads = AdManager::find($this->draft['ads_id']);
        $message = new MailMessage;
        $message->greeting('Hi, ' . $notifiable->first_name . ' ' . $notifiable->last_name)
            ->line('You recive a new credit application by ' . $this->draft['first_name'] . ' ' . $this->draft['last_name'])
            ->line('**For dealer:** ' . $ads->dealer->full_name)
            ->line('**Car year:** ' . $ads->year)
            ->line('**Car make:** ' . $ads->make_name)
            ->line('**Car model:** ' . $ads->model_name);

        foreach ($this->draft as $key => $draft) {
            if (is_array($draft)) {
                $message->line('_' . ucwords(str_replace('_', ' ', $key)) . ':_');
                foreach ($draft as $subkey => $subdraft) {
                    $name = ucwords(str_replace('_', ' ', $subkey));
                    $message->line("**$name:** $subdraft");
                }
            } else {
                $name = ucwords(str_replace('_', ' ', $key));
                $message->line("**$name:** $draft");
            }
        }

        $message->action('See ADS Applied', url('/gallery/anouncement/' . $this->draft['ads_id']));

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
