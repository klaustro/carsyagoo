<?php

namespace Tests\Feature;

use App\AdManager;
use App\Dealer;
use App\User;
use App\UserGroup;
use App\WishList;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class WishListTest extends TestCase
{
    use RefreshDatabase;

    protected $user, $dealer;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            'user_group_id' => factory(UserGroup::class)->create()->id,
        ]);

        $this->dealer = factory(Dealer::class)->create([
            'user_id' => $this->user->id
        ]);

        $this->actingAs($this->user);
    }

    /** @test */
    function if_wish_list_is_paginated()
    {
        $ads_managers = factory(AdManager::class, 6)->create([
            'dealer_id' => $this->dealer->id,
        ]);


        $ads_managers->map(function($ads_manager){
            factory(WishList::class)->create([
                'user_id' => $this->user->id,
                'ads_manager_id' => $ads_manager->id,
            ]);
        });

        $this->get('api/wish-list?page=1')
             ->assertSuccessful()
             ->assertSee($ads_managers[4]->embedcode)
             ->assertDontSee($ads_managers[0]->embedcode);

    }

    /**
     * @test
     */
    public function if_wish_list_is_stored()
    {
        $ads_manager = factory(AdManager::class)->create([
            'dealer_id' => $this->dealer->id,
        ]);

        $response = $this->post('api/wish-list', [
            'ads_manager_id' => $ads_manager->id,
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.wishList.store_message')]);

        $this->assertDatabaseHas('wish_lists',[
            'user_id' => $this->user->id,
            'ads_manager_id' => $ads_manager->id,
        ]);
    }

    /**
     * @test
     */
    public function if_wish_list_is_deleted()
    {
        $ads_manager = factory(AdManager::class)->create();

        $response = $this->delete('api/wish-list/' . $ads_manager->id)
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.wishList.delete_message')]);

        $this->assertDatabaseMissing('wish_lists',[
            'user_id' => $this->user->id,
            'ads_manager_id' => $ads_manager->id,
        ]);
    }
}
