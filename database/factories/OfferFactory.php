<?php

use App\AdManager;
use Faker\Generator as Faker;

$factory->define(App\Offer::class, function (Faker $faker) {
    return [
        'ads_manager_id' => function(){
            return firstOrFactory(AdManager::class);
        } ,
        'name' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'amount' => rand(500, 1000),
    ];
});
