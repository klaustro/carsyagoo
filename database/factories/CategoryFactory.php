<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'title' => $faker->title,
        'alias' => $faker->unique()->name,
        'metadata' => $faker->word,
        'metakey' => $faker->word,
        'metadesc' => $faker->word,
        'status' => $faker->boolean,
    ];
});
