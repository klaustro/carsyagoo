import Vue from 'vue'
import Router from 'vue-router'
import Index from '../components/Home.vue'
import User from '../components/back/users/Index.vue'
import Category from '../components/back/categories/Index.vue'
import CatEquipment from '../components/back/catEquipments/Index.vue'
import Condition from '../components/back/conditions/Index.vue'
import Color from '../components/back/colors/Index.vue'
import BodyType from '../components/back/bodyTypes/Index.vue'
import Make from '../components/back/makes/Index.vue'
import Model from '../components/back/models/Index.vue'
import Driver from '../components/back/drivers/Index.vue'
import Equipment from '../components/back/equipments/Index.vue'
import Transmission from '../components/back/transmissions/Index.vue'
import Toasted from 'vue-toasted'
import SettingGroup from '../components/back/setting_groups/Index.vue'
import Config from '../components/back/settings/Index.vue'
import Dealer from '../components/back/dealers/Index.vue'
import EditDealer from '../components/back/dealers/Edit.vue'
import DealerSocial from '../components/back/dealerSocials/Index.vue'
import Invoice from '../components/back/invoices/Index.vue'
import BlogCategory from '../components/back/blogCategories/Index.vue'
import SocialMedia from '../components/back/socialMedia/Index.vue'
import Tag from '../components/back/tag/Index.vue'
import AdminPost from '../components/back/post/Index.vue'
import BillingTerms from '../components/back/billingTerms/Index.vue'
import BillingServices from '../components/back/billingServices/Index.vue'
import BillingPayments from '../components/back/billingPayments/Index.vue'
import ContactList from '../components/back/contact/Index.vue'
import OfferList from '../components/back/offer/Index.vue'


//Front
import DealerHome from '../components/front/dealer/Index.vue'
import DealerDashboard from '../components/front/dealer/Dashboard.vue'
import DealerAds from '../components/front/dealer/ads/Index.vue'
import AdsEdit from '../components/front/dealer/ads/Edit.vue'
import AdsCreate from '../components/front/dealer/ads/Edit.vue'
import Subscribe from '../components/front/dealer/subscribe/Index.vue'
import Payment from '../components/front/dealer/subscribe/Payment.vue'
import DealerInvoice from '../components/front/dealer/invoice/Index.vue'
import DealerPaymentMethod from '../components/front/dealer/payment_method/Index.vue'
import DealerSocialNetwork from '../components/front/dealer/social/Index.vue'
import Contact from '../components/front/contact/Index.vue'

import Blog from '../components/front/blog/Index.vue'
import Posts from '../components/front/blog/post/Index.vue'
import Post from '../components/front/blog/post/Show.vue'
import Profile from '../components/front/dealer/profile/index.vue'
import Gallery from '../components/front/gallery/Index.vue'
import Anouncements from '../components/front/gallery/anouncements/Index.vue'
import Anouncement from '../components/front/gallery/anouncements/Show.vue'


Vue.use(Router)

let router = new Router({
	mode: 'history',
	routes: [

		{
			path: '/contact',
			component: Index,
		},
		{
			path: '/playfair',
			component: Index,
			children: [
				{
					path: '',
					component: Category
				},
				{
					path: 'users',
					component: User
				},
				{
					path: 'colors',
					component: Color
				},
				{
					path: 'categories',
					component: Category
				},
				{
					path: 'cat-equipments',
					component: CatEquipment
				},
				{
					path: 'conditions',
					component: Condition
				},
				{
					path: 'drivers',
					component: Driver
				},
				{
					path: 'body-types',
					component: BodyType
				},
				{
					path: 'makes',
					component: Make
				},
				{
					path: 'models',
					component: Model
				},
				{
					path: 'equipments',
					component: Equipment
				},
				{
					path: 'transmissions',
					component: Transmission

				},
				{
					path: 'setting-group',
					component: SettingGroup
				},
				{
					name: 'settings',
					path: 'configuration',
					component: Config
				},
				{
					path: 'dealers',
					component: Dealer,
					name:'dealers'
				},
				{
					path: 'create-dealer',
					component: EditDealer,
					name:'createDealer'

				},
				{
					path: 'edit-dealer/:id',
					component: EditDealer,
					name:'edit-dealer'

				},
				{
					path: 'dealer-socials',
					name: 'dealer-socials',
					component: DealerSocial,
				},
				{
					path: 'dealer-socials-by-dealer/:dealer_id',
					name: 'socials-by-dealer',
					component: DealerSocial,
				},
				{
					path: 'billing-terms',
					component: BillingTerms
				},
				{
					path: 'billing-services',
					component: BillingServices
				},
				{
					path: 'billing-payments',
					name: 'admin-billing-payment',
					component: BillingPayments
				},
				{
					path: 'dealer-billing-payments/:dealer_id',
					name: 'dealer-billing-payment',
					component: BillingPayments
				},
				{
					path: 'blog-categories',
					component: BlogCategory
				},
				{
					path: 'invoices',
					component: Invoice
				},
				{
					path: 'social-media',
					component: SocialMedia
				},
				{
					path: 'tag',
					component: Tag
				},
				{
					path: 'post',
					component: AdminPost
				},
				{
					path: 'contact-list',
					component: ContactList
				},
				{
					path: 'offer-list',
					component: OfferList
				},

			]
		},
		{
			path: '/dealer',
			component: DealerHome,
			props:true,
			children:[
				{
					name: 'dealer-home',
					path: '',
					component: DealerDashboard
				},
				{
					name: 'dealer-dashboard',
					path: 'dashboard',
					component: DealerDashboard
				},
				{
					name: 'dealer-ads',
					path: 'ads',
					component: DealerAds
				},
				{
					name: 'ads-edit',
					path: 'ads-edit/:id',
					component: AdsEdit,
				},
				{
					name: 'ads-create',
					path: 'ads-create',
					component: AdsEdit,
				},
				{
					name: 'profile',
					path: 'profile',
					component: Profile
				},
				{
					name: 'subscribe',
					path: 'subscribe',
					component: Subscribe
				},
				{
					name: 'payment',
					path: 'payment/:service_id',
					component: Payment
				},
				{
					name: 'dealer-invoice',
					path: 'invoice/',
					component: DealerInvoice
				},
				{
					name: 'dealer-payment-method',
					path: 'payment-method',
					component: DealerPaymentMethod
				},
				{
					name: 'dealer-social-network',
					path: 'social-network',
					component: DealerSocialNetwork
				},
				{
					name: 'add-payment-method',
					path: 'add-payment-method',
					component: Payment
				},
				{
					name: 'edit-payment-method',
					path: 'edit-payment-method/:id',
					component: Payment
				},
			],
		},

		{
			path: '/blog',
			component: Blog,
			children:[
				{
					name: 'blog-home',
					path: '',
					component: Posts
				},
				{
					name: 'blog-posts',
					path: 'posts',
					component: Posts
				},
				{
					name: 'show-post',
					path: 'post/:slug',
					component: Post,
				},
				{
					name: 'by-category',
					path: 'by-category/:blog_category_id',
					component: Posts,
				},
				{
					name: 'by-tag',
					path: 'by-tag/:tag_id',
					component: Posts
				},
			],
		},
		{
			path: '/gallery',
			component: Gallery,
			children:[
				{
					name: 'anouncement',
					path: '',
					component: Anouncements
				},
				{
					name: 'search-anouncements',
					path: 'search-anouncements',
					component: Anouncements,
					props:true,
				},
				{
					name: 'show-anouncement',
					path: 'anouncement/:id',
					component: Anouncement,
				},
				{
					name: 'by-model',
					path: 'by-model/:model_id',
					component: Anouncements,

				},
				{
					name: 'by-make',
					path: 'by-make/:make_id',
					component: Anouncements,

				},
				{
					name: 'by-year',
					path: 'by-year/:min_year/:max_year',
					component: Anouncements,

				},
				{
					name: 'by-bodytype',
					path: 'by-bodytype/:bodytype_id',
					component: Anouncements,

				},
				{
					name: 'by-transmission',
					path: 'by-transmission/:transmission_id',
					component: Anouncements,

				},
				{
					name: 'by-fuel',
					path: 'by-fuel/:fuel_id',
					component: Anouncements,

				},
				{
					name: 'by-state',
					path: 'by-state/:state',
					component: Anouncements,

				},
			],
		},

		{
			path: '*',
			component:
			{
				template : '<h1>Error 404</h1>'
			}
		},

	]
})


export default router