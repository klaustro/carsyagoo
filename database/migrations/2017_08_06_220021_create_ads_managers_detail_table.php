<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsManagersDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('ads_managers_detail', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('ads_manager_id');
            $table->foreign('ads_manager_id')->references('id')->on('ads_managers')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->string('urlphoto');
            $table->boolean('state')->default(true);
            $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads_managers_detail');
    }
}
