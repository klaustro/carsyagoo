let state = {
    catEquipments: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading:false
}

let getters = {
    findCatEquipment(state){
        return function(id){
            let catEquipment = state.catEquipments.find(catEquipment => catEquipment.id == id)
            return catEquipment;
        }
    },
}

let actions = {
    getCatEquipments(context, params){
        context.state.loading = true
        axios.get('/admin/cat-equipament?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc )
            .then(response => {
                context.commit('getCatEquipments', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    storeCatEquipment(context, payload){
        context.state.loading = true
        axios.post('/admin/cat-equipament', payload)
            .then(response => {
                let newCatEquipment = {
                    id: response.data.catEquipment.id,
                    name: response.data.catEquipment.name,
                    image: response.data.catEquipment.image,
                    category_id: response.data.catEquipment.category_id,
                    status: response.data.catEquipment.status,
                }
                Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                context.commit('storeCatEquipment', newCatEquipment)
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateCatEquipment(context, payload){
        context.state.loading = true
        axios.put('/admin/cat-equipament/' + payload.id, payload)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateCatEquipment', payload)
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeCatEquipment(context, id){
        context.state.loading = false
        axios.delete('/admin/cat-equipament/' + id)
            .then(response => {
                context.commit('removeCatEquipment', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'info'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })

    },

    listCatEquipments(context){
        axios.get('/admin/cat-equipmentList')
            .then(response => {
                context.commit('listCatEquipments', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getCatEquipments(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.catEquipments = data.data;
    },

    storeCatEquipment(state, newCatEquipment){
        newCatEquipment.image = '/images/cat_equipments/' + newCatEquipment.image
        state.catEquipments.unshift(newCatEquipment);
    },

    updateCatEquipment(state, {id, draft}){
        draft.image = '/images/cat_equipments/' + draft.image
        let index = state.catEquipments.findIndex(catEquipment => catEquipment.id == id);
        state.catEquipments.splice(index, 1, draft);
    },

    removeCatEquipment(state, id)    {
        let index = state.catEquipments.findIndex(catEquipment => catEquipment.id == id);
        state.catEquipments.splice(index, 1);
    },

    listCatEquipments(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}