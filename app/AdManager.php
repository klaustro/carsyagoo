<?php

namespace App;

use App\Scopes\AdManagerScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Stevebauman\Location\Facades\Location;

class AdManager extends Model
{
    protected $table = 'ads_managers';

    protected $fillable = ['category_id', 'make_id', 'model_id', 'condition_id', 'dealer_id', 'bodytype_id','driver_id','fuel_id','transmission_id','ext_color_id','specific_color', 'equipment_detail','year','vincode','mileage','price', 'frequency','bargain_price','doors','seats','engine','expire','embedcode','is_commercial','is_featured','is_top','is_reserved','is_special','hits','status','otherinfo','unfaden_weight','unfaden_weight','gross_weight','length','width','disclaimer','is_metalic_color','specific_model','specific_trans','export_price','fuel_consum_city','fuel_consum_freeway','fuel_consum_combined','acceleration','max_speed','C02','image_count','video_link','main_image', 'int_color_id'];

    protected $appends = ['make_name', 'model_name', 'bodytype_name', 'transmission_name', 'city', 'state', 'address', 'zipcode', 'latitude', 'longitude', 'is_wish_list', 'review_average', 'own_review', 'dealer_name'];


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new AdManagerScope);
    }

    public function category()
    {
        return $this->BelongsTo('App\Category');
    }

    public function dealer()
    {
        return $this->BelongsTo('App\Dealer');
    }

    public function make()
    {
        return $this->BelongsTo('App\Make');
    }

    public function model()
    {
        return $this->BelongsTo('App\CarModel');
    }

    public function condition()
    {
        return $this->BelongsTo('App\Condition');
    }

    public function bodytype()
    {
        return $this->BelongsTo('App\BodyType');
    }

    public function driver()
    {
        return $this->BelongsTo('App\Driver');
    }

    public function fuel()
    {
        return $this->BelongsTo('App\Fuel');
    }

    public function transmission()
    {
        return $this->BelongsTo('App\Transmission');
    }

    public function extColor()
    {
        return $this->BelongsTo('App\Color','ext_color_id');
    }

    public function specifictColor()
    {
        return $this->BelongsTo('App\Color','specific_color');
    }

    public function intColor()
    {
        return $this->BelongsTo('App\Color','int_color_id');
    }

    public function images()
    {
        return $this->hasmany('App\AdManagerDetail','ads_manager_id');
    }

    public function equipments()
    {
        return $this->belongsToMany(Equipment::class, 'ads_manager_equipment', 'ads_manager_id');
    }

    public function review()
    {
        return $this->hasMany(Review::class, 'ads_manager_id')->orderBy('id', 'desc');
    }
     /**
     * Method to search by any column
     * @param  Query $query
     * @param  string $target [description]
     * @return Query
     */


    public function scopeFilterDealer($query, $dealer_id)
    {
        if ($dealer_id != '') {
            return $query->where('dealer_id', $dealer_id);
        }
    }

    public function scopeModelSearch($query, $model_id)
    {
        if ($model_id != '') {
            return $query->where('model_id',$model_id);
        }
    }

    public function scopeMakeSearch($query, $model_id)
    {
        if ($model_id != '' && $model_id != 'all') {
            return $query->where('make_id',$model_id);
        }
    }

    public function scopeZipcodeSearch($query, $zipcode)
    {
        if ($zipcode != '') {
            return $query->where('zip_code',$zipcode);
        }
    }

    public function scopeColorSearch($query, $ext_color)
    {
        if ($ext_color != '') {
            return $query->where('ext_color_id', $ext_color);
        }
    }

    public function scopePriceSearch($query, $min_price, $max_price)
    {
        if ($max_price != '' &&  $min_price != '') {
            return $query->whereBetween('price',[$min_price, $max_price]);
        } elseif ($max_price == '' &&  $min_price != '') {
            return $query->where('price', '>=', $min_price);
        } elseif ($min_price == '' && $max_price != '') {
            return $query->where('price', '<=', $max_price);
        }
        return $query;
    }

    public function scopeMileageSearch($query, $min_mileage, $max_mileage)
    {
        if ($max_mileage != '' &&  $min_mileage != '') {
            return $query->whereBetween('mileage',[$min_mileage, $max_mileage]);
        } elseif ($max_mileage == '' &&  $min_mileage != '') {
            return $query->where('mileage', '>=', $min_mileage);
        } elseif ($min_mileage == '' && $max_mileage != '') {
            return $query->where('mileage', '<=', $max_mileage);
        }
        return $query;
    }

    public function scopeYearSearch($query, $min_year, $max_year)
    {
        if ($max_year != '' &&  $min_year != '') {
            return $query->whereBetween('year',[$min_year, $max_year]);
        } elseif ($max_year == '' &&  $min_year != '') {
            return $query->where('year', '>=', $min_year);
        } elseif ($min_year == '' && $max_year != '') {
            return $query->where('year', '<=', $max_year);
        }
        return $query;
    }

    public function scopeBodytypeSearch($query, $bodytype_id)
    {
        if ($bodytype_id != '') {
            return $query->where('bodytype_id',$bodytype_id);
        }
    }

    public function scopeTransmissionSearch($query, $transmission_id)
    {
        if ($transmission_id != '') {
            return $query->where('transmission_id',$transmission_id);
        }
    }
    public function scopeFuelSearch($query, $fuel_id)
    {
        if ($fuel_id != '') {
            return $query->where('fuel_id',$fuel_id);
        }
    }

    public function scopeRelateds($query, $id)
    {
        $ads = $this->withoutGlobalScope(AdManagerScope::class)->find($id);
        return $query->where('make_id', $ads->make_id)
                ->where('model_id', $ads->model_id)
                ->where('id', '!=', $id);
    }

    public function scopeSpecificModelFilter($query, $specific_model)
    {
        if ($specific_model != '') {
            return $query->where('specific_model', $specific_model);
        }
    }

    public function scopeByState($query, $state)
    {
        if ($state != '') {
            $now = Carbon::now();
            $date_diff = $now->subDays(getConfig('days_expires'));
            return $query->whereHas('dealer', function($query) use($state){
                    $query->whereHas('user', function($query) use($state){
                        $query->where('blocked_user', 0);
                            $query->whereHas('address', function($query) use($state){
                                    $query->where('state', $state);
                                });
                    });
                })
            ->where(function($query) use($date_diff){
                $query->where('created_at', '>=', $date_diff)
                    ->orWhere('expire', null);
            })
            ->withoutGlobalScope(AdManagerScope::class);
        }
    }

    public function scopeByDistance($query, $latitude, $longitude)
    {

        if ($latitude != 'undefined' || $longitude != 'undefined') {
            $user_ids = Address::select(DB::raw('*, ( 6367 * acos( cos( radians('.$latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( latitude ) ) ) ) AS distance'))
                ->having('distance', '<', getConfig('zipocode_milliage_range'))
                ->orderBy('distance')
                ->get()->pluck('user_id');

            $dealer_ids = Dealer::whereIn('user_id', $user_ids)->get()->pluck('id');

            if (count($dealer_ids) > 0) {
                $query->whereIn('dealer_id', $dealer_ids);
                return $query;
            }


        }
    }

    //Attributes

    public function getMakeNameAttribute()
    {
        return $this->make->name;
    }

    public function getModelNameAttribute()
    {
        return $this->model->name;
    }

    public function getBodytypeNameAttribute()
    {
        return $this->bodytype->name;
    }

    public function getTransmissionNameAttribute()
    {
        return $this->transmission->name;
    }

    public function getCityAttribute()
    {
        return optional($this->dealer)->city;
    }

    public function getStateAttribute()
    {
        return  optional($this->dealer)->state;
    }

    public function getAddressAttribute()
    {
        return optional($this->dealer)->address;
    }

    public function getZipcodeAttribute()
    {
        return optional($this->dealer)->zipcode;
    }

    public function getLatitudeAttribute()
    {
        return $this->dealer->latitude;
    }

    public function getLongitudeAttribute()
    {
        return $this->dealer->longitude;
    }

    public function getIsWishListAttribute()
    {

        if (auth()->guest()) {
            return;
        }

        $wish_list = WishList::where([
            'ads_manager_id'=> $this->id,
            'user_id' => auth()->user()->id,
        ])->first();

        return optional($wish_list)->count() > 0;
    }

    public function getReviewAverageAttribute()
    {
        return $this->review->avg('value');
    }

    public function getOwnReviewAttribute()
    {
        if (auth()->guest()) {
            return;
        }

        return $this->review->where('user_id', auth()->user()->id)->first();
    }


    public function getDealerNameAttribute()
    {
        return $this->dealer->user->first_name . ' ' . $this->dealer->user->last_name;
    }

}
