<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const ADMIN = 1;
    const DEALER = 2;
    const USER = 3;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password','type','phone','address_1','address_2', 'state','city','zipcode','brithdday','login','registration_ip','bio','user_group_id'
    ];

    //Relationships

    public function dealer()
    {
        return $this->hasOne(Dealer::class, 'user_id', 'id');
    }

    public function address()
    {
        return $this->hasOne(Address::class);
    }

    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->
                where('first_name', 'like', "%$target%")
                ->orWhere('login', 'like', "%$target%")
                ->orWhere('email', 'like', "%$target%")
                ->orWhere('last_name', 'like', "%$target%")
                ->orWhere('phone', 'like', "%$target%")
                ->orWhere('registration_ip', 'like', "%$target%");
        }
    }

    public function scopeActive($query)
    {
        return $query->where('blocked_user', 0);
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //Attributes

    public function getIsAdminAttribute()
    {
        return $this->user_group_id == 1;
    }

    public function getIsDealerAttribute()
    {
        return $this->user_group_id == 2;
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
