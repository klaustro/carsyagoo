<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'login' => 'carsyagoo',
            'user_group_id' => 1,
        ]);

        factory(User::class)->create([
            'login' => 'dealer',
            'user_group_id' => 2,
        ]);

        factory(User::class)->create([
            'login' => 'user',
            'user_group_id' => 3,
        ]);

        factory(User::class,7)->create([
            'user_group_id' => 3,
        ]);
    }
}
