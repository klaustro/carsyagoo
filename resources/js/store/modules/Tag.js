let state = {
    data: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading: false,
}

let actions = {
    getTags(context, params){
        context.state.loading = true
        axios.get('/admin/tag?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getTags', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    storeTag(context, payload){
        context.state.loading = true
        axios.post('/admin/tag', payload)
            .then(response => {
                let newTag = {
                    id: response.data.id,
                    name: payload.name,
                }
                context.commit('storeTag', newTag)
                Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateTag(context, payload){
        context.state.loading = true
        axios.put('/admin/tag/' + payload.id, payload)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateTag', payload)
                context.state.loading = false

            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeTag(context, id){
        context.state.loading = true
        axios.delete('/admin/tag/' + id)
            .then(response => {
                context.commit('removeTag', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                context.state.loading = false
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })

    },

    listTags(context){
        axios.get('/admin/tagList')
            .then(response => {
                context.commit('listTags', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getTags(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.data = data.data;
    },

    storeTag(state, newTag){
        state.data.unshift(newTag);
    },

    updateTag(state, {id, draft}){
        let index = state.data.findIndex(tag => tag.id == id);
        state.data.splice(index, 1, draft);
    },

    removeTag(state, id)    {
        let index = state.data.findIndex(tag => tag.id == id);
        state.data.splice(index, 1);
    },

    listTags(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    actions,
    mutations
}