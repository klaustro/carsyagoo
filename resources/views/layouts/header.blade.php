<!-- Start Site Header -->
<div class="site-header-wrapper">
    <header class="site-header">
        <div class="container sp-cont">
            <div class="site-logo">
                <h1><a href="/"><img src="{{ url('assets/images/logo.png') }}" alt="Logo"></a></h1>
                <span class="site-tagline">Let's Find your Car</span>
            </div>
            <div class="header-right">
                @if(auth()->guest())
                <div class="user-login-panel">
                    <a href="/login" class="user-login-btn"><i class="icon-profile"></i></a>
                </div>
                @endif
                <div class="topnav dd-menu">
                    <ul class="top-navigation sf-menu">
                        @if(auth()->guest())
                        <li><a href="/register">Join</a></li>
                        @else
                            <li>
                                <a href="javascript:void(0)"><i class="icon-profile"></i> {{auth()->user()->first_name}}</a>
                                <ul class="dropdown">
                                    <li>
                                        <a href="/wish-list"><i class="fa fa-star-o" aria-hidden="true"></i> Wish list</a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                                        <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                                            {!! csrf_field() !!}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- End Site Header -->