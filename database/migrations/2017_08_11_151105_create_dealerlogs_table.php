<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealerlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delaerlogs', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('date_exc');
            $table->unsignedInteger('dealer_id');
            $table->foreign('dealer_id')->references('id')->on('dealers')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->string('component');
            $table->string('city');
            $table->string('state');
            $table->string('postalcode');
            $table->longText('actiondetail');
            $table->ipAddress('ipvisitor');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delaerlogs');
    }
}
