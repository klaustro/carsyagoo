<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'brithdday' => $faker->dateTimeThisCentury,
        'bio' => $faker->paragraph,
        'user_group_id' => function(){
            return firstOrFactory(\App\UserGroup::class);
        },
        'login' => $faker->unique()->name,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'email' => $faker->unique()->email,
        'registration_ip' => $faker->ipv4,
        'recieve_email' => $faker->boolean,
        'blocked_user' => $faker->boolean,
        'require_password_reset' => $faker->boolean,
    ];
});
