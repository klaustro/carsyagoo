@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
    @stack('css')
    @yield('css')
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ? [
    'boxed' => 'layout-boxed',
    'fixed' => 'fixed',
    'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
    <div class="wrapper" id="app">

        <!-- Main Header -->
        <header class="main-header">
            @if(config('adminlte.layout') == 'top-nav')
            <nav class="navbar navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="/" class="navbar-brand">
                            <img src="{{asset('/images/logo@2x.png')}}" alt="logo">
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            {{-- @each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item') --}}

                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
            @else
            <!-- Logo -->
            <a href="/" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><img src="{{asset('assets/images/mini-logo.png')}}" height="50" alt="logo"></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><img src="{{asset('assets/images/logo.png')}}" height="50" alt="logo"></span>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                </a>
            @endif
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">

                    <ul class="nav navbar-nav">
                        <li>
                            @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))
                                <a href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}">
                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                </a>
                            @else
                                <a href="#"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                >
                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                </a>
                                <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                                    @if(config('adminlte.logout_method'))
                                        {{ method_field(config('adminlte.logout_method')) }}
                                    @endif
                                    {{ csrf_field() }}
                                </form>
                            @endif
                        </li>
                    </ul>
                </div>
                @if(config('adminlte.layout') == 'top-nav')
                </div>
                @endif
            </nav>
        </header>

       <!-- Navbar -->
        @if(config('adminlte.layout') != 'top-nav')
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar" style="height: auto;">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <i class="fa fa-user-circle" aria-hidden="false" style="color:white; font-size: 35px;"></i>
                    </div>
                    <div class="pull-left info">
                        @if(Auth::check())
                        <p>{{Auth()->user()->first_name}} {{Auth()->user()->last_name}}</p>
                        @else
                            <script>window.location = "/";</script>
                        @endif
                    </div>
                </div>

                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu tree" data-widget="tree">
                <li class="header">MAIN NAVIGATION</li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-database"></i>
                        <span>{{ trans('app.links.catalogs') }}</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <router-link tag="li" to="/playfair/categories" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.category.top_title') }}</a>
                        </router-link >

                        <router-link tag="li" to="/playfair/users" >
                            <a><i class="fa fa-user-o"></i> {{ trans('app.user.title') }}</a>
                        </router-link >

                        <router-link tag="li" to="/playfair/cat-equipments" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.catEquipment.title') }} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/equipments" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.equipment.title') }} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/conditions" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.condition.title') }} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/body-types" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.bodyType.title') }} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/makes" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.make.title') }} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/models" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.model.title') }} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/colors" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.color.title') }}</a>
                        </router-link >
                        <router-link tag="li" to="/playfair/transmissions" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.transmission.title') }} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/drivers" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.driver.title') }}</a>
                        </router-link >
                        <router-link tag="li" to="/playfair/contact-list" >
                            <a><i class="fa fa-envelope"></i> {{ trans('app.contact.title') }}</a>
                        </router-link >
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-user"></i>
                        <span>{{ trans('app.links.dealers') }}</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <router-link tag="li" to="/playfair/dealers" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.dealer.title') }} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/dealer-socials" >
                           <a><i class="fa fa-circle-o"></i> {{ trans('app.dealerSocial.title') }} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/billing-terms" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.billingTerm.title') }} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/billing-payments" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.billingPayment.title') }} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/invoices" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.invoice.title') }} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/offer-list" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.offer.title') }} </a>
                        </router-link >
                        <li>
                            <a href="/dealer"><i class="fa fa-circle-o"></i>{{ trans('app.adManager.title') }}</a>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-rss-square"></i>
                        <span>Blog</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <router-link tag="li" to="/playfair/blog-categories" >
                            <a><i class="fa fa-link"></i> {{ trans('app.blogCategory.title') }} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/tag" >
                            <a><i class="fa fa-link"></i> {{trans('app.tag.title')}} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/post" >
                            <a><i class="fa fa-link"></i> {{trans('app.post.name')}} </a>
                        </router-link >
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-sliders"></i>
                        <span>Settings</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <router-link tag="li" to="/playfair/social-media" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.socialMedia.title') }} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/setting-group" >
                            <a><i class="fa fa-cogs"></i> {{trans('app.setting_group.title')}} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/configuration" >
                            <a><i class="fa fa-wrench"></i> {{trans('app.setting.title')}} </a>
                        </router-link >
                        <router-link tag="li" to="/playfair/billing-services" >
                            <a><i class="fa fa-circle-o"></i> {{ trans('app.billingService.title') }} </a>
                        </router-link >
                    </ul>
                </li>
            </section>
        <!-- /.sidebar -->
        </aside>
        @endif
        <!-- /.fin navbar -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @if(config('adminlte.layout') == 'top-nav')
            <div class="container">
            @endif

            <!-- Content Header (Page header) -->
            <section class="content-header">
                @yield('content_header')
            </section>

            <!-- Main content -->
            <section class="content">

                @yield('content')

            </section>
            <!-- /.content -->
            @if(config('adminlte.layout') == 'top-nav')
            </div>
            <!-- /.container -->
            @endif
        </div>
        <!-- /.content-wrapper -->

    </div>
    <!-- ./wrapper -->
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{getConfig('google_api_key')}}&libraries=places"></script>

    @stack('js')
    @yield('js')
@stop
