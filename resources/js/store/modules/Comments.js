let state = {
    data: [],
    loading: false,
}

let getters = {
        findComment(state){
            return function(id){
                let comment = state.data.find(comment => comment.id == id)
                return comment;
            }
        },
}

let actions = {
    getComments(context, params){
        context.state.loading = true
        axios.get('/api/comment?post_id=' + params.post_id)
            .then(response => {
                context.commit('getComments', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    storeComment(context, payload){
        context.state.loading = true
        axios.post('/api/comment', payload)
            .then(response => {
                let newComment = response.data.comment
                context.commit('storeComment', newComment)
                Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateComment(context, payload){
        context.state.loading = true
        axios.put('/api/comment/' + payload.id, payload)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateComment', payload)
                context.state.loading = false

            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeComment(context, id){
        context.state.loading = true
        axios.delete('/api/comment/' + id)
            .then(response => {
                context.commit('removeComment', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                context.state.loading = false
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })

    },
}

let mutations = {
    getComments(state, {data}){
        state.data = data;
    },

    storeComment(state, newComment){
        state.data.push(newComment);
    },

    updateComment(state, {id, draft}){
        let index = state.data.findIndex(comment => comment.id == id);
        state.data.splice(index, 1, draft);
    },

    removeComment(state, id)    {
        let index = state.data.findIndex(comment => comment.id == id);
        state.data.splice(index, 1);
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}