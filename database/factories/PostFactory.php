<?php

use App\BlogCategory;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Post::class, function (Faker $faker) {
    $title = $faker->sentence;
    return [
        'user_id' => function(){
            return firstOrFactory(User::class);
        },
        'blog_category_id' => function(){
            return firstOrFactory(BlogCategory::class);
        },
        'title' => $title,
        'image' => '/images/post.jpg',
        'intro' => $faker->paragraph(3),
        'content' => $faker->paragraph(10),
        'slug' => Str::slug($title),
        'image_alt' => $faker->word,
        'metadesc' => $faker->sentence,
        'metakeywords' => $faker->sentence,
        'publish_date' => $faker->date,
    ];
});
