<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    protected $fillable = [
        'name','image', 'category_id','status'
    ];

    public function category()
    {
        return $this->BelongsTo('App\Category');
    }

    public function scopeSearch($query, $target)
    {

        if ($target != '') {
            return $query->where('name', 'like', "%$target%")
                    ->orWhereHas('category', function ($query) use ($target) {
                        $query->Where('name', 'like',"%$target%");
                    });
        }
    }

}
