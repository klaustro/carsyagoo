let state = {
    socialMedia: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading: false,
}

let getters = {
        findSocialMedia(state){
            return function(id){
                let socialMedia = state.socialMedia.find(socialMedia => socialMedia.id == id)
                return socialMedia;
            }
        },
}

let actions = {
    getSocialMedia(context, params){
        context.state.loading = true
        axios.get('/admin/social-media?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getSocialMedia', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    storeSocialMedia(context, payload){
        context.state.loading = true
        axios.post('/admin/social-media', payload)
            .then(response => {
                let newSocialMedia = {
                    id: response.data.id,
                    name: response.data.data.name,
                    icon: response.data.data.icon,
                    url: response.data.data.url,
                }
                Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                context.commit('storeSocialMedia', newSocialMedia)
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateSocialMedia(context, payload){
        context.state.loading = true
        axios.put('/admin/social-media/' + payload.id, payload)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateSocialMedia', payload)
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeSocialMedia(context, id){
        context.state.loading = true
        axios.delete('/admin/social-media/' + id)
            .then(response => {
                context.commit('removeSocialMedia', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })

    },

    listSocialMedia(context){
        axios.get('/admin/social-mediaList')
            .then(response => {
                context.commit('listSocialMedia', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getSocialMedia(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.socialMedia = data.data;
    },

    storeSocialMedia(state, newSocialMedia){
        state.socialMedia.unshift(newSocialMedia);
    },

    updateSocialMedia(state, {id, draft}){
        draft.icon = draft.icon ? '/images/social_media/' + draft.icon : ''
        let index = state.socialMedia.findIndex(socialMedia => socialMedia.id == id);
        state.socialMedia.splice(index, 1, draft);
    },

    removeSocialMedia(state, id)    {
        let index = state.socialMedia.findIndex(socialMedia => socialMedia.id == id);
        state.socialMedia.splice(index, 1);
    },

    listSocialMedia(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}