<?php

use Faker\Generator as Faker;

$factory->define(App\Make::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence,
        'alias' => uniqid(),
        'image' => $faker -> imageUrl(640, 480),
        'metakey' => $faker->randomNumber(),
        'metadesc' => $faker->randomNumber(),
        'category_id' => function(){
            return firstOrFactory(\App\Category::class);
        },
        'status' => $faker->boolean,

    ];
});
