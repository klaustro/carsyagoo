<?php

use App\Post;
use App\PostTag;
use Illuminate\Database\Seeder;

class PostTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Post::all() as $post) {
            factory(PostTag::class)->create([
                'tag_id' => rand(1, 10),
                'post_id' => $post->id
            ]);

            factory(PostTag::class)->create([
                'tag_id' => rand(1, 10),
                'post_id' => $post->id
            ]);

            factory(PostTag::class)->create([
                'tag_id' => rand(1, 10),
                'post_id' => $post->id
            ]);
        }
    }
}
