<?php

namespace App\Http\Controllers;

use App\Billing;
use App\BillingPayment;
use App\BillingService;
use App\BillingServiceDealer;
use App\Payment;
use App\Charge;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Method to subscribe dealer to a Billing Service
     * @return Response
     */
    public function subscribe()
    {
        $dealer_id = auth()->user()->dealer->id;
        $method = request()->method;
        $credit_card = request()->card;

        $billing = new Billing();
        $items = $billing->setBillingService(request()->service_id);

        $charge_payment = new Charge($dealer_id, $method, $credit_card, $items);
        //$response = $charge_payment->store();
        if ($method == 'credit_card') {
            $response = $charge_payment->payFlow();
        } else {
            $response = $charge_payment->expressCheckout();
        }

        if (array_key_exists('error', $response)) {
            return response()->json($response, 400);
        }

        //Set service to Dealer
        $this->setBillingServicesDealer(request()->service_id);

        //store billing data
        if (isset($credit_card)) {
            $this->storeBillingPayment($dealer_id, $credit_card);
        }

        return response()->json($response, 200);
    }

    /**
     * Method to bill a montly subscripcion
     * @return Response
     */
    public function billing()
    {
        $billing = new Billing();
        return $billing->recurrent();
    }

    /**
     * Method to redirect to Paypal success view
     * @return View
     */
    public function paypalSuccess()
    {
        $status = 'approved';
        $transaction_id = request()->paymentId;

        $payment = Payment::where('result_id', $transaction_id)->first();
        $payment->state = $status;
        $payment->save();

        //Set Billing Service to dealer
        $this->setBillingServicesDealer($payment->billing_service_id);

        return view('subscription.paypal_response', compact('status', 'transaction_id'));
    }

    /**
     * Method to redirect to Paypal fail view
     * @return View
     */
    public function paypalFails()
    {
        $status = 'fail';
        return view('subscription.paypal_response', compact('status'));
    }

    /**
     * Method to store Billing Payment (Payment method) Data
     * @param  integer $dealer_id     [description]
     * @param  array $credit_card [description]
     * @return null
     */
    protected function storeBillingPayment($dealer_id, $credit_card)
    {
        $billing_payment = BillingPayment::updateOrCreate([
            'dealer_id' => $dealer_id,
            'type' => $credit_card['type'],
            'cardnumber' => $credit_card['cardnumber'],
            'expire_month' => $credit_card['expire_month'],
            'expire_year' => $credit_card['expire_year'],
            'first_name' => $credit_card['first_name'],
            'last_name' => $credit_card['last_name'],
            'cvc' => $credit_card['cvc'],
        ]);
        $billing_data = BillingPayment::where('dealer_id', $dealer_id)
            ->where('id', '<>', $billing_payment->id)
            ->get();

        $billing_data->map(function($item){
            $item->primary = 0;
            $item->save();
        });
    }

    /**
     * Method to set Billing Services to a Dealer
     * @return null
     */
    protected function setBillingServicesDealer($service_id)
    {
        $service = BillingService::find($service_id);
        $due_date = Carbon::now()->addDays($service->billing_term->days)->toDateString();
        $dealer = auth()->user()->dealer;

        $dealer->services()->detach();
        $dealer->services()->attach($service_id, ['expires' => $due_date]);
    }
}