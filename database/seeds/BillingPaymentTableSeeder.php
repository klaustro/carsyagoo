<?php

use Illuminate\Database\Seeder;

class BillingPaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\BillingPayment::class)->create([
            'type' => 'visa',
            'cardnumber' => '4012888888881881',
            'expire_year' => '2025',
        ]);
    }
}
