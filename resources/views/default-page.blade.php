@extends('layouts.app')

@section('content')
    <div id="content" class="content full padding-b0">
        <div class="container">
            <div class="row">
                {!! getPage($key) !!}
            </div>
        </div>
    </div>
@endsection