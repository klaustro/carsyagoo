    <div class="container">
        <!-- Search by make -->
        <div class="row">
            <div class="col-md-3 col-sm-4">
                <h3>Search by Makes </h3>
                <a href="{{ url('gallery').'?make=all'}}" class="btn btn-default btn-lg">All make &amp; models</a>
            </div>
            <div class="col-md-9 col-sm-8">
                <div class="row">
                    <ul class="owl-carousel" id="make-carousel" data-columns="5" data-autoplay="6000" data-pagination="no" data-arrows="no" data-single-item="no" data-items-desktop="5" data-items-desktop-small="4" data-items-tablet="3" data-items-mobile="3">
                        @foreach($makes as $make)
                            @if(isset($make->image))
                                <li class="item"> <a href="{{ url('gallery').'?make='.$make->id }}" id="make_id"><img src="{{ url($make->image) }}" alt="{{ $make->name }}" value="{{ $make->id }}"></a></li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    $('body').on('click', '#make_id', function(event){
        event.preventDefault();
        href = $(this).attr("href");
        $.ajax({
                type:"GET",
                url:href,
                dataType:"json",
                // data: {id:id},
                success:function(response){

                }
            });
    });
</script>
