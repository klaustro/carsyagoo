<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SocialMedia;


class SocialMediaController extends Controller
{
     public function index()
	{

        return SocialMedia::search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);

	}


	public function store()
    {
        try {

            $v = \Validator::make(request()->draft, [
                'name' => 'required',
                'url' => 'required',
                ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }


            if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);

                $folder = '/images/social_media/';
                rename(public_path().'/'.request()->images[0]['url'],public_path() . $folder . request()->images[0]['url']);

                $socialMedia = SocialMedia::create([
                    'name' => request()->draft['name'],
                    'url' => request()->draft['url'],
                    'icon' => $folder . request()->images[0]['url'],
                ]);

                return [
                    'message' => trans('app.socialMedia.store_message'),
                    'id' => $socialMedia->id, 'data'=> $socialMedia, 'status' => 1
                ];

            }else{

                $socialMedia = SocialMedia::create([
                    'name' => request()->draft['name'],
                    'url' => request()->draft['url'],
                ]);

                return [
                    'message' => trans('app.socialMedia.store_message'),
                    'id' => $socialMedia->id, 'data'=> $socialMedia, 'status' => 1
                ];
            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.store_error')], 500);
        }
    }

	public function update($id)
    {
        try {

            $v = \Validator::make(request()->draft, [
                'name' => 'required',
                'url' => 'required',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }

            if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);
                $folder = '/images/social_media/';
                rename(public_path().'/'.request()->images[0]['url'],public_path() . $folder . request()->images[0]['url']);

                $socialMedia = SocialMedia::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'url' => request()->draft['url'],
                    'icon' => $folder . request()->images[0]['url'],
                ]);

                return ['message' => trans('app.socialMedia.update_message')];

            }else{
                $socialMedia = SocialMedia::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'url' => request()->draft['url'],
                ]);

                return ['message' => trans('app.socialMedia.update_message')];
            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.update_error')], 500);
        }

    }

    public function delete($id)
    {
        $socialMedia = SocialMedia::destroy($id);

        return ['message' => trans('app.socialMedia.delete_message')];
    }
}
