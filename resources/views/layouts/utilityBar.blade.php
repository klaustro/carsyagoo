<div class="utility-bar">
    <div class="container">
        <div class="row">
            <br>
            <br>
            <div class="col-md-4 col-sm-6 col-xs-8"></div>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <ul class="utility-icons  social-icons-colored">
                    @if(getConfig('facebook_site'))<li class="facebook"><a href="{{ getConfig('facebook_site') }}" target="blank"><i class="fa fa-facebook"></i></a></li>@endif
                    @if(getConfig('twitter_site'))<li class="twitter"><a href="{{ getConfig('twitter_site') }}" target="blank"><i class="fa fa-twitter"></i></a></li>@endif
                    @if(getConfig('googleplus_site'))<li class="googleplus"><a href="{{ getConfig('googleplus_site') }}" target="blank"><i class="fa fa-google-plus"></i></a></li>@endif
                    @if(getConfig('linkedin_site'))<li class="linkedin"><a href="{{ getConfig('linkedin_site') }}" target="blank"><i class="fa fa-linkedin"></i></a></li>@endif
                    @if(getConfig('instagram_site'))<li class="instagram"><a href="{{ getConfig('instagram_site') }}" target="blank"><i class="fa fa-instagram"></i></a></li>@endif
                    @if(getConfig('youtube_site'))<li class="youtube"><a href="{{ getConfig('youtube_site') }}" target="blank"><i class="fa fa-youtube"></i></a></li>@endif
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="container">
            <div class="row">
                @include('layouts.search')
                <hr>
                <div class="body-types" hidden="true">

                <h3>Quick Find by Body Type</h3>
                <ul class="owl-carousel carousel-alt" data-columns="6" data-autoplay="" data-pagination="no" data-arrows="yes" data-single-item="no" data-items-desktop="6" data-items-desktop-small="4" data-items-mobile="3" data-items-tablet="4">
                    @foreach($body_types as $body_type)
                        <li class="item"><a href="{{ url('gallery').'?bodytype='.$body_type->id }}"><img src="{{ url($body_type->image) }}" alt="{{ $body_type->name }}"><span>{{ $body_type->name }}</span></a></li>
                    @endforeach
                </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@push('footer_scripts')
<script src="{{ url('assets/js/owl.carousel.min.js') }}"></script> <!-- Owl Carousel -->
<script type="text/javascript">
    $( document ).ready(function() {
        setTimeout(function(){
                $('.body-types').show()
        }, 1000);
    });
</script>
@endpush