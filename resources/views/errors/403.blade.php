@extends('layouts.app')

@section('content')
    <div class="main" role="main">
        <div id="content" class="content full">
            <div class="container">
                <div class="text-align-center error-404">
                    <h1 class="huge">403</h1>
                    <hr class="sm">
                    <p><strong>Sorry - Page Forbidden!</strong></p>
                    <p>You don't have authorization to get this page <br> If you have authorization, please contact the System Administrator</p>
                </div>
                <div class="spacer-30"></div>
            </div>
        </div>
    </div>
@endsection