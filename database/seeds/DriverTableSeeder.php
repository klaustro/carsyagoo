<?php

use Illuminate\Database\Seeder;


class DriverTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Driver::class)->create(['name' => '4x2/2-wheel drive']);
        factory(App\Driver::class)->create(['name' => '4x4/4-wheel drive']);
        factory(App\Driver::class)->create(['name' => 'AWD']);
        factory(App\Driver::class)->create(['name' => 'FWD']);
        factory(App\Driver::class)->create(['name' => 'RWD']);
        factory(App\Driver::class)->create(['name' => 'Unknown']);
    }
}
