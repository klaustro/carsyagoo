<?php

namespace App\Http\Controllers;

use App\BodyType;
use App\CarModel;
use App\Color;
use App\Make;
use App\Post;
use App\Transmission;
use Illuminate\Http\Request;
use App\AdManager;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->isAdmin) {
            return redirect('/playfair');
        } else if(auth()->user()->isDealer){
            return redirect('/dealer');
        }

        return $this->front();
    }


    public function front()
    {
        $models = CarModel::paginate(10);
        $transmissions = Transmission::all();
        $body_types = BodyType::all();
        $makes = Make::where('status', 1)->orderBy('name')->get();
        $colors = Color::all();
        $posts = Post::orderBy('id', 'DESC')->paginate(5);

        $footer = loadFooter();

        return view('layouts.index', compact('makes', 'posts', 'body_types', 'makes', 'models', 'transmissions', 'colors', 'footer'));
    }

    public function filter(){
        $request = request()->all();
        if (request()->make != null) {
            $filter = [
                'make_id'=> request()->make,
            ];
        }else if (request()->bodytype != null) {
            $filter = [
                'bodytype_id'=> request()->bodytype,
            ];
        }else if(count(request()->all()) >0 ){
            $filter = [
                'zipcode'=> request()['zipcode'],
                'make_id'=> request()['make_id'],
                'model_id'=> request()['model_id'],
                'min_price'=> request()['min_price'],
                'max_price'=> request()['max_price'],
                'min_year'=> request()['min_year'],
                'max_year'=> request()['max_year'],
                'specific_model'=> request()['specific_model'],
            ];
        }

        $footer = loadFooter();

        return view('gallery.index', compact('filter', 'footer'));

    }

    public function anouncement()
    {
        $footer = loadFooter();

        return view('gallery.index', compact('footer'));
    }

    public function blog()
    {
        $footer = loadFooter();

        $slug = str_replace('blog/post/', '', request()->path());

        $post = Post::where('slug', $slug)->first();

        $page_title = optional($post)->title;

        $meta_keywords = optional($post)->metakeywords;

        $meta_desc = optional($post)->metadesc;

        return view('blog.index', compact('footer', 'page_title', 'meta_keywords', 'meta_desc'));

    }

    public function contact()
    {
        $footer = loadFooter();

        return view('contact.index', compact('footer'));

    }
}
