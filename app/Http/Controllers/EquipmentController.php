<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Equipment;

class EquipmentController extends Controller
{


    public function index(){
        return Equipment::search(request()->search)
                    ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
                    ->paginate(5);
    }

    public function EquipmentList()
    {
        return Equipment::all();
    }

	public function store()
    {
        $v = \Validator::make(request()->draft, [
            'name' => 'required',
            'code' => 'required',
            'status' => 'required',
            'category_id' => 'required|integer|exists:categories,id',
        ]);

        $errors = $v->errors();
        $message=[];

        foreach ($errors->all() as  $mess) {
            $message[]=$mess.'  ';
        }

        if ($v->fails())
        {
            return ['message' => $message , 'status' => 0];
        }

        if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

            $Base64Img = request()->images[0]['Base64Img'];
            list(, $Base64Img) = explode(';', $Base64Img);
            list(, $Base64Img) = explode(',', $Base64Img);
            $Base64Img = base64_decode($Base64Img);
            $file = file_put_contents(request()->images[0]['url'], $Base64Img);
            $folder = '/images/equipments/';
            rename(public_path().'/'.request()->images[0]['url'],public_path() . $folder . request()->images[0]['url']);

            $equipment = Equipment::create([
                'name' => request()->draft['name'],
                'code' => request()->draft['code'],
                'category_id' => request()->draft['category_id'],
                'status' => request()->draft['status'],
                'iconimage' => $folder . request()->images[0]['url'],
            ]);


        }else{

            $equipment = Equipment::create([
                'name' => request()->draft['name'],
                'code' => request()->draft['code'],
                'category_id' => request()->draft['category_id'],
                'status' => request()->draft['status'],
            ]);
        }
        return [
            'message' => trans('app.equipment.store_message'),
            'id' => $equipment->id, 'data' => $equipment
        ];
    }

	public function update($id)
    {
        $v = \Validator::make(request()->draft, [
            'name' => 'required',
            'code' => 'required',
            'status' => 'required',
            'category_id' => 'required|integer|exists:categories,id',
        ]);

        $errors = $v->errors();
        $message=[];

        foreach ($errors->all() as  $mess) {
            $message[]=$mess.'  ';
        }

        if ($v->fails())
        {
            return ['message' => $message , 'status' => 0];
        }

        if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

            $Base64Img = request()->images[0]['Base64Img'];
            list(, $Base64Img) = explode(';', $Base64Img);
            list(, $Base64Img) = explode(',', $Base64Img);
            $Base64Img = base64_decode($Base64Img);
            $file = file_put_contents(request()->images[0]['url'], $Base64Img);             $folder = '/images/equipments/';
            rename(public_path().'/'.request()->images[0]['url'],public_path() . $folder . request()->images[0]['url']);

            $equipment = Equipment::where('id',$id)->update([
                'name' => request()->draft['name'],
                'code' => request()->draft['code'],
                'category_id' => request()->draft['category_id'],
                'status' => request()->draft['status'],
                'iconimage' => $folder . request()->images[0]['url'],
            ]);


        }else{
            $equipment = Equipment::where('id',$id)->update([
                'name' => request()->draft['name'],
                'category_id' => request()->draft['category_id'],
                'status' => request()->draft['status'],
            ]);

        }

        return ['message' => trans('app.equipment.update_message')];

    }

    public function delete($id)
    {
        Equipment::destroy($id);

        return ['message' => trans('app.equipment.delete_message')];
    }

}
