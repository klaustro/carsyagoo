<?php

use Faker\Generator as Faker;

$factory->define(App\BodyType::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
        'category_id' => function(){
            return firstOrFactory(\App\Category::class);
        },
        'status' => $faker->boolean,
        'metakey' => $faker->randomNumber(),
        'metadesc' => $faker->randomNumber(),
    ];
});
