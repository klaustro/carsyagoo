let state = {
    data: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    loading: false,
}

let actions = {
    getOffers(context, params){
        console.log(params)
        context.state.loading = true
        axios.get('/api/offer?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getOffers', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    storeOffer(context, payload){
        context.state.loading = true
        return new Promise((resolve, reject) => {
            axios.post('/api/offer', payload)
                .then(response => {
                    context.commit('storeOffer', response)
                    resolve()
                })
                .catch(error => {
                    Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                    context.state.loading = false
                    reject()
                })
        })
    },
}

let mutations = {
    getOffers(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.data = data.data;
    },

    storeOffer(state, response){
        Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
        state.loading = false
    },
}

export default {
    state,
    actions,
    mutations
}