@extends('layouts.app')

@section('content')
<!-- Start Page header -->
<div class="page-header parallax">
    <div id="contact-map" style="width:100%;height:300px"></div>
</div>
<!-- Utiity Bar -->
<div class="utility-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-8">
                <ol class="breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li class="active">Contact Us</li>
                </ol>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-4">
            </div>
        </div>
    </div>
</div>
<!-- Start Body Content -->
<div class="main" role="main">
    <div id="content" class="content full">
        <div class="container">
            <div class="listing-header margin-40">
                <h2>Contact Us</h2>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <i class="fa fa-home"></i></span> <b>AutoStars Inc.</b><br>
                        2500 CityWest Blvd.<br>
                        Suite 300 Houston, 77042 USA<br><br>
                        <i class="fa fa-phone"></i> <b>1800-989-990</b><br>
                        <i class="fa fa-fax"></i> <b>1800-989-991</b><br>
                        <i class="fa fa-envelope"></i> <a href="mailto:example@info.com">info@autostars.com</a><br><br>
                        <i class="fa fa-home"></i> <b>Mon - Fri 9.00 - 18.00</b><br>
                        Saturday - Sunday CLOSED
                </div>
                <div class="col-md-9 col-sm-8">
                    <contact></contact>
                    <div class="clearfix"></div>
                    <div id="message"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection