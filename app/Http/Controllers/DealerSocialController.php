<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DealerSocial;
use Auth;

class DealerSocialController extends Controller
{
    public function index()
	{
        return DealerSocial::search(request()->search)
            ->byDealer(request()->dealer_id)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);

	}


	public function store()
    {
        if (request()->images) {
            $image = request()->images[0];
            $Base64Img = $image['dataURL'];
            $file_name = $image['upload']['filename'];
            list(, $Base64Img) = explode(';', $Base64Img);
            list(, $Base64Img) = explode(',', $Base64Img);
            $Base64Img = base64_decode($Base64Img);
            $file = file_put_contents($file_name, $Base64Img);

            $folder = '/images/dealer_socials/';
            rename(public_path().'/'. $file_name , public_path() . $folder . $file_name);
        }

        $dealerSocial = DealerSocial::create([
            'socialname' => request()->draft['socialname'],
            'link' => request()->draft['link'],
            'dealer_id' => request()->draft['dealer_id'],
            'status' => request()->draft['status'],
            'icon' => isset(request()->images) ? $folder . $file_name : null,
        ]);

        return [
            'message' => trans('app.dealerSocial.store_message'),
            'id' => $dealerSocial->id,
        ];
    }

	public function update($id)
    {
        $dealerSocial = DealerSocial::find($id);
        $dealerSocial->fill(request()->draft);
        if (isset(request()->images) && isset(request()->images[0]['dataURL'])) {
            \Log::info('entró');
            $image = request()->images[0];
            $Base64Img = $image['dataURL'];
            $file_name = $image['upload']['filename'];
            list(, $Base64Img) = explode(';', $Base64Img);
            list(, $Base64Img) = explode(',', $Base64Img);
            $Base64Img = base64_decode($Base64Img);
            $file = file_put_contents($file_name, $Base64Img);

            $folder = '/images/dealer_socials/';
            rename(public_path().'/'. $file_name , public_path() . $folder . $file_name);
            $dealerSocial->icon = $folder . $file_name;
        }

        $dealerSocial->save();

        return ['message' => trans('app.dealerSocial.update_message')];

    }

    public function delete($id)
    {
        $dealerSocial = DealerSocial::destroy($id);

        return ['message' => trans('app.dealerSocial.delete_message')];
    }
}
