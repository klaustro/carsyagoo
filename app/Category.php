<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 'title', 'alias','metadata','metakey','metadesc','status'
    ];


    public function catEquipment()
    {
        return $this->hasmany('App\CatEquipment');
    }

    public function condition ()
    {
        return $this->BelongsTo('App\Condition');
    }

    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->
                where('name', 'like', "%$target%")
                ->orWhere('title', 'like', "%$target%")
                ->orWhere('alias', 'like', "%$target%");
        }
    }
}
