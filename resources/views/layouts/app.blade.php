<!DOCTYPE HTML>
<html lang="en"  class="no-js">
<head>
    <!-- Basic Page Needs ================================================== -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>{{ isset($page_title) ? $page_title : getConfig('page_title') }}</title>
    <meta name="">
    <meta name="description" content="{{ isset($meta_desc) ? $meta_desc : 'Unbiased car reviews and over a million opinions and photos from real people. Use CarGurus to find the best used car deals.'}}">
    <meta name="keywords" content="Used Cars, New Cars, Reviews{{isset($meta_keywords) ? ', ' . $meta_keywords : '' }}">
    <meta name="author" content="Nissi Technology Enterprises INC">
    <!-- Mobile Specific Metas ================================================== -->
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <!-- <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0"> -->
    <meta name="format-detection" content="telephone=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    @include('layouts.scripts.head')

</head>
<body class="home">
    <div class="body">
        @include('layouts.header')
        @include('layouts.navbar')
    </div>
            @stack('slider')
        <div id="app">
            <div class="main" role="main">
                @yield('content')
            </div>
            @include('layouts.footer')
        </div>
        <a id="back-to-top"><i class="fa fa-angle-double-up"></i></a>
    @include('layouts.scripts.footer')

</body>
</html>