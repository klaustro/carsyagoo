<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;
class DriverController extends Controller
{
     public function index()
	{
        return Driver::search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);
	}

    public function driverList()
    {
        return Driver::all();
    }

	public function store()
    {

        try {

            $v = \Validator::make(request()->draft, [
                'name' => 'required',
                'status' => 'required',
                'category_id' => 'required|integer|exists:categories,id',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }

            if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);

                $folder = '/images/drivers/';
                rename(public_path().'/'.request()->images[0]['url'],public_path(). $folder .request()->images[0]['url']);

                $driver = Driver::create([
                    'name' => request()->draft['name'],
                    'status' => request()->draft['status'],
                    'category_id' => request()->draft['category_id'],
                    'image' => $folder . request()->images[0]['url'],
                ]);

                return [
                    'message' => trans('app.driver.store_message'),
                    'id' => $driver->id
                ];

            }else{

                $driver = Driver::create([
                    'name' => request()->draft['name'],
                    'status' => request()->draft['status'],
                    'category_id' => request()->draft['category_id'],

                ]);

                return [
                    'message' => trans('app.driver.store_message_error'),
                    'id' => $driver->id
                ];
            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.store_error')], 500);
        }
    }

	public function update($id)
    {
        try {

            $v = \Validator::make(request()->draft, [
                'name' => 'required',
                'status' => 'required',
                'category_id' => 'required|integer|exists:categories,id',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }

            if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);
                $folder = '/images/drivers/';
                rename(public_path().'/'.request()->images[0]['url'],public_path(). $folder .request()->images[0]['url']);

                $driver = Driver::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'status' => request()->draft['status'],
                    'category_id' => request()->draft['category_id'],
                    'image' => $folder . request()->images[0]['url'],
                ]);

                return ['message' => trans('app.driver.update_message')];

            }else{
                $driver = Driver::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'status' => request()->draft['status'],
                    'category_id' => request()->draft['category_id'],
                ]);

                return ['message' => trans('app.driver.update_message_error')];
            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.update_error')], 500);
        }
    }

    public function delete($id)
    {
        $driver = Driver::destroy($id);

        return ['message' => trans('app.driver.delete_message')];
    }
}
