<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('phone')->nullable();
            $table->datetime('brithdday')->nullable();
            $table->longtext('bio')->nullable();
            $table->unsignedInteger('user_group_id')->default(3);
            $table->foreign('user_group_id')->references('id')->on('user_groups');
            $table->string('login')->unique()->nullable();
            $table->string('password');
            $table->string('email')->unique();
            $table->ipAddress('registration_ip')->nullable();
            $table->boolean('recieve_email')->default(true);
            $table->boolean('blocked_user')->default(false);
            $table->boolean('require_password_reset')->default(false);
            $table->timestamps();
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
