<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Offer extends Model
{
    use Notifiable;

    protected $fillable = ['ads_manager_id', 'name', 'email', 'phone', 'amount', ];

    public function ads_manager()
    {
        return $this->belongsTo(AdManager::class)->withoutGlobalScope(AdManagerScope::class);
    }

    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->
                where('name', 'like', "%$target%")
                ->orWhere('email', 'like', "%$target%")
                ->orWhere('phone', 'like', "%$target%")
                ->orWhere('amount', 'like', "%$target%");
        }
    }
}
