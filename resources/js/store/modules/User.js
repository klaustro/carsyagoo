let state = {
    users: [],
    userGroups: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    cities: [],
    states: [],
    loading:false,
    authUser: null,
}

let getters = {
        findUser(state){
            return function(id){
                let user = state.users.find(color => user.id == id)
                return user;
            }
        },
}

let actions = {
    getUsers(context, params){
        context.state.loading = true
        axios.get('/admin/user?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getUsers', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    storeUser(context, payload){
        context.state.loading = true
        axios.post('/admin/user', payload)
            .then(response => {
                let newUser = {
                    id: response.data.id,
                    first_name: response.data.first_name,
                    last_name: response.data.last_name,
                    address_1: response.data.address_1,
                    address_2: response.data.address_2,
                    city_id: response.data.city_id,
                    zipcode: response.data.zipcode,
                    phone: response.data.phone,
                    city_id: response.data.city_id,
                    brithdday: response.data.brithdday,
                    bio: response.data.bio,
                    user_group_id: response.data.user_group_id,
                    login: response.data.login,
                    email: response.data.email,
                    registration_ip: response.data.registration_ip,
                    image: payload.image,
                    blocked_user_id: payload.blocked_user_id,
                }
                Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                context.commit('storeUser', newUser)
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateUser(context, payload){
        context.state.loading = true
        axios.put('/admin/user/' + payload.id, payload.draft)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateUser', payload)
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeUser(context, id){
        context.state.loading = true
        axios.delete('/admin/user/' + id)
            .then(response => {
                context.commit('removeUser', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })

    },

    listUser(context){
        axios.get('/admin/userList')
            .then(response => {
                context.commit('listUsers', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    listUserGroups(context){
        axios.get('/admin/userGroupList')
            .then(response => {
                context.commit('listUserGroups', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    listCities(context, state_id){
                console.log(state_id)
        axios.get('/admin/cityList?state_id=' + state_id)
            .then(response => {
                context.commit('listCities', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    listStates(context){
        axios.get('/admin/stateList')
            .then(response => {
                context.commit('listStates', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    setAuthUser(context, user){
        context.commit('setAuthUser', user)
    },
}

let mutations = {
    getUsers(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.users = data.data;
    },

    storeUser(state, newUser){
        state.users.unshift(newUser);
    },

    updateUser(state, {id, draft}){
        let index = state.users.findIndex(user => user.id == id);
        state.users.splice(index, 1, draft);
    },

    removeUser(state, id)    {
        let index = state.users.findIndex(user => user.id == id);
        state.users[index].blocked_user = 1;
    },

    listUsers(state, data){
        state.list = data.data;
    },
    listCities(state, data){
        state.cities = data.data;
    },
    listStates(state, data){
        state.states = data.data;
    },
    listUserGroups(state, data){
        state.userGroups = data.data;
    },
    setAuthUser(state, user){
        state.authUser = user
    }

}

export default {
    state,
    getters,
    actions,
    mutations
}