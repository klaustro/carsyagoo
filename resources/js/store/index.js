import Vue from 'vue'
import Vuex from 'vuex'
import User from './modules/User.js'
import Color from './modules/Colors.js'
import Category from './modules/Categories.js'
import Condition from './modules/Conditions.js'
import CatEquipment from './modules/CatEquipments.js'
import BodyType from './modules/BodyTypes.js'
import Make from './modules/Makes.js'
import Driver from './modules/Drivers.js'
import Model from './modules/Models.js'
import Dealer from './modules/Dealers.js'
import DealerSocial from './modules/DealerSocials.js'
import Equipment from './modules/Equipments.js'
import Transmission from './modules/Transmissions.js'
import SettingGroup from './modules/SettingGroup.js'
import Setting from './modules/Setting.js'
import Post from './modules/Post.js'
import Comment from './modules/Comments.js'
import Vote from './modules/Vote.js'
import BillingTerm from './modules/BillingTerms.js'
import BillingService from './modules/BillingServices.js'
import BillingPayment from './modules/BillingPayments.js'
import Invoice from './modules/Invoices.js'
import BlogCategory from './modules/BlogCategories.js'
import SocialMedia from './modules/SocialMedia.js'
import AdManager from './modules/AdManagers.js'
import Fuel from './modules/Fuels.js'
import Tag from './modules/Tag.js'
import Payment from './modules/Payment.js'
import Contact from './modules/Contact.js'
import WishList from './modules/WishList.js'
import Offer from './modules/Offer.js'
import Review from './modules/Review.js'
import Address from './modules/Address.js'

//Local Data Persistant
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex)


let store = new Vuex.Store({
	modules:{
		User,
		Color,
		Category,
		Condition,
		CatEquipment,
		BodyType,
		Make,
		Model,
		Driver,
		Equipment,
		Transmission,
		SettingGroup,
		Setting,
		Dealer,
		DealerSocial,
		Post,
		Comment,
		Vote,
		BillingTerm,
		BillingService,
		BillingPayment,
		Invoice,
		BlogCategory,
		SocialMedia,
		AdManager,
		Fuel,
		Tag,
		Payment,
		Contact,
		WishList,
		Offer,
		Review,
		Address,
	},
	plugins: [createPersistedState({
		paths: [
			'AdManager.address',
		]
	})],
})


export default store