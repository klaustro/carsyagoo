let state = {
    adManagers: [],
    anouncement: [],
    relatedVehicles:[],
    solo: null,
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading:false,
    address: null,
}

let getters = {
        finAdManager(state){
            return function(id){
                let adManager = state.adManagers.find(adManager => adManager.id == id)
                return adManager;
            }
        },
}

let actions = {
    getAdManagers(context, params){
        context.state.loading = true
        axios.get('/api/ad-manager?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getAdManagers', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    getAdManager(context, id){
        context.state.loading = true
        return new Promise((resolve, reject) => {
        axios.get('/admin/ad-manager-edit/' + id)
            .then(response => {
                context.commit('getAdManager', {data: response.data})
                context.state.loading = false
                resolve(response)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
                reject(error)
            })
        })
    },

    getAnouncement(context, params){
        context.state.loading = true
        return new Promise((resolve, reject) => {
            axios.get('/api/anouncement/' + params.id)
                .then(response => {
                    context.commit('getAnouncement', {data: response.data})
                    context.state.loading = false
                    resolve(response)
                })
                .catch(error => {
                    Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                    reject(error)
                })
        })
    },
    getAnouncements(context, params){
        console.log(params.draft)
        context.state.loading = true
        let model_id = params.draft.model_id ? params.draft.model_id : ''
        let make_id = params.draft.make_id ? params.draft.make_id : ''
        let min_price = params.draft.min_price ? params.draft.min_price : ''
        let max_price = params.draft.max_price ? params.draft.max_price : ''
        let min_year = params.draft.min_year ? params.draft.min_year : ''
        let max_year = params.draft.max_year ? params.draft.max_year : ''
        let zipcode = params.draft.zipcode ? params.draft.zipcode : ''
        let specific_model = params.draft.specific_model ? params.draft.specific_model : ''
        let bodytype_id = params.draft.bodytype_id ? params.draft.bodytype_id : ''
        let fuel_id = params.draft.fuel_id ? params.draft.fuel_id : ''
        let state = params.state ? params.state : ''
        let address = context.state.address ? context.state.address : ''

        axios.get('/api/anouncements?page=' + params.page +
                '&search=' + params.target +
                '&orderBy=' + params.orderBy +
                '&desc=' + params.desc +
                '&rows=' + params.rows +
                '&model_id=' + model_id +
                '&make_id=' + make_id +
                '&min_price=' + min_price +
                '&max_price=' + max_price +
                '&min_year=' + min_year +
                '&max_year=' + max_year +
                '&zipcode=' + zipcode +
                '&specific_model=' + specific_model +
                '&bodytype_id=' + bodytype_id +
                '&fuel_id=' + fuel_id +
                '&state=' + state +
                '&latitude=' + address.latitude +
                '&longitude=' + address.longitude
            )
            .then(response => {
                context.commit('getAnouncements',
                    {
                        data: response.data,
                        model_id: params.model_id,
                        bodyType_id: params.bodyType_id,
                        transmission_id: params.transmission_id,
                        fuel_id: params.fuel_id,})
                        context.state.loading = false
                })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    storeAdManager(context, payload){
        context.state.loading = true
        return new Promise((resolve, reject) => {
        axios.post('/api/ad-manager', payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    if (payload.draft.image != '') {
                          payload.draft.image= '/images/ad_managers/main/'+payload.draft.image
                    }
                    let newAdManager = {
                        id: response.data.id,
                        category_id:payload.draft.category_id,
                        model_id:payload.draft.model_id,
                        condition_id:payload.draft.condition_id,
                        city_id:payload.draft.city_id,
                        dealer_id:payload.draft.dealer_id,
                        bodytype_id:payload.draft.bodytype_id,
                        drive_id:payload.draft.drive_id,
                        fuel_id:payload.draft.fuel_id,
                        transmission_id:payload.draft.transmission_id,
                        ext_color_id:payload.draft.ext_color_id,
                        int_color_id:payload.draft.int_color_id,
                        specific_color:payload.draft.specific_color,
                        address:payload.draft.address,
                        zip_code:payload.draft.zip_code,
                        latitude:payload.draft.latitude,
                        longitude:payload.draft.longitude,
                        equipment_detail:payload.draft.equipment_detail,
                        year:payload.draft.year,
                        vincode:payload.draft.vincode,
                        mileage:payload.draft.mileage,
                        price:payload.draft.price,
                        bargain_price:payload.draft.bargain_price,
                        doors:payload.draft.doors,
                        seats:payload.draft.seats,
                        engine:payload.draft.engine,
                        expire:payload.draft.expire,
                        embedcode:payload.draft.embedcode,
                        is_commercial:payload.draft.is_commercial,
                        is_stop:payload.draft.is_stop,
                        is_reserved:payload.draft.is_reserved,
                        is_special:payload.draft.is_special,
                        hits:payload.draft.hits,
                        status:payload.draft.status,
                        otherinfo:payload.draft.otherinfo,
                        unfaden_weight:payload.draft.unfaden_weight,
                        gross_weigth:payload.draft.gross_weigth,
                        length:payload.draft.length,
                        width:response.data.data.width,
                        disclaimer:payload.draft.disclaimer,
                        is_metalic_color:payload.draft.is_metalic_color,
                        specific_model:payload.draft.specific_model,
                        specific_trans:payload.draft.specific_trans,
                        export_price:payload.draft.export_price,
                        fuel_consum_city:payload.draft.fuel_consum_city,
                        fuel_consum_freeway:payload.draft.fuel_consum_freeway,
                        fuel_consum_combined:payload.draft.fuel_consum_combined,
                        acceleration:payload.draft.acceleration,
                        max_speed:payload.draft.max_speed,
                        C02:payload.draft.C02,
                        image_count:payload.draft.image_count,
                        video_link:payload.draft.video_link,
                        main_image:response.data.data.main_image,
                    }
                    Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                    context.commit('storeAdManager', newAdManager)
                }
                context.state.loading = false
                resolve(response)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
                reject(error)
            })
        })
    },

    updateAdManager(context, payload){
        context.state.loading = true
        return new Promise((resolve, reject) => {
            axios.put('/admin/ad-manager-update/' + payload.id, payload)
                .then(response => {
                    if (response.data.status == 0) {
                        for (var i = response.data.message.length - 1; i >= 0; i--) {
                            Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                        }
                    } else {
                        if (payload.draft.image != '') {
                            payload.draft.image= '/images/ad_managers/main/' + payload.draft.image
                        }
                        Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                        context.commit('updateAdManager', payload)
                    }
                    context.state.loading = false
                    resolve(response)
                })
                .catch(error => {
                    Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                    context.state.loading = false
                    reject(error)
                })
        })
    },

    removeAdManager(context, id){
        context.state.loading = true
        axios.delete('/api/ad-manager/' + id)
            .then(response => {
                context.commit('removeAdManager', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })

    },

    listAdManagers(context){

        axios.get('/api/ad-manager-list')
            .then(response => {
                context.commit('listAdManagers', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
    setAddress(context, payload){
        context.commit('setAddress', payload)
    },
    removeAddress(context){
        context.commit('removeAddress')
    },
}

let mutations = {
    getAdManagers(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.adManagers = data.data;
    },

    getAnouncements(state, {data, model_id, bodytype_id, transmission_id, fuel_id}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = parseInt(data.per_page)
        state.currentModelId = model_id
        state.currentBodytypeId = bodytype_id
        state.currentTransmissionId = transmission_id
        state.currentFuelId = fuel_id
        state.adManagers = data.data;
    },

    getAnouncement(state, data){
        state.anouncement = data.data.anouncement;
        state.relatedVehicles = data.data.relatedVehicles;
    },

    getAdManager(state, {data}){
        state.solo = data;
    },

    storeAdManager(state, newAdManager){
        state.adManagers.unshift(newAdManager);
    },

    updateAdManager(state, {id, draft}){
        let index = state.adManagers.findIndex(adManager => adManager.id == id);
        state.adManagers.splice(index, 1, draft);
    },

    removeAdManager(state, id)    {
        let index = state.adManagers.findIndex(adManager => adManager.id == id);
        state.adManagers.splice(index, 1);
    },

    listAdManagers(state, data){
        state.list = data.data;
    },
    setAddress(state, payload){
        state.address = payload
    },
    removeAddress(state){
        state.address = null
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}