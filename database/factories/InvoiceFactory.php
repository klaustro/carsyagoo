<?php

use Faker\Generator as Faker;

$factory->define(App\Invoice::class, function (Faker $faker) {
    return [
        'note' => $faker->sentence,
        'total' => $faker->numberBetween($min = 1000, $max = 9000) ,
        'tax' => $faker->numberBetween($min = 1000, $max = 9000),
        'subtotal' => $faker->numberBetween($min = 1000, $max = 9000),
        'date' => $faker->dateTime,
        'token' => uniqid(),
        'dealer_id' => function(){
          return firstOrFactory(App\Dealer::class);
        },
        'billing_service_id' => function(){
          return firstOrFactory(App\BillingService::class);
        },
    ];


});
