@extends('layouts.app')

@section('content')
    <blog :user="{{ json_encode(auth()->user()) }}"></blog>
@endsection