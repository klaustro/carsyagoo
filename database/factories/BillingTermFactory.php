<?php

use Faker\Generator as Faker;

$factory->define(App\BillingTerm::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'days' => rand(7, 365),
    ];
});
