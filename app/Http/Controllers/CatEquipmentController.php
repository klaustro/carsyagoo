<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CatEquipment;

class CatEquipmentController extends Controller
{

    public function index(){
        return CatEquipment::search(request()->search)
                    ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
                    ->paginate(5);
    }

    public function catEquipmentList()
    {
        return CatEquipment::all();
    }

	public function store()
    {
        $v = \Validator::make(request()->draft, [
            'name' => 'required',
            'status' => 'required',
            'category_id' => 'required|integer|exists:categories,id',
        ]);

        $errors = $v->errors();
        $message=[];

        foreach ($errors->all() as  $mess) {
            $message[]=$mess.'  ';
        }

        if ($v->fails())
        {
            return ['message' => $message , 'status' => 0];
        }

        if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

            $Base64Img = request()->images[0]['Base64Img'];
            list(, $Base64Img) = explode(';', $Base64Img);
            list(, $Base64Img) = explode(',', $Base64Img);
            $Base64Img = base64_decode($Base64Img);
            $file = file_put_contents(request()->images[0]['url'], $Base64Img);

            $folder = '/images/cat_equipments/';
            rename(public_path().'/'.request()->images[0]['url'],public_path(). $folder .request()->images[0]['url']);

            $catEquipment = CatEquipment::create([
                'name' => request()->draft['name'],
                'category_id' => request()->draft['category_id'],
                'status' => request()->draft['status'],
                'image' => $folder . request()->images[0]['url'],
            ]);

        }else{

            $catEquipment = CatEquipment::create([
                'name' => request()->draft['name'],
                'category_id' => request()->draft['category_id'],
                'status' => request()->draft['status'],
            ]);
        }
        return [
            'message' => trans('app.catEquipment.store_message'),
            'catEquipment' => $catEquipment
        ];
    }

	public function update($id)
    {
        try {

            $v = \Validator::make(request()->draft, [
                'name' => 'required',
                'status' => 'required',
                'category_id' => 'required|integer|exists:categories,id',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }

            if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);
                $folder = '/images/cat_equipments/';
                rename(public_path().'/'.request()->images[0]['url'],public_path(). $folder .request()->images[0]['url']);

                $catEquipment = CatEquipment::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                    'image' => $folder . request()->images[0]['url'],
                ]);

                return ['message' => trans('app.catEquipment.update_message')];

            }else{
                $catEquipment = CatEquipment::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                ]);

                return ['message' => trans('app.catEquipment.update_message_error')];
            }
        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.error')], 500);
        }

    }

    public function delete($id)
    {
        $catEquipment = CatEquipment::destroy($id);

        return ['message' => trans('app.catEquipment.delete_message')];
    }

}
