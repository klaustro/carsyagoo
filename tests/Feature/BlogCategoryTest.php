<?php

namespace Tests\Feature;

use App\BlogCategory;
use App\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BlogCategoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function if_post_category_is_listed()
    {
        $blog_categories = factory(BlogCategory::class, 5)->create();

        $this->get('api/blogCategory')
             ->assertSuccessful()
             ->assertSee($blog_categories[4]->name);

    }

    /** @test */
    function if_post_of_category_is_counted()
    {
        $blog_categories = factory(BlogCategory::class, 5)->create();

        $posts = factory(Post::class, 5)->create([
            'blog_category_id' => $blog_categories[0]->id,
        ]);

        $this->get('api/blogCategory')
             ->assertSuccessful()
             ->assertSee($blog_categories[4]->name)
             ->assertJsonFragment(['post_count' => 5]);

    }
}
