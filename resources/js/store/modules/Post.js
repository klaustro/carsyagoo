let state = {
    data: [],
    post: null,
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    loading: false,
    currentCategoryId: null,
    recents: null,
}

let getters = {
        findPost(state){
            return function(id){
                let post = state.data.find(post => post.id == id)
                return post;
            }
        },
}

let actions = {
    getPosts(context, params){
        context.state.loading = true
        let blog_category_id = params.blog_category_id ? params.blog_category_id : ''
        let tag_id = params.tag_id ? params.tag_id : ''
        axios.get('/api/post?page=' + params.page +
                '&search=' + params.target +
                '&orderBy=' + params.orderBy +
                '&desc=' + params.desc +
                '&rows=' + params.rows +
                '&blog_category_id=' + blog_category_id +
                '&tag_id=' + tag_id
            )
            .then(response => {
                context.commit('getPosts', {data: response.data, blog_category_id: params.blog_category_id})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
    getRecentPosts(context){
        context.state.loading = true
        axios.get('/api/post?page=' + 1 +
                '&orderBy=id'+
                '&desc=' + true +
                '&rows=2'
            )
            .then(response => {
                context.commit('getRecentPosts', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
    getPost(context, params){
        context.state.loading = true
        axios.get('/api/post/' + params.slug
            )
            .then(response => {
                context.commit('getPost', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
    storePost(context, payload){
        context.state.loading = true
        axios.post('/admin/post', payload)
            .then(response => {
                let newPost = {
                    id: response.data.id,
                    blog_category_id: payload.blog_category_id,
                    title: payload.title,
                    image: response.data.image,
                    intro: payload.intro,
                    content: payload.content,
                    image_alt: payload.image_alt,
                    metadesc: payload.metadesc,
                    metakeywords: payload.metakeywords,
                    publish_date: payload.publish_date,
                }
                context.commit('storePost', newPost)
                Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updatePost(context, payload){
        context.state.loading = true
        axios.put('/admin/post/' + payload.id, payload.draft)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updatePost', payload)
                context.state.loading = false

            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removePost(context, id){
        context.state.loading = true
        axios.delete('/admin/post/' + id)
            .then(response => {
                context.commit('removePost', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                context.state.loading = false
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })

    },
}

let mutations = {
    getPosts(state, {data, blog_category_id}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = parseInt(data.per_page)
        state.currentCategoryId = blog_category_id
        state.data = data.data;
    },
    getRecentPosts(state, {data}){
        state.recents = data.data;
    },
    getPost(state, data){
        state.post = data.data;
    },
    storePost(state, newPost){
        state.data.unshift(newPost);
    },

    updatePost(state, {id, draft}){
        let index = state.data.findIndex(post => post.id == id);
        state.data.splice(index, 1, draft);
    },

    removePost(state, id)    {
        let index = state.data.findIndex(post => post.id == id);
        state.data.splice(index, 1);
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}