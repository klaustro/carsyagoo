@extends('layouts.app')

@section('content')
        <div class="page-header parallax" :style="'background-image:url(' + trans('settings.blog.image') + ');'">
            <div class="container">
                <h1 class="page-title">Subscription</h1>
            </div>
        </div>
        <div class="main" role="main">
            <br>
            <div class="container container-fixed">
                <div class="row">
                    @if($status == 'approved')
                        <div class="col-md-6 col-md-offset-3">
                            <div class="icon-box ibox-center">
                                <div class="ibox-icon">
                                    <i class="fa fa-check" style="background-color: green"></i>
                                </div>
                                <h3>{{ trans('app.payment.success_title') }}</h3>
                                <p>{{ trans('app.payment.success_message', ['transaction_id' => $transaction_id]) }}</p>
                                <br>
                                <a href="/dealer" class="btn btn-primary btn-lg orange">Go to Dealer page</a>
                            </div>
                        </div>
                    @elseif($status == 'rejected')
                        <div class="col-md-6 col-md-offset-3">
                            <div class="icon-box ibox-center">
                                <div class="ibox-icon">
                                    <i class="fa fa-close" style="background-color: red"></i>
                                </div>
                                <h3>{{ trans('app.payment.fail_title') }}</h3>
                                <p>{{ trans('app.payment.fail_message') }}</p>
                                <br>
                                <a href="/dealer" class="btn btn-primary btn-lg orange">Go to Dealer page</a>
                            </div>
                        </div>
                    @endif


                </div>
            </div>
        </div>
@endsection

<style>
    .container-fixed{
        height: 40%;
    }
</style>