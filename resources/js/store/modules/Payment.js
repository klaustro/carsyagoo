let state = {
    result: '',
    loading: false
}

let actions = {
    storePayment(context, payload){
        context.state.loading = true
        return new Promise((resolve, reject) => {
            axios.post('/payments/subscribe', payload)
                .then( response => {
                    context.commit('storePayment', response.data)
                    resolve(response)
                })
                .catch( error => {
                    Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error', position: 'bottom-center'})
                    context.state.loading = false
                    reject(error)
                })
        })
    }
}

let mutations = {
    storePayment(state, payload){
        state.result = payload
        let statusOk = state.result.state == 'Approved'
        let icon = statusOk ? 'check' : 'exclamation-triangle'
        let type = statusOk ? 'success' : 'error'
        let message = state.result.state + ': ' + state.result.transaction_id
        Vue.toasted.show(message, {icon: icon, type: type, position: 'top-center', theme: 'bubble'})
        state.loading = false
    }
}

export default {
    state,
    actions,
    mutations
}