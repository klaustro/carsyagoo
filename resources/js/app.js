import router from './router'
import store from './store'

require('./bootstrap');

window.Vue = require('vue');

/**
 *  Vendor Components
 */
require('./vendor_components')

/**
 * Custom Vue Components
 */
require('./vue_components')

//filters
Vue.filter('blog_intro', function(value){
    return value.substr(0, 240)
})

Vue.filter('money', function(value){
    var numeral = require('numeral')
    return numeral(value).format('0,0')
})

Vue.filter('review', function(value){
    var numeral = require('numeral')
    return numeral(value).format('0.0')
})

//Localization
Vue.prototype.trans = string => _.get(window.i18n, string);
window.trans = string => _.get(window.i18n, string);

window.clone = function (obj) {
    return JSON.parse(JSON.stringify(obj));
}

const app = new Vue({
    el: '#app',
    router,
    store,
});