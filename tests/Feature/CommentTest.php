<?php

namespace Tests\Feature;

use App\Post;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function if_comment_can_be_stored()
    {
        $user = factory(User::class)->create();
        $post = factory(Post::class)->create();

        $this->actingAs($user);
        $response = $this->post('api/comment', [
            'post_id' =>$post->id,
            'reply' => null,
            'content' => 'Earum qui dolore dolor unde dicta. Nulla qui natus est a officia quia.',
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.comment.store_message')]);

        $this->assertDatabaseHas('comments',[
            'user_id' => $user->id,
            'post_id' => $post->id,
            'reply' => null,
            'content' => 'Earum qui dolore dolor unde dicta. Nulla qui natus est a officia quia.',
        ]);

    }
}
