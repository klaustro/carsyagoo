let state = {
    invoices: [],
    dealerInvoices: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading: false,
}

let getters = {
        findInvoice(state){
            return function(id){
                let invoice = state.invoices.find(invoice => invoice.id == id)
                return invoice;
            }
        },
}

let actions = {

    getInvoices(context, params){
        context.state.loading = true
        axios.get('/admin/invoice?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getInvoices', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    storeInvoice(context, payload){
        context.state.loading = true
        axios.post('/admin/invoice', payload)
            .then(response => {

                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    let newInvoice = {
                        id: response.data.id,
                        dealer_id: response.data.data.dealer_id,
                        billing_service_id: response.data.data.billing_service_id,
                        date: response.data.data.date,
                        subtotal: response.data.data.subtotal,
                        tax: response.data.data.tax,
                        total: response.data.data.total,
                        note: response.data.data.note,
                    }
                    context.commit('storeInvoice', newInvoice)
                    Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                }

                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateInvoice(context, payload){
        context.state.loading = true
        axios.put('/admin/invoice/' + payload.id, payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                    context.commit('updateInvoice', payload)
                }
                context.state.loading = false

            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeInvoice(context, id){
        context.state.loading = true
        axios.delete('/admin/invoice/' + id)
            .then(response => {

                context.commit('removeInvoice', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                context.state.loading = false
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })

    },

    listInvoices(context){
        axios.get('/admin/invoiceList')
            .then(response => {
                context.commit('listInvoices', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    getDealerInvoices(context, params){
        context.state.loading = true
        axios.get('/api/dealer-invoice/' + params.dealer_id + '?page=' + params.page)
            .then(response => {
                context.commit('getDealerInvoices', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getInvoices(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.invoices = data.data;
    },

    getDealerInvoices(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.dealerInvoices = data.data;
    },

    storeInvoice(state, newInvoice){
        state.invoices.unshift(newInvoice);
    },

    updateInvoice(state, {id, draft}){
        let index = state.invoices.findIndex(invoice => invoice.id == id);
        state.invoices.splice(index, 1, draft);
    },

    removeInvoice(state, id)    {
        let index = state.invoices.findIndex(invoice => invoice.id == id);
        state.invoices.splice(index, 1);
    },

    listInvoices(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}