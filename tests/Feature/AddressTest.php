<?php

namespace Tests\Feature;

use App\Address;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AddressTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->actingAs($this->user);
    }

    /** @test */
    function if_address_can_be_showed()
    {
        $address = factory(Address::class)->create([
            'user_id' => $this->user->id,
        ]);

        $this->get('/api/address')
            ->assertSuccessful();
    }

    /** @test */
    function if_address_can_be_stored()
    {
        $response = $this->post('/api/address', [
            'administrative_area_level_1' => 'FL',
            'country' => 'United States',
            'latitude' => '25.780108',
            'locality' => 'Miami',
            'longitude' => '-80.26444099999998',
            'postal_code' => '33126',
            'route' => 'Northwest 42nd Avenue',
            'street_number' => '850',
            'secondary' => 'Apt 2',
            'phone' => '+1555555555',
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.address.store_message')]);

        $this->assertDatabaseHas('addresses',[
            'user_id' => $this->user->id,
            'country' => 'United States',
            'state' => 'FL',
            'locality' => 'Miami',
            'street_number' => '850',
            'route' => 'Northwest 42nd Avenue',
            'secondary' => 'Apt 2',
            'postal_code' => '33126',
            'phone' => '+1555555555',
            'latitude' => '25.780108',
            'longitude' => '-80.26444099999998',
            'secondary' => 'Apt 2',
            'phone' => '+1555555555',
        ]);
    }

    /** @test */
    function if_address_can_be_updated()
    {
        $address = factory(Address::class)->create([
            'user_id' => $this->user->id,
        ]);

        $response = $this->put('/api/address/' . $address->id, [
            'administrative_area_level_1' => 'FL',
            'country' => 'United States',
            'latitude' => '25.780108',
            'locality' => 'Miami',
            'longitude' => '-80.26444099999998',
            'postal_code' => '33126',
            'route' => 'Northwest 42nd Avenue',
            'street_number' => '850',
            'secondary' => 'Apt 2',
            'phone' => '+1555555555',
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.address.update_message')]);

        $this->assertDatabaseHas('addresses',[
            'user_id' => $this->user->id,
            'country' => 'United States',
            'state' => 'FL',
            'locality' => 'Miami',
            'street_number' => '850',
            'route' => 'Northwest 42nd Avenue',
            'secondary' => 'Apt 2',
            'postal_code' => '33126',
            'phone' => '+1555555555',
            'latitude' => '25.780108',
            'longitude' => '-80.26444099999998',
        ]);
    }
}
