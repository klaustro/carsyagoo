<?php

use Faker\Generator as Faker;

$factory->define(\App\Color::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->colorName,
        'image' => $faker->imageUrl,
        'csscode' => $faker->hexcolor,
        'category_id' => function(){
            return firstOrFactory(\App\Category::class);
        },
        'status' => $faker->boolean,
    ];
});
