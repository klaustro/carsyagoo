<?php

use Faker\Generator as Faker;

$factory->define(App\CarModel::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
        'image' => $faker -> imageUrl(640, 480),
        'alias' =>  uniqid(),
        'description' => $faker->word,
        'make_id' => function(){
            return firstOrFactory(\App\Make::class);
        },
        'metakey' => $faker->randomNumber(),
        'metadesc' => $faker->randomNumber(),
        'status' =>  $faker -> randomElement($array = array (true, false)),
    ];
});
