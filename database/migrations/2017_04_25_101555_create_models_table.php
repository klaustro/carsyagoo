<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('models', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('make_id');
            $table->foreign('make_id')->references('id')->on('makes')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->string('name');
            $table->string('alias')->unique();
            $table->longText('description');
            $table->string('image')->nullable();
            $table->string('metakey');
            $table->string('metadesc');
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('models');
    }
}
