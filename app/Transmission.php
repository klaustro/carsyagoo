<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transmission extends Model
{
    protected $fillable = [
        'name','iconimage','code', 'category_id','status'
    ];

    public function category()
    {
        return $this->BelongsTo('App\Category');
    }

    public function anouncements()
    {
        return $this->hasMany(AdManager::class, 'transmission_id');
    }

    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            $query->where('name', 'like', "%$target%")
            ->orWhere('code', 'like',"%$target%")
            ->orWhereHas('category', function ($query) use ($target) {
                    $query->Where('name', 'like',"%$target%");
                });
        }

        return $query;
    }
}
