<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdManagerDetail extends Model
{
    protected $table = 'ads_managers_detail';
    protected $fillable = ['state', 'urlphoto','ads_manager_id',];

    public function adManager()
    {
        return $this->BelongsTo('App\AdManager','ads_manager_id');
    }
}
