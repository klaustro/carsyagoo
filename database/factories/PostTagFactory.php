<?php

use App\Post;
use App\Tag;
use Faker\Generator as Faker;

$factory->define(App\PostTag::class, function (Faker $faker) {
    return [
        'tag_id' => function(){
            return firstOrFactory(Tag::class);
        },
        'post_id' => function(){
            return firstOrFactory(Post::class);
        },
    ];
});
