<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dealer_id');
            $table->foreign('dealer_id')->references('id')->on('dealers');
            $table->unsignedInteger('billing_service_id');
            $table->foreign('billing_service_id')->references('id')->on('billing_services');
            $table->string('type');
            $table->string('result_id')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('invoice')->nullable();
            $table->string('state');
            $table->string('currency')->nullable();
            $table->string('tax')->nullable();
            $table->string('price')->nullable();
            $table->longText('response');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
