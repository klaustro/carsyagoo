<?php

use App\Fuel;
use Illuminate\Database\Seeder;

class FuelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Fuel::class)->create(['name' => 'Gasoline']);
        factory(Fuel::class)->create(['name' => 'Diesel']);
        factory(Fuel::class)->create(['name' => 'E85 Flex Fuel']);
        factory(Fuel::class)->create(['name' => 'Other']);
    }
}
