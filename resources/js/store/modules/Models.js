let state = {
    models: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading:false
}

let getters = {
        findModel(state){
            return function(id){
                let model = state.models.find(model => model.id == id)
                return model;
            }
        },
}

let actions = {
    getModels(context, params){
        context.state.loading = true
        axios.get('/admin/model?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getModels', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    storeModel(context, payload){
        context.state.loading = true
        axios.post('/admin/model', payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }
                }else{
                    if (payload.draft.image != '') {
                          payload.draft.image= '/images/models/'+payload.draft.image
                    }
                    let newModel = {
                        id: response.data.data.id,
                        name: payload.draft.name,
                        metakey: payload.draft.metakey,
                        metadesc: payload.draft.metadesc,
                        image: payload.draft.image,   
                        status: payload.draft.status,
                        make_id: payload.draft.make_id,
                        description: payload.draft.description,
                        alias: payload.draft.alias
                    }
                    Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                    context.commit('storeModel', newModel)
                }
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateModel(context, payload){
        context.state.loading = true
        axios.put('/admin/model/' + payload.id, payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }
                }else{
                    Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                    if (payload.draft.image != '') {
                        payload.draft.image= '/images/models/'+payload.draft.image
                    }
                    context.commit('updateModel', payload)
                }
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeModel(context, id){
        context.state.loading = true
        axios.delete('/admin/model/' + id)
            .then(response => {
                context.commit('removeModel', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    listModels(context, make_id){
        axios.get('/admin/model-list?make_id=' + make_id)
            .then(response => {
                context.commit('listModels', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getModels(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.models = data.data;
    },

    storeModel(state, newModel){
        state.models.unshift(newModel);
    },

    updateModel(state, {id, draft}){
        let index = state.models.findIndex(model => model.id == id);
        state.models.splice(index, 1, draft);
    },

    removeModel(state, id)    {
        let index = state.models.findIndex(model => model.id == id);
        state.models.splice(index, 1);
    },

    listModels(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}