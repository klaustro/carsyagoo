<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BodyType extends Model
{
    protected $fillable = [
        'name','image', 'category_id','metakey','metadesc','status'
    ];

    public function category()
    {
        return $this->BelongsTo('App\Category');
    }

    public function anouncements()
    {
        return $this->hasMany(AdManager::class, 'bodytype_id');
    }

    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->where('name', 'like', "%$target%")
            			 ->orWhere('metakey', 'like', "%$target%")
            			 ->orWhere('metadesc', 'like', "%$target%")
                         ->orWhereHas('category', function ($query) use ($target) {
                        $query->Where('name', 'like',"%$target%");
                    });
        }
    }
}
