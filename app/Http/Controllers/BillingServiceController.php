<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\BillingService;

class BillingServiceController extends Controller
{

    public function index()
	{
        $billing_services = BillingService::search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(request()->rows);

        if (auth()->user()->is_dealer) {
            $billing_services->map(function($service){
                $service->own = count(auth()->user()->dealer->services) > 0
                    ? $service->id == auth()->user()->dealer->services[0]->id
                    : false;
            });
        }

        return $billing_services;

	}


	public function store()
    {
        try {

            $v = \Validator::make(request()->draft, [
                'billing_term_id' => 'required',
                'name' => 'required',
                'code' => 'required',
                'description' => 'required',
                'price' => 'required|numeric',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }

            if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);

                rename(public_path().'/'.request()->images[0]['url'],public_path().'/images/billing_services/'.request()->images[0]['url']);

                $folder = '/images/billing_services/';
                $billingService = BillingService::create([
                    'billing_term_id' => request()->draft['billing_term_id'],
                    'name' => request()->draft['name'],
                    'code' => request()->draft['code'],
                    'price' => request()->draft['price'],
                    'description' => request()->draft['description'],
                    'icon' => $folder . request()->images[0]['url'],
                ]);

                return [
                    'message' => trans('app.billingService.store_message'),
                    'id' => $billingService->id, 'data'=> $billingService, 'status' => 1
                ];

            }else{

                $billingService = BillingService::create([
                    'billing_term_id' => request()->draft['billing_term_id'],
                    'name' => request()->draft['name'],
                    'code' => request()->draft['code'],
                    'price' => request()->draft['price'],
                    'description' => request()->draft['description'],
                ]);

                return [
                    'message' => trans('app.billingService.store_message_error'),
                    'id' => $billingService->id, 'data'=> $billingService, 'status' => 1
                ];
            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.store_error')], 500);
        }
    }

	public function update($id)
    {
        $v = \Validator::make(request()->draft, [
            'billing_term_id' => 'required',
            'name' => 'required',
            'code' => 'required',
            'description' => 'required',
            'price' => 'required',
        ]);

        $errors = $v->errors();
        $message=[];

        foreach ($errors->all() as  $mess) {
            $message[]=$mess.'  ';
        }

        if ($v->fails())
        {
            return ['message' => $message , 'status' => 0];
        }

        $billingService = BillingService::find($id);
        $billingService->fill(request()->draft);

        if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {
            $Base64Img = request()->images[0]['Base64Img'];
            list(, $Base64Img) = explode(';', $Base64Img);
            list(, $Base64Img) = explode(',', $Base64Img);
            $Base64Img = base64_decode($Base64Img);
            $file = file_put_contents(request()->images[0]['url'], $Base64Img);
            $folder = '/images/billing_services/';
            rename(public_path() . '/' . request()->images[0]['url'], public_path(). $folder .request()->images[0]['url']);
            $billingService->icon = $folder . request()->images[0]['url'];
        }
        $billingService->save();

        return [
            'message' => trans('app.billingService.update_message'),
            'data' => $billingService,
        ];
    }

    public function delete($id)
    {
        $billingService = BillingService::destroy($id);

        return ['message' => trans('app.billingService.delete_message')];
    }

    public function list()
    {
        return BillingService::all();
    }

    public function show($id)
    {
        return BillingService::find($id);
    }
}
