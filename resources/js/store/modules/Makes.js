let state = {
    makes: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading:false
}

let getters = {
        findMake(state){
            return function(id){
                let make = state.makes.find(make => make.id == id)
                return make;
            }
        },
}

let actions = {
    getMakes(context, params){
        context.state.loading = true
        axios.get('/admin/make?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getMakes', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    storeMake(context, payload){
        context.state.loading = true
        axios.post('/admin/make', payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    if (payload.draft.image != '') {
                          payload.draft.image= '/images/makes/'+payload.draft.image
                    }
                    let newMake = {
                        id: response.data.make.id,
                        name: payload.draft.name,
                        metakey: payload.draft.metakey,
                        metadesc: payload.draft.metadesc,
                        image: payload.draft.image,
                        category_id: payload.draft.category_id,
                        description: payload.draft.description,
                        alias: payload.draft.alias
                    }
                    console.log(response)
                    Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                    context.commit('storeMake', newMake)
                }
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateMake(context, payload){
        context.state.loading = true
        axios.put('/admin/make/' + payload.id, payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }
                }else{
                    Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                    if (payload.draft.image != '') {
                          payload.draft.image= '/images/makes/'+payload.draft.image
                    }
                    context.commit('updateMake', payload)
                }
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeMake(context, id){
        context.state.loading = true
        axios.delete('/admin/make/' + id)
            .then(response => {
                context.commit('removeMake', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    listMakes(context, category_id){
        axios.get('/admin/makeList?category_id=' + category_id)
            .then(response => {
                context.commit('listMakes', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getMakes(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.makes = data.data;
    },

    storeMake(state, newMake){
        state.makes.unshift(newMake);
    },

    updateMake(state, {id, draft}){
        let index = state.makes.findIndex(make => make.id == id);
        state.makes.splice(index, 1, draft);
    },

    removeMake(state, id)    {
        let index = state.makes.findIndex(make => make.id == id);
        state.makes.splice(index, 1);
    },

    listMakes(state, data){
        if (state.list.length == 0) {
            state.list = data.data;
        }
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}