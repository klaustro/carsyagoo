let state= {
    data:null,
    loading: false,
}

let actions= {
    getAddress(context){
        axios.get('/api/address')
            .then( response => {
                context.commit('getAddress', {data: response.data})
                context.state.loading = false
            })
            .catch( error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },
    storeAddress(context, payload){
        axios.post('/api/address', payload)
            .then( response => {
                let newAddress = {
                    user_id: response.data.user_id,
                    country: payload.country,
                    state: payload.state,
                    locality: payload.locality,
                    street_number: payload.street_number,
                    route: payload.route,
                    secondary: payload.secondary,
                    postal_code: payload.postal_code,
                    phone: payload.phone,
                    latitude: payload.latitude,
                    longitude: payload.longitude,
                }
                context.commit('storeAddress', newAddress)
                context.state.loading = false
                Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
            })
            .catch( error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },
    updateAddress(context, payload){
        context.state.loading = true
        axios.put('/api/address/' + payload.id, payload.address)
            .then( response => {
                context.commit('updateAddress', payload.address)
                context.state.loading = false
                Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
            })
            .catch( error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },
}

let mutations ={
    getAddress(state, payload){
        state.data = payload
    },
    storeAddress(state, newAddress){
        state.data = newAddress
    },
    updateAddress(state, payload){
        state.data = payload;
    },
}

export default {
    state,
    actions,
    mutations
}