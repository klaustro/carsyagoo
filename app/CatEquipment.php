<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;

class CatEquipment extends Model
{
    protected $fillable = [
        'name','image', 'category_id','status'
    ];

    public function category()
    {
        return $this->BelongsTo('App\Category');
    }

    

    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            $query->where('name', 'like', "%$target%")
            ->orWhereHas('category', function ($query) use ($target) {
                Log::debug('wherehas'.$target);
                            $query->Where('name', 'like',"%$target%");
                });
        }

        return $query;
    }
}
