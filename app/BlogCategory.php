<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $fillable = [
        'name','imageurl', 'metadesc','metakeywords','description'
    ];

    public function posts()
    {
        return $this->hasMany(Post::class, 'blog_category_id');
    }

    public function scopeSearch($query, $target)
    {

        if ($target != '') {
            return $query->where('name', 'like', "%$target%")
            			 ->orWhere('description', 'like', "%$target%")
            			 ->orWhere('metadesc', 'like', "%$target%")
            			 ->orWhere('metakeywords', 'like', "%$target%");
        }
    }
}
