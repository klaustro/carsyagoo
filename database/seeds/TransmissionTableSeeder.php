<?php

use Illuminate\Database\Seeder;


class TransmissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Transmission::class)->create(['name' => 'Atomatic']);
        factory(App\Transmission::class)->create(['name' => 'Manual']);
        factory(App\Transmission::class)->create(['name' => 'Other']);
    }
}
