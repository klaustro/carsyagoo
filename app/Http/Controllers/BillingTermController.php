<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\BillingTerm;

class BillingTermController extends Controller
{

    public function index()
    {
        return BillingTerm::search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(request()->rows);

    }

    public function billingTermList()
    {
        return BillingTerm::all();
    }

	public function store()
    {
        try {

            $v = \Validator::make(request()->draft, [
                'name' => 'required',
                'days' => 'required',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }

            $billingTerm = BillingTerm::create(request()->draft);


            if ( $billingTerm->save()) {
                return [
                    'message' => trans('app.billingTerm.store_message'),
                    'id' => $billingTerm->id, 'data'=>$billingTerm, 'status' => 1,
                ];

            }else{
                return [
                    'message' => trans('app.billingTerm.store_message_error'), 'status' => 1,

                ];

            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.store_error')], 500);
        }
    }

	public function update($id)
    {
        try {

            $v = \Validator::make(request()->draft, [
                'name' => 'required',
                'days' => 'required',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }
            $billingTerm = BillingTerm::where('id',$id)
            ->update(request()->draft);

            if ( $billingTerm) {
                return [
                    'message' => trans('app.billingTerm.update_message'), 'status' => 1,
                ];
            }else{
                return [
                    'message' => trans('app.billingTerm.update_message_error'), 'status' => 1
                ];
            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.error')], 500);
        }

    }

    public function delete($id)
    {
        $billingTerm = BillingTerm::destroy($id);

        return ['message' => trans('app.billingTerm.delete_message')];
    }
}
