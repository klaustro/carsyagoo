<?php

namespace App\Http\Controllers;

use App\Color;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    public function index()
    {
        return Color::search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);
    }

    public function colorList()
    {
        return Color::orderBy('name')->get();
    }

    public function store()
    {
        $v = \Validator::make(request()->draft, [
            'name' => 'required',
            'csscode' => 'required',
            'category_id' => 'required|integer|exists:categories,id',
        ]);

        $errors = $v->errors();
        $message=[];

        foreach ($errors->all() as  $mess) {
            $message[]=$mess.'  ';
        }

        if ($v->fails())
        {
            return ['message' => $message , 'status' => 0];
        }

        if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);

                rename(public_path().'/'.request()->images[0]['url'],public_path().'/images/colors/'.request()->images[0]['url']);
                $folder = '/images/colors/';
                $color = Color::create([
                    'name' => request()->draft['name'],
                    'csscode' => request()->draft['csscode'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                    'image' => $folder . request()->images[0]['url'],
                ]);

                return [
                    'message' => trans('app.color.store_message'),
                    'id' => $color->id, 'data'=> $color
                ];

            }else{

                $color = Color::create([
                    'name' => request()->draft['name'],
                    'csscode' => request()->draft['csscode'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                ]);

                return [
                    'message' => trans('app.color.store_message'), 'id'=> $color->id, 'data'=> $color

                ];
            }

    }

    public function update($id)
    {
        $v = \Validator::make(request()->draft, [
            'name' => 'required',
            'csscode' => 'required',
            'category_id' => 'required|integer|exists:categories,id',
        ]);

        $errors = $v->errors();
        $message=[];

        foreach ($errors->all() as  $mess) {
            $message[]=$mess.'  ';
        }

        if ($v->fails())
        {
            return ['message' => $message , 'status' => 0];
        }

        if (count(request()->images)>0 && request()->images[0]['Base64Img'] != null) {

                $Base64Img = request()->images[0]['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents(request()->images[0]['url'], $Base64Img);
                rename(public_path().'/'.request()->images[0]['url'],public_path().'/images/colors/'.request()->images[0]['url']);
                $folder = '/images/colors/';
                $color = Color::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'category_id' => request()->draft['category_id'],
                    'status' => request()->draft['status'],
                    'csscode' => request()->draft['csscode'],
                    'image' => $folder . request()->images[0]['url'],
                ]);

                return ['message' => trans('app.color.update_message')];

            }else{
                $color = Color::where('id',$id)->update([
                    'name' => request()->draft['name'],
                    'category_id' => request()->draft['category_id'],
                    'csscode' => request()->draft['csscode'],
                    'status' => request()->draft['status'],
                ]);

                return ['message' => trans('app.color.update_message')];
            }

    }

    public function delete($id)
    {
        $color = Color::destroy($id);

        return ['message' => trans('app.color.delete_message')];
    }

    public function list()
    {
        return Color::all();
    }

}
