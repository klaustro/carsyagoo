<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['user_id', 'blog_category_id', 'title',
    'image', 'content', 'slug', 'image_alt', 'metadesc', 'metakeywords', 'publish_date', ];

    /**
    * Relationships
    */

   public function category()
   {
        return $this->belongsTo(BlogCategory::class, 'blog_category_id');
   }

   public function user()
   {
        return $this->belongsTo(User::class);
   }

   public function tags()
   {
        return $this->belongsToMany(Tag::class);
   }

   public function comments()
   {
        return $this->hasMany(Comment::class);
   }

   public function votes()
   {
        return $this->hasMany(Vote::class);
   }


    /**
     * Method to search by any column
     * @param  Query $query
     * @param  string $target [description]
     * @return Query
     */
    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->
                where('id', $target)
                ->orWhere('user_id', 'like', "%$target%")
                ->orWhere('blog_category_id', 'like', "%$target%")
                ->orWhere('title', 'like', "%$target%")
                ->orWhere('intro', 'like', "%$target%")
                ->orWhere('image', 'like', "%$target%")
                ->orWhere('content', 'like', "%$target%");
        }
    }
    /**
     * Method to filter by categories
     * @param  Query $query
     * @param  string $target [description]
     * @return Query
     */
    public function scopeCategory($query, $blog_category_id)
    {
        if ($blog_category_id != '') {
            return $query->where('blog_category_id', $blog_category_id);
        }
    }
    /**
     * Method to filter by categories
     * @param  Query $query
     * @param  string $target [description]
     * @return Query
     */
    public function scopeTag($query, $tag_id)
    {
        if ($tag_id != '') {
            $posts = PostTag::where('tag_id', $tag_id)->get()->pluck('post_id');
            return $query->whereIn('id', $posts);
        }
    }
}
