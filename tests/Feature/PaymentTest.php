<?php

namespace Tests\Feature;

use App\BillingPayment;
use App\BillingService;
use App\BillingTerm;
use App\Dealer;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PaymentTest extends TestCase
{
    use RefreshDatabase;

    protected $user, $dealer;

    function setUp()
    {
        /*

        // Stop here and mark this test as incomplete.
        $this->markTestIncomplete(
          'This test is avoid for speed tests.'
        );
         */

        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->dealer = factory(Dealer::class)->create([
            'user_id' => $this->user->id
        ]);

        $this->actingAs($this->user);
    }
    /**
     * @test
     */
    public function if_payment_is_procesed_with_credit_card()
    {
        $billing_term = factory(BillingTerm::class)->create([
            'days' => 30,
        ]);

        $billing_service = factory(BillingService::class)->create([
            'billing_term_id' => $billing_term->id,
        ]);

        $card = [
            'type' => 'visa',
            'cardnumber' => '4758411877817150',
            'expire_month' => '05',
            'expire_year' => '2019',
            'cvc' => '456',
            'first_name' => 'Juan',
            'last_name' => 'Palencia',
        ];


        $this->post('payments/subscribe', [
            'method' => 'credit_card',
            'card' => $card,
            'service_id' => $billing_service->id
        ])
            ->assertSuccessful()
            ->assertJsonFragment([
                'name' => $billing_service->name,
                'price' => number_format($billing_service->price, 2),
                'currency' => 'USD',
                'quantity' => '1',
                'description' => 'Carsyagoo membership: ' . $this->user->login,
            ])
            ->assertJsonFragment(['state' => 'approved']);

        $this->assertDatabaseHas('payments', [
            'dealer_id' => $this->dealer->id,
            'currency' => 'USD',
            'tax' => 0,
            'price' => $billing_service->price,
        ]);

        $this->assertDatabaseHas('billing_payments', [
            'dealer_id' => $this->dealer->id,
            'expire_month' => '05',
            'expire_year' => '2019',
            'first_name' => 'Juan',
            'last_name' => 'Palencia',
            'cvc' => '456',
        ]);

        $due_date = Carbon::now()->addDays($billing_term->days)->toDateString();

        $this->assertDatabaseHas('billing_service_dealer', [
            'dealer_id' => $this->dealer->id,
            'billing_service_id' => $billing_service->id,
            'expires' => $due_date,
        ]);

        $this->assertDatabaseHas('invoices', [
            'dealer_id' => $this->dealer->id,
            'billing_service_id' => $billing_service->id,
            'total' => $billing_service->price,
        ]);
    }

    /**
     * @test
     */
    public function if_payment_is_procesed_with_paypal()
    {
        $billing_service = factory(BillingService::class)->create();

        $this->post('payments/subscribe', [
            'method' => 'paypal',
            'service_id' => $billing_service->id
        ])
            ->assertSuccessful()
            ->assertJsonFragment([
                'name' => $billing_service->name,
                'price' => number_format($billing_service->price, 2),
                'currency' => 'USD',
                'quantity' => 1,
                'description' => 'Carsyagoo membership: ' . $this->user->login,
            ])
            ->assertJsonFragment(['state' => 'created']);

        $this->assertDatabaseHas('payments', [
            'dealer_id' => $this->dealer->id,
            'currency' => 'USD',
            'tax' => 0,
            'price' => $billing_service->price
        ]);

        $this->assertDatabaseHas('invoices', [
            'dealer_id' => $this->dealer->id,
            'billing_service_id' => $billing_service->id,
            'total' => $billing_service->price,
        ]);

    }

    /**
     * @test
     */
    public function if_billing_payment_is_procesed_with_credit_card_storaded()
    {
        $dealers = factory(Dealer::class, 2)->create([
            'user_id' => function(){
                return factory(User::class)->create()->id;
            }
        ]);

        $service = factory(BillingService::class)->create([
            'price' => 99,
        ]);

        $due_date = Carbon::now()->subDay()->toDateString();



        $dealers->map(function($dealer) use($service, $due_date){

            $dealer->services()->attach($service->id, ['expires' => $due_date]);

            $wrong_dealer = rand(0, 1);

            if ($wrong_dealer == 0) {
                factory(BillingPayment::class)->create([
                    'dealer_id' => $dealer->id,
                    'type' => 'visa',
                    'cardnumber' => '4012888888881881',
                    'expire_month' => '02',
                    'expire_year' => '2019',
                    'first_name' => $dealer->user->first_name,
                    'last_name' => $dealer->user->last_name,
                    'cvc' => '123',
                    'primary' => 1,
                ]);
                $dealer->wrong = false;
            } else {
                $fail_card = factory(BillingPayment::class)->create([
                    'dealer_id' => $dealer->id,
                    'primary' => 1,
                ]);
                $dealer->wrong = true;
            }

        });


        $this->get('payments/billing')
            ->assertSuccessful();


        $dealers->map(function($dealer) use($due_date, $service){
            if (! $dealer->wrong) {

                $billing_service = BillingService::find($service->billing_service_id);
                $new_date = $dealer->service->billing_term->renew_date($due_date);

                $this->assertDatabaseHas('payments', [
                    'dealer_id' => $dealer->id,
                    'currency' => 'USD',
                    'tax' => '0',
                    'price' => 99,
                ]);

                $this->assertDatabaseHas('billing_service_dealer', [
                    'dealer_id' => $dealer->id,
                    'expires' => $new_date,
                ]);

                $this->assertDatabaseHas('invoices', [
                    'dealer_id' => $dealer->id,
                    'billing_service_id' => $service->id,
                ]);
            } else {
                $this->assertDatabaseHas('payments', [
                    'dealer_id' => $dealer->id,
                    'state' => 'error',
                ]);

                $this->assertDatabaseHas('billing_service_dealer', [
                    'dealer_id' => $dealer->id,
                    'expires' => $due_date,
                ]);

            }

        });

    }
}
