<?php

use Illuminate\Database\Seeder;
use App\Invoice;

class InvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Invoice::class,10)->create([
            'dealer_id' => 1,
        ]);

        factory(Invoice::class,10)->create();
    }
}
