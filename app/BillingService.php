<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BillingService extends Model
{
    protected $fillable = [
       'billing_term_id', 'code', 'icon', 'name', 'price', 'description'
    ];

    protected $appends = ['frequency'];

    //Relationships
    public function billing_term()
    {
        return $this->belongsTo(BillingTerm::class);
    }

    //Attributes
    public function getFrequencyAttribute()
    {
        return $this->billing_term->name;
    }

    //scopes
    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->where('name', 'like', "%$target%")
            			 ->orWhere('code', 'like', "%$target%")
            			 ->orWhere('price', 'like', "%$target%")
            			 ->orWhere('description', 'like', "%$target%");
        }
    }
}
