<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads_managers', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->unsignedInteger('make_id');
            $table->foreign('make_id')->references('id')->on('makes')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->unsignedInteger('model_id');
            $table->foreign('model_id')->references('id')->on('models')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->unsignedInteger('condition_id');
            $table->foreign('condition_id')->references('id')->on('conditions')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->unsignedInteger('dealer_id');
            $table->foreign('dealer_id')->references('id')->on('dealers')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->unsignedInteger('bodytype_id');
            $table->foreign('bodytype_id')->references('id')->on('body_types')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->unsignedInteger('driver_id');
            $table->foreign('driver_id')->references('id')->on('drivers')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->unsignedInteger('fuel_id');
            $table->foreign('fuel_id')->references('id')->on('fuels')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->unsignedInteger('transmission_id');
            $table->foreign('transmission_id')->references('id')->on('transmissions')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->unsignedInteger('ext_color_id');
            $table->foreign('ext_color_id')->references('id')->on('colors')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->unsignedInteger('int_color_id');
            $table->foreign('int_color_id')->references('id')->on('colors')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->unsignedInteger('specific_color');
            $table->foreign('specific_color')->references('id')->on('colors')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->mediumText('equipment_detail')->nullable();
            $table->integer('year');
            $table->string('vincode');
            $table->integer('mileage');
            $table->double('price');
            $table->double('bargain_price')->nullable();
            $table->integer('doors');
            $table->integer('seats');
            $table->string('engine');
            $table->integer('expire')->nullable();
            $table->mediumText('embedcode')->nullable();
            $table->boolean('is_commercial')->default(false);
            $table->boolean('is_featured')->default(false);
            $table->boolean('is_top')->default(false);
            $table->boolean('is_reserved')->default(false);
            $table->boolean('is_special')->default(false);
            $table->integer('hits')->nullable();
            $table->integer('status');
            $table->mediumText('otherinfo')->nullable();
            $table->double('unfaden_weight')->nullable();
            $table->double('gross_weight')->nullable();
            $table->double('length')->nullable();
            $table->double('width')->nullable();
            $table->longtext('disclaimer')->nullable();
            $table->boolean('is_metalic_color')->default(false);
            $table->string('specific_model')->nullable();
            $table->string('specific_trans')->nullable();
            $table->integer('export_price')->nullable();
            $table->double('fuel_consum_city')->nullable();
            $table->double('fuel_consum_freeway')->nullable();
            $table->double('fuel_consum_combined')->nullable();
            $table->double('acceleration')->nullable();
            $table->double('max_speed')->nullable();
            $table->double('C02')->nullable();
            $table->string('video_link')->nullable();
            $table->string('main_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads_managers');
    }
}
