export default {
	state:{
		attributeValues:[],
		attributeInProducts:[],
		combo_values:[],
		perPage: null,
	    currentPage: 1,
	    lastPage: null,
	    totalRows: null,
	},
	actions:{
		comboValue(context, payload){
			axios.post('admin/playfair/comboValue',payload)
			.then(response => {
				context.commit('comboValue' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		getAttributeValuesInProduct( context ,payload){
			axios.post('admin/playfair/getAttributeValuesInProduct',payload)
			.then(response => {
				context.commit('getAttributeValues' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		getAttributeValues( context ,params){
			axios.get('admin/playfair/getAttributeValues?page='+params.page + '&search=' + params.target)
			.then(response => {
				context.commit('getAttributeValues' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createAttributeValue(context,payload){
			axios.post('/admin/playfair/createAttributeValue',payload)
			.then(response => {
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
				context.commit('createAttributeValue', response.data)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removeProduct_id(context, payload){
	        axios.post('/admin/playfair/removeProduct_id/' + payload.id_product, payload)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'error'})
                context.commit('removeAttributeValueInProduct', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	},
		removeAttributeValue(context, id){
			axios.delete('/admin/playfair/attributeValue/'+id)
			.then(response => {
                context.commit('removeAttributeValue', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		updateAttributeValue(context, payload){
	        axios.put('/admin/playfair/attributeValue/' + payload.id, payload)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateAttributeValue', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	},
    	createAttributeValueInProduct(context, payload){
	        axios.put('/admin/playfair/updateAttributeValueInProduct/' + payload.id, payload)
            .then(response => {
            	let newAttributeInProduct = {
					id: response.data.data.id,
                    attribute_id: response.data.data.attribute_id,
                    attributes:response.data.data.attributes,
                    product_id:response.data.data.product_id,
                    products:response.data.data.products,
                    status:response.data.data.status,
                    value:response.data.data.value
            	}
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'success'})
                context.commit('createAttributeValueInProduct', newAttributeInProduct)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	}
	},
	mutations:{
		comboValue(state, {list}){
			return state.combo_values = list
		},
		getAttributeValues(state,{ list }){
			state.attributeInProducts = list
			state.attributeValues = list.data
			state.lastPage = list.last_page
	        state.totalRows = list.total
	        state.perPage = list.per_page
		},
		createAttributeValue(state,data){
			// data.data.map(aux => {
   //     			state.attributeValues.unshift(data.result);
			// })
       			state.attributeValues.unshift(data.result);

		},
		removeAttributeValue(state, id){
			let index = state.attributeValues.findIndex(attributeValue => attributeValue.id == id)
			state.attributeValues.splice(index,1)
		},
		updateAttributeValue(state, {id,draft}){
        	let index = state.attributeInProducts.findIndex(attributeValue => attributeValue.id == id);
        	state.attributeInProducts.splice(index, 1, draft);
		},
		createAttributeValueInProduct(state, newAttributeInProduct){
        	state.attributeInProducts.unshift(newAttributeInProduct);
		},

		removeAttributeValueInProduct(state, id){
			let index = state.attributeInProducts.findIndex(attributeValue => attributeValue.id == id.id_product)
			state.attributeInProducts.splice(index,1)
		},
	},
	
}