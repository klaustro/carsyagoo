<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Make extends Model
{
    protected $fillable = ['name', 'image', 'description', 'category_id', 'alias','metakey','metadesc'];

    public function category()
    {
        return $this->BelongsTo('App\Category');
    }

    public function models()
    {
        return $this->hasMany(CarModel::class, 'make_id');
    }

    public function anouncements()
    {
        return $this->hasMany(AdManager::class)->withoutGlobalScope(AdManagerScope::class);
    }

    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            $query->where('name', 'like', "%$target%")
            ->orWhere('alias', 'like', "%$target%")
            ->orWhere('metakey', 'like', "%$target%")
            ->orWhere('metadesc', 'like', "%$target%")
            ->orWhereHas('category', function ($query) use ($target) {
                        $query->Where('name', 'like',"%$target%");
                });
        }

        return $query;
    }

    public function scopeFilterCategory($query, $category_id)
    {
        if ($category_id != '') {
            return $query->where('category_id', $category_id);
        }

    }


}
