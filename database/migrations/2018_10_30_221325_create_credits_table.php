<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credits', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('ads_id');
            $table->foreign('ads_id')->references('id')->on('ads_managers')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('day_phone');
            $table->string('email');
            $table->string('street_number')->nullable();
            $table->string('route')->nullable();
            $table->string('locality')->nullable();
            $table->string('administrative_area_level_1')->nullable();
            $table->string('country')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('evening_phone');
            $table->string('birthday');
            $table->string('residence');
            $table->string('mortgage');
            $table->string('license');
            $table->string('social_security');
            $table->string('employer_name');
            $table->string('employer_street_number')->nullable();
            $table->string('employer_route')->nullable();
            $table->string('employer_locality')->nullable();
            $table->string('employer_administrative_area_level_1')->nullable();
            $table->string('employer_country')->nullable();
            $table->string('employer_postal_code')->nullable();
            $table->string('employer_latitude')->nullable();
            $table->string('employer_longitude')->nullable();
            $table->string('business_phone');
            $table->string('employment_status');
            $table->string('occupation');
            $table->string('job_time');
            $table->string('income');
            $table->string('consent');
            $table->string('desired_monthly_payment');
            $table->string('desired_term_length');
            $table->string('desired_down_payment');
            $table->string('desired_loan_amount');
            $table->string('trade_make')->nullable();
            $table->string('trade_model')->nullable();
            $table->string('trade_year')->nullable();
            $table->string('trade_mileage')->nullable();
            $table->string('trade_amount')->nullable();
            $table->string('trade_comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credits');
    }
}
