<?php

namespace App\Http\Controllers;

use App\Credit;
use App\Notifications\CreditApplication;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class CreditController extends Controller
{
    public function store()
    {
        \Log::info(request()->all());
        $credit = Credit::create(request()->all());
        foreach (request()->address as $key => $value) {
            $credit->$key = $value;
        }
        foreach (request()->employer_address as $key => $value) {
            $field = "employer_$key";
            $credit->$field = $value;
        }
        $credit->save();
        $admins = User::where('user_group_id', User::ADMIN)->get();
        Notification::send($admins, new CreditApplication(request()->all()));
        return ['message' => trans('app.credit.successful')];
    }
}
