<?php

use Illuminate\Database\Seeder;


class ConditionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Condition::class)->create(['name' => 'New']);
        factory(App\Condition::class)->create(['name' => 'Used']);
    }
}
