<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
/*    	$this->call(StateTableSeeder::class);
    	$this->call(CityTableSeeder::class);*/
        $this->call(UserGroupTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(CatEquipmentTableSeeder::class);
        $this->call(ConditionTableSeeder::class);
        $this->call(UserTableSeeder::class);
        //$this->call(AddressSeeder::class);
        $this->call(ColorTableSeeder::class);
        $this->call(BodyTypeTableSeeder::class);
        $this->call(DriverTableSeeder::class);
        $this->call(MakeTableSeeder::class);
        $this->call(EquipmentTableSeeder::class);
        $this->call(TransmissionTableSeeder::class);
        $this->call(CarModelTableSeeder::class);
        $this->call(SettingGroupSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(DealerTableSeeder::class);
        $this->call(BillingServiceTableSeeder::class);
        $this->call(BillingTermTableSeeder::class);
        $this->call(BillingPaymentTableSeeder::class);
        $this->call(FuelTableSeeder::class);

/*
 */
        //Development seeders
        $this->call(DealerSocialTableSeeder::class);
        $this->call(BlogCategoryTableSeeder::class);
        $this->call(PostSeeder::class);
        $this->call(TagSeeder::class);
        $this->call(PostTagSeeder::class);
        $this->call(CommentSeeder::class);
        $this->call(InvoiceTableSeeder::class);
        $this->call(SocialMediaTableSeeder::class);
        $this->call(PaymentSeeder::class);
        $this->call(AdManagerTableSeeder::class);
        $this->call(ContactSeeder::class);
        //$this->call(ReviewSeeder::class);


    }
}
