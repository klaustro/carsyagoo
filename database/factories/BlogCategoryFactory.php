<?php

use Faker\Generator as Faker;

$factory->define(App\BlogCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'imageurl' => $faker -> imageUrl(640, 480),
        'description' => $faker->sentence,
        'metakeywords' => $faker->sentence,
        'metadesc' => $faker->sentence,
    ];
});
