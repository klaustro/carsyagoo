<?php

namespace App\Scopes;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Stevebauman\Location\Facades\Location;

class AdManagerScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        \Log::info(session('region'));
        //request()->session()->forget('region');
        if (!request()->session()->has('region')) {

        /*
            $ip = '204.89.92.153'; //Florida
            $ip = '35.232.163.4'; // California
            $ip = '209.237.150.140';
         */
            $ip = request()->getClientIp();

            if ($ip == '127.0.0.1') {
                $ip = '204.89.92.153';
            }
            try {
                ini_set('default_socket_timeout', 5);
                //$location = unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
                $location = unserialize(file_get_contents('http://pro.ip-api.com/php/' . $ip . '?key=' . env('IP-API_KEY')));

                session(['region' => $location['region']]);

            } catch (\Exception $e) {
                \Log::info($e);
                session(['region' => 'FL']);
            }
        }
        $now = Carbon::now();
        $days = getConfig('days_expires');
        $date_diff = $now->subDays($days ? $days : 30);

        $region = session('region');

            $builder->whereHas('dealer', function($builder) use($region){
                $builder->whereHas('user', function($builder) use($region){
                    $builder->where('blocked_user', 0);
                        $builder->whereHas('address', function($builder) use($region){
                            if (in_array($region, ['CA', 'FL'])) {
                                $builder->where('state', $region);
                            }
                        });
                });
            })->where(function($query) use($date_diff){
                $query->where('created_at', '>=', $date_diff)
                    ->orWhere('expire', null);
            });
    }
}
