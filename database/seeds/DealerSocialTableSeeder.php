<?php

use Illuminate\Database\Seeder;


class DealerSocialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\DealerSocial::class,10)->create();
    }
}
