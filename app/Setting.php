<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['setting_group_id', 'param', 'value', 'key', 'type', 'options',] ;
}
