let state = {
    billingServices: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    solo: null,
    loading: false,
}

let getters = {
        findBillingService(state){
            return function(id){
                let billingService = state.billingServices.find(billingService => billingService.id == id)
                return billingService;
            }
        },
}

let actions = {
    getBillingServices(context, params){
        context.state.loading = true
        axios.get('/admin/billingService?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc + '&rows=' + params.rows)
            .then(response => {
                context.commit('getBillingServices', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    getBillingService(context, params){
        context.state.loading = true
        axios.get('/admin/billingService/' + params)
            .then(response => {
                context.commit('getBillingService', {data: response.data})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    storeBillingService(context, payload){
        context.state.loading = true
        axios.post('/admin/billingService', payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    if (payload.draft.icon != '') {
                          payload.draft.icon= '/images/billing_services/'+payload.draft.image
                    }
                    let newBillingService = {
                        id: response.data.id,
                        name: payload.draft.name,
                        code: payload.draft.code,
                        price: payload.draft.price,
                        icon: payload.draft.icon,
                        description: payload.draft.description,
                    }
                    context.commit('storeBillingService', newBillingService)
                    Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                }

                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateBillingService(context, payload){
        context.state.loading = true
        axios.put('/admin/billingService/' + payload.id, payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                    payload.draft.frequency = response.data.data.frequency
                    context.commit('updateBillingService', payload)
                }
                context.state.loading = false

            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeBillingService(context, id){
        context.state.loading = true
        axios.delete('/admin/billingService/' + id)
            .then(response => {

                context.commit('removeBillingService', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
                context.state.loading = false
            })
            .catch(error => {
                context.state.loading = false
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })

    },

    listBillingServices(context){
        axios.get('/admin/billingServiceList')
            .then(response => {
                context.commit('listBillingServices', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getBillingServices(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.billingServices = data.data;
    },

    getBillingService(state, {data}){
        state.solo = data
    },

    storeBillingService(state, newBillingService){
        state.billingServices.unshift(newBillingService);
    },

    updateBillingService(state, {id, draft}){
        let index = state.billingServices.findIndex(billingService => billingService.id == id);
        state.billingServices.splice(index, 1, draft);
    },

    removeBillingService(state, id)    {
        let index = state.billingServices.findIndex(billingService => billingService.id == id);
        state.billingServices.splice(index, 1);
    },

    listBillingServices(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}