<?php

use Illuminate\Database\Seeder;


class CatEquipmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\CatEquipment::class,10)->create();
    }
}
