<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealers', function (Blueprint $table) {
            $table->increments('id');

            /*Personal Info*/
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->string('dealer_shortname')->nullable();
            $table->string('dealer_EIN')->nullable()->unique();
            $table->string('dealer_manager')->nullable();

            /*Location*/
            $table->string('sunbizzurl')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->string('website')->nullable();
            $table->string('original_phone')->nullable();
            $table->string('redirect_phone')->nullable();
            $table->string('fax')->nullable();

            /*Content*/
            $table->longtext('mission')->nullable();
            $table->longtext('vision')->nullable();
            $table->longtext('overview')->nullable();
            $table->longtext('about')->nullable();
            $table->longtext('biographies')->nullable();
            $table->boolean('mail_receive')->default(1);
            $table->boolean('status')->default(true);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealers');
    }
}
