<!-- CSS ================================================== -->
<link href="{{ url('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
<link href="{{ url('assets/css/bootstrap-theme.css') }}" rel="stylesheet" type="text/css">
<link href="{{ url('assets/css/style.css') }}" rel="stylesheet" type="text/css">
<link href="{{ url('assets/css/prettyPhoto.css') }}" rel="stylesheet" type="text/css">
<link href="{{ url('assets/css/owl.carousel.css') }}" rel="stylesheet" type="text/css">
<link href="{{ url('assets/css/owl.theme.css') }}" rel="stylesheet" type="text/css">
<link href="{{ url('assets/css/custom.css') }}" rel="stylesheet" type="text/css">
<!-- Color Style -->
<link href="{{ url('assets/colors/color4.css')}}" rel="stylesheet" type="text/css">
<!-- <link href="{{ url('/css/app.css')}}" rel="stylesheet" type="text/css"> -->
<!-- SCRIPTS ================================================== -->
<script src="{{ url('assets/js/modernizr.js')}}"></script><!-- Modernizr -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{getConfig('google_api_key')}}&libraries=places"></script>
<script src="{{ url('js/lan.js') }}"></script>
@stack('head_scripts')