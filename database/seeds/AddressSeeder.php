<?php

use App\Address;
use App\User;
use Illuminate\Database\Seeder;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Address::class)->create(['user_id' => 26, 'country' => 'United States', 'state' => 'CA', 'locality' => 'ONTARIO ', 'postal_code' => '91761', 'route' => '1960 S Quaker Ridge Pl', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 27, 'country' => 'United States', 'state' => 'CA', 'locality' => 'RIVERSIDE ', 'postal_code' => '92501', 'route' => '1391 W. La Cadena DR. #A Riverside CA 92501 ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 28, 'country' => 'United States', 'state' => 'CA', 'locality' => 'PARAMOUNT ', 'postal_code' => '90241', 'route' => '11221 Paramount, BLVS Downey CA 90241 ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 29, 'country' => 'United States', 'state' => 'CA', 'locality' => 'VAN NUYS  ', 'postal_code' => '91405', 'route' => '7560 Sepulveda Blvd, Van Nuys, CA 91405 ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 21, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Miami ', 'postal_code' => '33166', 'route' => '721 Curtiss Pkwy  ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 22, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Miami ', 'postal_code' => '33166', 'route' => '721 Curtiss Pkwy Apt 6', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 31, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Miami ', 'postal_code' => '33142', 'route' => '3410 NW 36TH STREET MIAMI, FL 33142 ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 33, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Miami ', 'postal_code' => '33317', 'route' => '550 S STATE ROAD 7, PLANTATION FL 33317 ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 34, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Hialeah ', 'postal_code' => '33013', 'route' => '4265 EAST 8 AVE. HIALEAH, FL 33013', 'secondary' => '4101 EAST 8AVE. HIALEAH, FL 33013 ', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 23, 'country' => 'United States', 'state' => 'CA', 'locality' => 'ONTARIO ', 'postal_code' => '91762', 'route' => '5325 W. Mission Blvd  ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 36, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Miami ', 'postal_code' => '33142', 'route' => '3700 NW 27th AVENUE MIAMIA, FL 33142  ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 37, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Miami ', 'postal_code' => '33142', 'route' => '3320 NW 36 ST Miami FL 33142  ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 39, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Hialeah ', 'postal_code' => '33013', 'route' => '840 EAST 49th ST HIALEAH, FL 33013', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 42, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Miami ', 'postal_code' => '33334', 'route' => '4020 NE 6 AVE. OAKLAND PARK, FL 33334 ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 43, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Miramar Beach ', 'postal_code' => '33023', 'route' => '1800 S STATE ROAD 7, MIRAMAR, FL 33023', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 44, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Hialeah ', 'postal_code' => '33010', 'route' => '1175 SE 8TH AVE. HIALEAH FL 33010 ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 45, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Miami ', 'postal_code' => '33142', 'route' => '2951 NW 27 TH AVE. MIAMI FL 33142 ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 46, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Miami ', 'postal_code' => '33126', 'route' => '850 NW 42 AVE. MIAMI FL 33126 ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 47, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Miami ', 'postal_code' => '33317', 'route' => '830 SR 7 Plantation, FL 33317 ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 48, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Hollywood ', 'postal_code' => '33023', 'route' => '5980 FUSTON STREET HOLLYWOOD, FL 33023', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 49, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Miami ', 'postal_code' => '33126', 'route' => '4525 NW 7TH. ST. MIAMI, FL 33126  ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 51, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Hialeah ', 'postal_code' => '33010', 'route' => '400 EAST 9 STREET HIALEAH, FL 33010 ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 53, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Hialeah ', 'postal_code' => '33016', 'route' => '10030 NW 75TH AVENUE HIALEAH GARDENS, FL 33016', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
        factory(Address::class)->create(['user_id' => 35, 'country' => 'United States', 'state' => 'FL', 'locality' => 'Miami ', 'postal_code' => '33314', 'route' => '5000 SW 52nd STREET SUITE 501 DAVIE FL 33314  ', 'secondary' => '', 'phone' => null, 'latitude' => null, 'longitude' => null, 'street_number' => '']);
    }

}