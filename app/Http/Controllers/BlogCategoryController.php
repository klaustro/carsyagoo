<?php

namespace App\Http\Controllers;

use App\BlogCategory;
use App\Post;
use Auth;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
     public function index()
    {
        return BlogCategory::search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);

    }

    public function blogCategoryList()
    {
        return BlogCategory::all();
    }

	public function store()
    {
        try {

            $v = \Validator::make(request()->draft, [
                'name' => 'required',
                /*'imageurl' => 'required',*/
                'metadesc' => 'required',
                'metakeywords' => 'required',
                'description' => 'required',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }

            $blogCategory = BlogCategory::create(request()->draft);


            if ( $blogCategory->save()) {
                return [
                    'message' => trans('app.blogCategory.store_message'),
                    'id' => $blogCategory->id, 'data'=>$blogCategory, 'status' => 1,
                ];

            }else{
                return [
                    'message' => trans('app.blogCategory.store_message_error'), 'status' => 1,

                ];

            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.store_error')], 500);
        }
    }

	public function update($id)
    {
        try {

            $v = \Validator::make(request()->draft, [
                'name' => 'required',
                'description' => 'required',
                /*'imageurl' => 'required',*/
                'metadesc' => 'required',
                'metakeywords' => 'required',
                'description' => 'required',
            ]);

            $errors = $v->errors();
            $message=[];

            foreach ($errors->all() as  $mess) {
                $message[]=$mess.'  ';
            }

            if ($v->fails())
            {
                return ['message' => $message , 'status' => 0];
            }
            $blogCategory = BlogCategory::where('id',$id)
            ->update(request()->draft);

            if ( $blogCategory) {
                return [
                    'message' => trans('app.blogCategory.update_message'), 'status' => 1,
                ];
            }else{
                return [
                    'message' => trans('app.blogCategory.update_message_error'), 'status' => 1
                ];
            }

        }catch(\Exception $e){
            return response()->json(['status', trans('app.common.error')], 500);
        }

    }

    public function delete($id)
    {
        $blogCategory = BlogCategory::destroy($id);

        return ['message' => trans('app.blogCategory.delete_message')];
    }

    public function list()
    {
        $post_categories = BlogCategory::get();

        $post_categories->map(function($category){
            $category->post_count = $category->posts->count();
        });

        $post_count = Post::count();

        return [
            'post_categories' => $post_categories,
            'post_count' => $post_count,
        ];
    }
}
