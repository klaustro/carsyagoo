<?php

namespace App\Http\Controllers;

use App\Comment;
use App\User;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index()
    {
        return Comment::with(['user', 'replies.user'])->where('post_id', request()->post_id)->get();
    }

    public function store()
    {
        $comment = new Comment();
        $comment->user_id = auth()->user()->id;
        $comment->post_id = request()->post_id;
        $comment->reply = request()->reply;
        $comment->content = request()->content;
        $comment->save();

        return [
            'message' => trans('app.comment.store_message'),
            'comment' => Comment::with(['user', 'replies.user'])->find($comment->id),
        ];
    }
}
