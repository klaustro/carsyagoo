let state = {
    categories: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
    loading:false,
}

let getters = {
        findCategory(state){
            return function(id){
                let category = state.categories.find(color => category.id == id)
                return category;
            }
        },
}

let actions = {
    getCategories(context, params){
        context.state.loading = true
        axios.get('/admin/category?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getCategories', {data: response.data.categories})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },
    storeCategory(context, payload){
        context.state.loading = true
        axios.post('/admin/category', payload)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }

                }else{
                    let newCategory = {
                        id: response.data.category.id,
                        name: response.data.category.name,
                        title: response.data.category.title,
                        alias: response.data.category.alias,
                        metadata: response.data.category.metadata,
                        metakey: response.data.category.metakey,
                        metadesc: response.data.category.metadesc,
                        status: response.data.category.status,
                    }
                    Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                    context.commit('storeCategory', newCategory)
                }
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    updateCategory(context, payload){
        context.state.loading = true
        axios.put('/admin/category/' + payload.id, payload.draft)
            .then(response => {
                if (response.data.status == 0) {
                    for (var i = response.data.message.length - 1; i >= 0; i--) {
                        Vue.toasted.show(response.data.message[i], {icon: 'exclamation-triangle', type: 'error'})
                    }
                }else{
                    Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                    context.commit('updateCategory', payload)
                }
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })
    },

    removeCategory(context, id){
        context.state.loading = true
        axios.delete('/admin/category/' + id)
            .then(response => {
                context.commit('removeCategory', id)
                Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
                context.state.loading = false
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
                context.state.loading = false
            })

    },

    listCategories(context){
        axios.get('/admin/categories')
            .then(response => {
                context.commit('listCategories', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

}

let mutations = {
    getCategories(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.categories = data.data;
    },

    storeCategory(state, newCategory){
        state.categories.unshift(newCategory);
    },

    updateCategory(state, {id, draft}){
        let index = state.categories.findIndex(category => category.id == id);
        state.categories.splice(index, 1, draft);
    },

    removeCategory(state, id)    {
        let index = state.categories.findIndex(category => category.id == id);
        state.categories.splice(index, 1);
    },

    listCategories(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}