<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DealerImage extends Model
{
    protected $fillable = [
      'type','name', 'dealer_id'
    ];

    public function dealer()
    {
    	return $this->BelongsTo('App\Dealer');
    }
}
