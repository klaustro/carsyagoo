<?php

use Faker\Generator as Faker;

$factory->define(App\Fuel::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
        'letter' => $faker->city,
        'csscode' => $faker->hexcolor,
        'image' => $faker -> imageUrl(640, 480),
        'category_id' => function(){
            return firstOrFactory(\App\Category::class);
        },
    ];
});
