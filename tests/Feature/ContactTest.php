<?php

namespace Tests\Feature;

use App\Contact;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->actingAs($this->user);
    }

    /** @test */
    function if_contact_is_paginated()
    {
        $contacts = factory(Contact::class, 6)->create();

        $this->get('admin/contact?page=1&search=&orderBy=id&desc=true&rows=5')
             ->assertSuccessful()
             ->assertSee($contacts[5]->email)
             ->assertDontSee($contacts[0]->email);

    }

    /** @test */
    function if_contact_is_finded()
    {
        $searched = factory(Contact::class)->create([
            'email' => 'contact@example.com',
        ]);

        $contact = factory(Contact::class)->create();

        $this->get('admin/contact?search=contact@example.com&orderBy=id&desc=true&rows=5')
             ->assertSuccessful()
             ->assertSee($searched->email)
             ->assertDontSee($contact->email);

    }
}
